// code editor
let editor = new CodeMirror.fromTextArea(document.getElementById("editor"), {
    dragDrop: true,
    lineNumbers: true,
    mode: "",
    theme: "mreq",
    lineWrapping: true,
    autorefresh: false
})
//translated code viewer
let viewer = new CodeMirror.fromTextArea(document.getElementById("viewer"), {
    dragDrop: false,
    readOnly: true,
    lineNumbers: true,
    mode: "",
    theme: "mreq",
    lineWrapping: true,
    autorefresh: false
})
let filename = '' //source code filename
let issueTableTitles = {"Type" : "col-1", "At line": "col-1", "Message": "col-10"} //issue table titles with column widths

//target language codes
const Target = Object.freeze({
    "NONE": 0,
    "LUA": 1,
    "PY": 2,
    "JS": 3,
    "RUBY": 4,
    "PSUDO": 5
})

//target language codes
const IssueType = Object.freeze({
    "UNKNOWN" : 0, 
    "UNSUPPORTED" : 1,
    "SYNTAX" : 2,
    "WARNING" : 3,
    "LOGICAL" : 4,
    "OPTIMIZE" : 5,
    "INFO" : 6,
    "FAILURE" : 7,
})


function main() {
    initiateEditor()
    bindClickEvents()
    initiateSelectionBoxes()
    initiateIssueTable()
}

/**
 * Sets editor mode by file extension of the current source file.
 */
function setEditorMode() {
    extension = filename.slice(filename.indexOf('.') + 1)
    mode = ''

    //set mode by file extension
    switch (extension) {
        case 'js':
            mode = 'javascript'
            break
        case 'lua':
            mode = 'lua'
            break
        case 'py':
            mode = 'python'
            break
        case 'rb':
            mode = 'ruby'
            break
    }
    //select the right source language
    for (const option of document.querySelectorAll("#source-langs > .custom-option")) {
        if (option.getAttribute('display-mode') == mode) {
            option.parentNode.querySelector('.custom-option.selected').classList.remove('selected')
            option.classList.add('selected')
            option.closest('.custom-select').querySelector('.custom-selected span').textContent = option.textContent
            break
        }
    }

    editor.setOption('mode', mode) //set mode
}

/**
 * initiates code viewer and editor.
 */
function initiateEditor() {
    //initiate size
    editor.setSize("100%", "100%")
    viewer.setSize("100%", "100%")
    //setup default text
    editor.setValue("Drop your file here!\n<< or >>\nJust write your code and select the right source language :)")
    viewer.setValue("Translated code preview (readonly)\nTo download the results,\njust click the download button ;)")
    //drop event handler
    editor.on('drop', function(edit, e) {
        // Check if files were dropped
        let files = e.dataTransfer.files
        if (files.length > 0) {
            filename = files[0].name
            setEditorMode(filename)
            editor.setValue("")
            document.getElementById("filename").textContent = filename
        }
    })
}

/**
 * initiates issue table.
 */
function initiateIssueTable() {
    let table = document.querySelector("#issues")
    setTableTitles(table, issueTableTitles)
}

/**
 * initiates all selection boxes.
 */
function initiateSelectionBoxes() {
    //initiate taggle option
    for(const box of document.querySelectorAll('.custom-select-wrapper'))
    {
        box.addEventListener('click', function() {
            this.querySelector('.custom-select').classList.toggle('open')
        })
    }
    
    //initiate options for target languages
    for (const option of document.querySelectorAll("#target-langs > .custom-option")) {
        option.addEventListener('click', function() {
            if (!this.classList.contains('selected')) {
                this.parentNode.querySelector('.custom-option.selected').classList.remove('selected')
                this.classList.add('selected')
                this.closest('.custom-select').querySelector('.custom-selected span').textContent = this.textContent
            }
        })
    }

    //initiate options for source languages
    for (const option of document.querySelectorAll("#source-langs > .custom-option")) {
        option.addEventListener('click', function() {
            if (!this.classList.contains('selected')) {
                this.parentNode.querySelector('.custom-option.selected').classList.remove('selected')
                this.classList.add('selected')
                this.closest('.custom-select').querySelector('.custom-selected span').textContent = this.textContent
            }
            editor.setOption('mode', option.getAttribute('display-mode'))
            filename = generateFilename(true)
            document.getElementById("filename").textContent = filename
        })
    }
}

/**
 * binds click events of menu's buttons.
 */
function bindClickEvents() {
    document.getElementById("properties").onclick = clickProperties
    document.getElementById("report").onclick = clickReport
    document.getElementById("convert").onclick = clickConvert
    document.getElementById("download").onclick = clickDownload
}

/**
 * collapses properties menu.
 */
function collapseProperties() {
    let menu = document.getElementById("properties-menu")
    let elements = document.getElementById("properties-display")

    elements.classList.remove('show-animation')
    elements.classList.add('hide-animation')
    menu.style.width = "0%"
    menu.style.visibility = "hidden"
    menu.setAttribute('data-collapsed', 'true')
}

/**
 * expands properties menu.
 */
function expandProperties() {
    let menu = document.getElementById("properties-menu")
    let elements = document.getElementById("properties-display")

    elements.classList.remove('hide-animation')
    elements.classList.add('show-animation')
    menu.style.width = "25%"
    menu.setAttribute('data-collapsed', 'false')
}

/**
 * properties button click event.
 */
function clickProperties() {
    let isCollapsed = document.getElementById("properties-menu").getAttribute('data-collapsed') === 'true'

    if(isCollapsed) {
        collapseReport()
        expandProperties()
    }
    else {
        collapseProperties()
    }
}

/**
 * expands report menu.
 */
function expandReport() {
    let menu = document.getElementById("report-menu")
    let elements = document.getElementById("report-display")

    elements.classList.remove('hide-animation')
    elements.classList.add('show-animation')
    menu.style.width = "25%"
    menu.style.height = "100%"
    menu.style.overflow = "auto"
    menu.setAttribute('data-collapsed', 'false')
}

/**
 * collapses report menu.
 */
function collapseReport() {
    let menu = document.getElementById("report-menu")
    let elements = document.getElementById("report-display")

    elements.classList.remove('show-animation')
    elements.classList.add('hide-animation')
    menu.style.width = "0%"
    menu.style.overflow = "hidden"
    menu.style.visibility = "hidden"
    menu.setAttribute('data-collapsed', 'true')
}

/**
 * report button click event.
 */
function clickReport() {
    let isCollapsed = document.getElementById("report-menu").getAttribute('data-collapsed') === 'true'

    if(isCollapsed) {
        collapseProperties()
        expandReport()
    }
    else {
        collapseReport()
    }
}

/**
 * download button click event.
 */
function clickDownload() {
    download(generateFilename(false), viewer.getValue())
}

/**
 * downloads text as a file.
 * @param {string} filename filename
 * @param {string} text file content
 */
function download(filename, text) {
    var pom = document.createElement('a')
    pom.setAttribute('href', 'data:text/plaincharset=utf-8,' + encodeURIComponent(text))
    pom.setAttribute('download', filename)

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents')
        event.initEvent('click', true, true)
        pom.dispatchEvent(event)
    }
    else {
        pom.click()
    }
}

/**
 * generates filename for source code or translated result code.
 * @param {boolean} isSource is this a source file?
 * @returns generated filename.
 */
function generateFilename(isSource) {
    let result = ''
    let ext = ''
    let query = isSource ? "#source-langs > .custom-option" : "#target-langs > .custom-option" //source | target language query
    for (const option of document.querySelectorAll(query)) {
        if (option.classList.contains('selected')) {
            //get the file extension of the selected language
            switch (option.getAttribute('display-mode')) {
                case 'javascript':
                    ext = 'js'
                    break
                case 'lua':
                    ext = 'lua'
                    break
                case 'python':
                    ext = 'py'
                    break
                case 'ruby':
                    ext = 'rb'
                    break
            }
            break
        }
    }
    //create filename
    result = isSource ? "editor_content." + ext : filename.split('.')[0] + "." + ext
    
    return result
}

/**
 * convert button click event.
 */
function clickConvert() {
    //check if file was dropped or if editor is empty
    if(!filename || !editor.getValue()) {
        alert("No source code found!\nPlease check if the right source language is selected and the editor is not empty.")
    }
    else {
        //send request to server
        fetch('./translate', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ 
                code: 1, 
                source: editor.getValue(), 
                filename: filename, 
                target: getSelectedTarget()
            })
        })
        .then(response => response.json())
        .then((data) => {
            viewer.setValue(data.translated) //set preview of the result
            setIssuesView(data.issues) //set issues for display
        })
    }
}

/**
 * get the selected target language code.
 * @returns language code.
 */
function getSelectedTarget() {
    let lang = 0
    //search for the selected target language
    for (const option of document.querySelectorAll("#target-langs > *")) {
        if(option.classList.contains('selected')) {
            viewer.setOption('mode', option.getAttribute('display-mode'))
            lang = +option.getAttribute('lang-code')
            break
        }
    }

    return lang
}

/**
 * sets issues display.
 * @param {array} issues 
 */
function setIssuesView(issues) {
    let table = document.querySelector("#issues")
    let oldBody = document.querySelector("#issues > tbody")

    //sort issues by line number
    issues = issues.sort((first, second) => {
        if(first.line < second.line) {
            return -1;
        }
        if(first.line > second.line) {
            return 1;
        }
        return 0;
    })

    let body = document.createElement("tbody")

    //if exists, replace old body with the new body of the issue table
    if(oldBody) {
        table.replaceChild(body, oldBody)
    }
    else {
        table.appendChild(body)
    }

    //set all issues records for display
    for(let issue of issues) {
        let row = body.insertRow()
        let cell = row.insertCell()
        let mark = document.createElement("span")

        //set issue type
        mark.classList.add('issue-type')
        mark.classList.add(getIssueMarkByType(issue["type"]))
        cell.appendChild(mark)
        cell.classList.add(issueTableTitles["Type"])
        cell.setAttribute("disp-type", "issue-type")
        
        //set line number
        cell = row.insertCell()
        cell.appendChild(document.createTextNode(issue["line"]))
        cell.classList.add('text-center')
        cell.classList.add(issueTableTitles["At line"])
        cell.setAttribute("disp-type", "line")

        //set issue message
        cell = row.insertCell()
        cell.appendChild(document.createTextNode(issue["message"]))
        cell.classList.add(issueTableTitles["Message"])
        cell.setAttribute("disp-type", "message")

        //set record click event
        row.onclick = (event) => {
            //move cursor to position
            editor.focus()
            editor.setCursor({ line: parseInt(event.target.closest('tr').querySelector('[disp-type="line"]').firstChild.nodeValue) - 1, ch: 0 })
        }
        row.classList.add('d-flex')
    }
}

/**
 * sets table titles.
 * @param {Element} table 
 * @param {Object} titles 
 */
function setTableTitles(table, titles) {
    let thread = table.createTHead()
    let row = thread.insertRow()

    //for every title, save it in the head and setup column width
    for (let title of Object.keys(titles)) {
        let th = document.createElement("th")
        let text = document.createTextNode(title)
        th.classList.add('text-center')
        th.appendChild(text)
        th.classList.add(titles[title])
        row.appendChild(th)
    }
    row.classList.add('d-flex')
}

/**
 * get issue mark by issue type.
 * @param {int} type 
 * @returns mark CSS class name.
 */
function getIssueMarkByType(type) {
    let mark = ''
    switch(type) {
        case IssueType.UNSUPPORTED:
        case IssueType.FAILURE:
        case IssueType.LOGICAL:
        case IssueType.SYNTAX:
            mark = 'error-type'
            break
        case IssueType.WARNING:
            mark = 'warn-type'
            break
        case IssueType.OPTIMIZE:
            mark = 'optimize-type'
            break
        case IssueType.INFO:
            mark = 'info-type'
            break
        default:
            mark = 'default-type'
            break
    }
    return mark
}

//run
main()