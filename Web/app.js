const api = require('./session') //codex engine API
const express = require('express')
const app = express()
const port = 3000

function main() {
    //setup application server
    app.use(express.static('public'))
    app.use(express.json())
    app.use(
        express.urlencoded({
            extended: false
        })
    )
    
    //set get request handler
    app.get('/', (req, res) => {
        res.sendFile(__dirname + '/index.html') //send main page
    })

    //set post request handler
    app.post('/translate', handle)

    //strat listening
    app.listen(port, () => {
        console.log(`CodeX is now listening at http://localhost:${port}`)
    })
}

/**
 * handles post request.
 * @param {any} request client request
 * @param {any} response server response
 */
function handle(request, response) {
    //resolve request
    resolve(request.body, (result) => {
        response.json(result) //send response as json
    })
}

/**
 * resolves client request.
 * @param {Object} payload request content
 * @param {function} callback result sender
 */
function resolve(payload, callback) {
    if(payload.hasOwnProperty('code')) {
        switch(payload.code) {
            //handle translation request
            case 1:
                //check if request is valid
                if(payload.hasOwnProperty('source') || payload.hasOwnProperty('filename') || payload.hasOwnProperty('target')) {
                    //send request to the API and recevie results
                    api.session(api.encoder(payload.source, payload.filename, payload.target), callback)
                }
                else
                {
                    error(callback)
                }
                break
            default:
                error(callback)
                break
        }
    }
    else {
        error(callback)    
    }
}

/**
 * sends generic error message.
 * @param {function} callback result sender 
 */
function error(callback) {
    callback({ code: 0, message: "invalid request" })
}

//run
main()