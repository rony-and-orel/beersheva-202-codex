const net = require('net');

//target language codes
const Target = Object.freeze({
    "NONE": 0,
    "LUA": 1,
    "PY": 2,
    "JS": 3,
    "RUBY": 4 
})

/**
 * Encodes a request message.
 * @param {string} source source code raw text
 * @param {string} filename source filename
 * @param {int} target traget language
 * @returns a buffer of the request message
 */
function encoder(source, filename, target) {
    return Buffer.from((JSON.stringify({ "source": source, "filename": filename, "target": target }) + "\n"), "utf-8")
}

/**
 * Decodes a response message.
 * @param {Buffer} data response raw data
 * @returns a json of the results from engine
 */
function decoder(data) {
    return JSON.parse(data.toString().slice(0, -2))
}

/**
 * Runs a session with codex engine.
 * @param {buffer} request client request
 * @param {function} handler result handler
 */
function session(request, handler) {
    // connect to engine and set listeners
    let connection = net.createConnection({ port: 8200 }, () => {
        connection.on('data', (data) => {
            connection.end()
            handler(decoder(data)) //handle result
        })
        connection.write(request)
    })
}

//export
module.exports = { encoder, decoder, session, Target }