# CodeX - The Ultimate Transpiler
As part of the national cyber education program - Magshimim - we were tasked to work on a large-scale project for our third and last year in the program. It took a lot of effort, wit, and courage to complete this ambitious project, but we successfully did it 🎉

## What to download?
First, you must download some external libraries for C++ via [vcpkg](https://github.com/microsoft/vcpkg), some workspaces... you know, the usual things.
- For C++ (in x64 mode):
    * boost-log
    * boost-asio
    * boost-algorithm
    * boost-regex
    * nlohmann-json
- For Javascript:
    * Express.js
    * Node.js

*Note*: Bootstrap and CodeMirror are deployed via CDN's, there is no need to install them locally.
*Note*: This project is written in C++20, make sure you have the latest update of MSVC, and you have the `/std:latest` option enabled in the project properties.

## How can I use this amazing project?
Simple! Just compile (in debugging mode) and run the codex engine (`Codex.exe`). After that lunch, the website and connect to `http://localhost:3000/`. 
Now all that's left is to play around a little bit, the UI is not that complex! 😉

## Remarks
Some of the features in this project are still experimental, so we might suggest playing with this project carefully. 
All in all, we are happy to share this project with you, we put all our love into it 💖

Best regards,
Rony Alaluf and Orel Aharonov (AKA the developers).
