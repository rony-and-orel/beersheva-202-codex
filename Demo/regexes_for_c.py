# basic regex patterns #

INT_PATTERN = r"(\d+)"
HEX_PATTERN = r"(0x[[:xdigit:]]+)"
DOUBLE_PATTERN = r"(\d+\.\d+)"
CHAR_PATTERN = r"(\'(.|(\\[trabfnv\\\'\"\d]))\')"

ARITHMTIC_PATTERN = r"(\+|\-|\/|\*|\%)"
BINARY_PATTERN = r"(\<\<|\>\>|\&|\||\^)"
BOOLEAN_PATTERN = r"(\&\&|\|\||\<|\>|\<\=|\=\=|\=\>|\!\=)"

VARIABLE_PATTERN = r"([A-Za-z_]\w*)"
FUNCTION_PATTERN = r"([A-Za-z_]+\d*\(.*\))"
FUNCTION_NAME_PATTERN = r"([A-Za-z_]+\d*(?=\())"

COMMENT_PATTERN = r"(\/\/.+)|(\/\*[\w\s{}=<>|,;.?\"\':~`+!@#$%^&*()]+\*\/)"

END_SEMICOLON = r"(?=\;)"

TYPE_PATTERN = r"(((void)|((unsinged|signed)*(int|float|double|char|long|long long|long long int)))\**(\[.*\])*)"
RTYPE_PATTERN = r"(^" + TYPE_PATTERN + r")"

STR_PATTERN = r'".+"'

COMA = r","

# operation-related regex patterns #

MEM_PATTERN = r'(?<=\s)(\*|\&)' + VARIABLE_PATTERN

EXPRESSION_PATTERN = r'((' + FUNCTION_PATTERN + r'|' + HEX_PATTERN + r'|' + CHAR_PATTERN + r'|' + DOUBLE_PATTERN + r'|' + MEM_PATTERN + r'|' + VARIABLE_PATTERN + r'|' + INT_PATTERN + r')\s*(' \
                        + ARITHMTIC_PATTERN + r'|' + BINARY_PATTERN + r')\s*)*' + \
                        r'(' + FUNCTION_PATTERN + r'|' + HEX_PATTERN + r'|' + CHAR_PATTERN + r'|' + DOUBLE_PATTERN + r'|' + MEM_PATTERN + r'|' + VARIABLE_PATTERN + r'|' + INT_PATTERN + r')'

ASSIGN_PATTERN = VARIABLE_PATTERN + r' *= *' + EXPRESSION_PATTERN + END_SEMICOLON

CONDITION_PATTERN = r'(' + EXPRESSION_PATTERN + r' *' + BOOLEAN_PATTERN + r' *' + r')+' + EXPRESSION_PATTERN

IF_PATTERN = r'if *\( *(' + EXPRESSION_PATTERN + r'|' + CONDITION_PATTERN + r') *\)'

ELSE_PATTERN = r'else'

BRACKET_PATTERN = r"\(|\)"

SCOPE_PATTERN = r"\{|\}"

DECLERATION_PATTERN = TYPE_PATTERN + r"\s+" + VARIABLE_PATTERN

DEFINITION_PATTERN = TYPE_PATTERN + r"\s+" + FUNCTION_PATTERN

RETURN_PATTERN = r"return\s+(" + EXPRESSION_PATTERN + r'|' + CONDITION_PATTERN + r')' + END_SEMICOLON

IMPORT_PATTERN = r"\#include\ *.+"

# custom patterns #
FUNCTION_CODE = (r"(?:(?:(?:void)|(?:(?:unsinged|signed)*(?:int|float|double|char|long|long long|long long int)))\**(?:\[.*\])*)\s+(?:", r")\(.*\)\s*\{(?:\s*.*;)*\s*\}")

# patterns and types list
default_patterns = (
   (COMMENT_PATTERN, "comment"),
   (IF_PATTERN, "if"),
   (SCOPE_PATTERN, "scope"),
   (IMPORT_PATTERN, "import"),
   (DEFINITION_PATTERN, "definition"),
   (DECLERATION_PATTERN, "decleration"),
   (RETURN_PATTERN, "return"),
   (ASSIGN_PATTERN, "assign"),
   (ELSE_PATTERN, "else"),
   (CONDITION_PATTERN, "condition"),
   (FUNCTION_PATTERN, "call"),
   (EXPRESSION_PATTERN, "expression"),
   (MEM_PATTERN, "mem"),
   (BRACKET_PATTERN, "bracket"),
)