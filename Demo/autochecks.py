from parse import CodeMap
from reader import Reader
import operations as ops

class Reporter():
    REPORT_FILE = "report.txt"
    
    @staticmethod
    def __init__():
        open(Reporter.REPORT_FILE, "w")

    @staticmethod
    def write(line : int, filename : str, err : int, msg : str):
        with open(Reporter.REPORT_FILE, "a") as f:
            f.write("# new issue found!\n  at '" + filename + "' in line " + str(line) + ":\n  " + "ERR" + str(err) + ": " + msg + "\n")

class AutoChecks():
    @staticmethod
    def return_in_middle_of_code(code_map : CodeMap):
        """
        Scans for unreachable code in function due to 'return' in middle of code.
        """
        scanner = Reader(code_map)
        while True:
            try:
                opSeqNode, flag = scanner.step(do_automatic_jump=True)
                if type(opSeqNode.get_op()) == ops.Return and (flag == Reader.state.IN_SCOPE or flag == Reader.state.AT_SCOPES_START):
                    Reporter.write(opSeqNode.get_line_number(), opSeqNode.get_filename(), 2, 'Return in middle of code')
            except StopIteration:
                break

    @staticmethod
    def condition_is_constant(code_map : CodeMap):
        """
        Scans for a condition that is *always* the same result
        """
        scanner = Reader(code_map)
        while True:
            try:
                op = scanner.step(do_automatic_jump=True)[0]
                if type(op.get_op()) == ops.If and AutoChecks.is_condition_constant(op.get_op().get_condition()):
                    Reporter.write(op.get_line_number(), op.get_filename(), 1, 'Constant condition in if statement')
            except StopIteration:
                break
    
    @staticmethod
    def is_condition_constant(op):
        if type(op) == ops.Condition:
            return AutoChecks.is_condition_constant(op.get_tree())
        elif type(op) == ops.Expression:
            return AutoChecks.is_expression_constant(op)
        elif type(op) == ops.Condition.CondNode:
            left = AutoChecks.is_condition_constant(op.get_left())
            right = AutoChecks.is_condition_constant(op.get_right())
            return left == True and right == True
        else:
            raise BaseException('Unknown type in condition testing.')

    @staticmethod
    def is_expression_constant(op):
        if type(op) == ops.Expression.ExprNode:
            left = AutoChecks.is_expression_constant(op.get_left())
            right = AutoChecks.is_expression_constant(op.get_right())
            return left == True and right == True
        if type(op) == ops.Variable:
            return False
        elif type(op) == ops.Value:
            return True
        elif type(op) == ops.Call:
            return False
        elif type(op) == ops.Expression:
            return AutoChecks.is_expression_constant(op.get_tree())
        else:
            raise BaseException('Unknown type in expression testing.')


checks = (AutoChecks.return_in_middle_of_code, AutoChecks.condition_is_constant)