import re
import regexes_for_c

class Lexer():
   def __init__(self, lang):
      self.__regexes_module = {
         "c": regexes_for_c
      }[lang] # select regex pattern module that matches the programming language

   def analyze(self, code, patterns=None):
      """
      doing lexical analysis to a code.
      :param code: code piece
      :param patterns: regex patterns to types
      :type code: str
      :type patterns: tuple
      :return: list of found lexemes
      :rtype: lst
      """
      if patterns == None:
         patterns = self.__regexes_module.default_patterns # retreive default patterns from module
      return Lexer.find_lexemes(code, patterns)

   @staticmethod
   def find_lexemes(code, patterns):
      """
      finds all lexemes in a piece of code.
      :param code: code piece
      :type code: str
      :param patterns: regex patterns to types
      :type patterns: tuple
      :return: list of found lexemes
      :rtype: lst
      """
      lexemes = []
      returned_lexemes = []

      for pattern, typeof in patterns:
         returned_lexemes = Lexer.__match_pattern(code, pattern, typeof)
         lexemes += returned_lexemes

      Lexer.__remove_duplicated_matches(lexemes, patterns)
      lexemes += Lexer.__find_endlines(code)
      return Lexer.__remove_temporary_fields(sorted(lexemes, key=lambda x: x["span"]))

   @staticmethod
   def __find_endlines(code):
      return Lexer.__match_pattern(code, r"\n", "endl")

   @staticmethod
   def __match_pattern(code, pattern, typeof):
      """
      matches a specific pattern to a lexeme type in a code.
      :param code: code piece
      :type code: str
      :param pattern: regex pattern
      :type pattern: str
      :param typeof: type of lexeme
      :type typeof: str
      :return: list of matched lexemes
      :rtype: lst
      """
      lexeme = {}
      lexemes = []
      for match in re.finditer(pattern, code):
         lexeme["span"] = match.span()
         lexeme["match"] = match.group()
         lexeme["type"] = typeof
         lexeme["passed_duplicated_matches_check"] = True
         lexemes.append(lexeme)
         lexeme = {}

      return lexemes

   @staticmethod
   def __remove_duplicated_matches(lexemes, patterns):
      """
      filters duplicated matches from the lexeme list.
      :param lexemes: list of lexemes
      :type lexemes: lst
      :param patterns: regex patterns to types
      :type patterns: tuple
      :return: list of filtered lexemes
      :rtype: lst
      """
      types = [t[1] for t in patterns]
      filtered_lexemes = []

      # search for duplicated matches 
      for lex1 in lexemes:
         for lex2 in lexemes:
            if lex1["span"][0] <= lex2["span"][0] and lex2["span"][1] <= lex1["span"][1] and lex1 != lex2:
               # compare lexemes types (find the strongest)
               if types.index(lex1["type"]) < types.index(lex2["type"]):
                  lex2["passed_duplicated_matches_check"] = False # turn off if the match is duplicated
               else:
                  lex1["passed_duplicated_matches_check"] = False # turn off if the match is duplicated
      
      for lex in lexemes:
         if lex["passed_duplicated_matches_check"]:
            filtered_lexemes.append(lex) # copy only the matches who passed the check

      return filtered_lexemes
   
   @staticmethod
   def __remove_temporary_fields(lexemes):
      new_lexemes = []
      for lex in lexemes:
         if lex["passed_duplicated_matches_check"]:
            del lex["span"], lex["passed_duplicated_matches_check"] # remove unnecessary keys
            new_lexemes.append(lex) # copy only the matches who passed the check
      return new_lexemes