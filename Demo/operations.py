"""
Context-free operations for C language.
"""
import re
import utils

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning) # ignore warning

# operation abstract class
class Operation(object):
    pass

class Else(Operation):
    def __init__(self, data):
        self.__data = data
    
    def __str__(self):
        return type(self).__name__ + '()'

class If(Operation):
    def __init__(self, data):
        self.__data = data

    def get_condition(self):
        """
        :return: the condition
        :rtype: Condition, Expression
        """
        return self.__data

    def __str__(self):
        return type(self).__name__ + '(' + str(self.__data) + ')'

class Mathematic(Operation):
    def __init__(self, data):
        self.__data = data

    def get_type(self):
        return self.__data

    def __str__(self):
        return str(self.__data)

class Variable(Operation):
    def __init__(self, data):
        self.__data = {}
        self.__data['name'] = data
        self.__data['id'] = None
        self.__data['type'] = None

    def get_name(self) -> str:
        return self.__data['name']

    def set_id(self, id : int):
        self.__data['id'] = id

    def set_type(self, var_type : str):
        self.__data['type'] = var_type

    def get_id(self) -> int:
        return self.__data['id']

    def get_type(self) -> str:
        return self.__data['type']


    def __str__(self):
        return type(self).__name__ + '(' + self.__data['name'] + ')'

class Value(Operation):
    def __init__(self, data):
        self.__data = data
    
    def get_val(self):
        return self.__data

    def __str__(self):
        return type(self).__name__ + '(' + str(self.__data) + ')'

class Expression(Operation):
    # AST node for arithmatic expression
    class ExprNode():
        def __init__(self, left, op, right):
            self.op = op
            self.right = right
            self.left = left

        def get_op(self):
            return self.op

        def get_left(self):
            return self.left

        def get_right(self):
            return self.right

        def to_string(self):
            return 'ExprNode(' + str(self.left) + ', ' + str(self.right) + ', op: ' + str(self.op) + ')'
        
        def __str__(self):
            return str(self.left) + ' ' + str(self.op) + ' ' + str(self.right)

    def __init__(self, data):
        self.__data = data

    def get_tree(self) -> ExprNode:
        return self.__data

    def __str__(self):
        return type(self).__name__ + '(' + str(self.__data) + ')'

class Booleanic(Operation):
    def __init__(self, data):
        self.__data = data
    
    def get_type(self):
        return self.__data

    def __str__(self):
        return str(self.__data)

class Condition(Operation):
    # AST node for booleanic expression
    class CondNode():
        def __init__(self, left, op, right):
            self.op = op
            self.right = right
            self.left = left

        def get_op(self) -> Booleanic:
            return self.op

        def get_left(self):
            return self.left

        def get_right(self):
            return self.right

        def to_string(self):
            return 'CondNode(' + str(self.left) + ', ' + str(self.right) + ', op: ' + str(self.op) + ')'
        
        def __str__(self):
            return str(self.left) + ' ' + str(self.op) + ' ' + str(self.right)

    def __init__(self, data):
        self.__data = data

    def get_tree(self) -> CondNode:
        return self.__data

    def __str__(self):
        return type(self).__name__ + '(' + str(self.__data) + ')'

class Assign(Operation):
    def __init__(self, data):
        self.__data = data

    def get_var(self):
        return self.__data['var']

    def get_assign(self):
        return self.__data['assign']

    def __str__(self):
        return type(self).__name__ + '(' + str(self.__data['var']) + ' = ' + str(self.__data['assign']) + ')'

class Declaration(Operation):
    def __init__(self, data):
        self.__data = data
    
    def get_type(self) -> str:
        return self.__data["type"]
    
    def get_name(self) -> str:
        return self.__data["name"]

    def __str__(self):
        return type(self).__name__ + '(' + self.__data["type"] + ' ' + self.__data["name"] + ')'

class Definition(Operation):
    def __init__(self, data):
        self.__data = data

    def get_type(self) -> str:
        return self.__data["rtype"]

    def get_name(self) -> str:
        return self.__data["name"]

    def get_args(self) -> list:
        return self.__data["args"]

    def __str__(self):
        return type(self).__name__ + '(' + self.__data["rtype"] + ' ' + self.__data["name"] + ('' if not self.__data["args"] else ': ' + ', '.join([str(arg) for arg in self.__data["args"]])) + ')'

class Call(Operation):
    def __init__(self, data):
        self.__data = data

    def get_name(self) -> str:
        return self.__data['name']

    def get_args(self) -> list:
        return self.__data['args']

    def __str__(self):
        return type(self).__name__ + '(' + self.__data['name'] + ('' if not self.__data["args"] else ': ' + ', '.join([str(arg) for arg in self.__data['args']])) + ')'

class Scope(Operation):
    def __init__(self, data):
        self.__data = data

    # getters
    def isClosed(self):
        return not self.__data

    def isOpened(self):
        return self.__data
    
    def __str__(self):
        return type(self).__name__ + '(is open? ' + str(self.__data) + ')'

class Return(Operation):
    def __init__(self, data):
        self.__data = data
    
    def get_ret(self):
        return self.__data

    def __str__(self):
        return type(self).__name__ + '(' + str(self.__data) + ')'

class Mem(Operation):
    def __init__(self, data):
        self.__data = data

    def __str__(self):
        return type(self).__name__ + '(' + ("get address of " if self.__data["type"] == 'get_address' else 'get value of ') + "'" + self.__data["var"] + "'" + ')'

class Import(Operation):
    def __init__(self, data):
        self.__data = data
    
    def search(self, func, parser):
        impl = ''
        flag = False
        line_counter = 1
        search_pattern = self.__data["search_regex"][0] + func + self.__data["search_regex"][1]
        patterns = (
            (search_pattern, "impl"),
        )
        import_lexemes = []
        func_lexemes = []
        with open(self.__data["file"], "r") as f:
            impl = f.read()
            import_lexemes = parser.code_lexer.analyze(impl, patterns)
            for lex in import_lexemes:
                if lex["type"] == "endl":
                    line_counter += 1
                if lex["type"] == "impl":
                    flag = True
                    func_lexemes = parser.code_lexer.analyze(lex["match"])
                    break
        return (func_lexemes, flag, line_counter)

    def get_file(self):
        return self.__data["file"]

    def __str__(self):
        return type(self).__name__ + '(' + str(self.__data) + ')'

if __name__ == "__main__":
    print(str(Expression("x - 54 * 2 << 45")))