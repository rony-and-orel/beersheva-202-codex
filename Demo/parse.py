"""
Parser demo for C language.
"""

import operations as ops
from opfactory import OperationFactory
from lexer import Lexer
import utils

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning) # ignore warning


class OpSeqNode():
    # operation tree node
    def __init__(self, operation, line_number, filename):
        self.__op = operation
        self.__seq = [] # under scope operations list
        self.__var_map = {} # var list for current scope
        self.__line_number = line_number
        self.__filename = filename

    # getters and setters #
    def set_op(self, op : ops.Operation):
        self.__op = op

    def get_op(self) -> ops.Operation:
        return self.__op

    def get_seq(self) -> list:
        return self.__seq

    def get_var_map(self) -> dict:
        return self.__var_map

    def get_line_number(self) -> int:
        return self.__line_number
    
    def get_filename(self) -> str:
        return self.__filename

    def has_scope(self) -> bool:
        return True if self.__seq else False


class FunctionTree():
    # operation tree class
    def __init__(self, filename):
        self.__root = []
        self.__current_node = None
        self.__scope_stack = []
        self.__current_seq = self.__root
        self.__filename = filename

    def get_filename(self):
        return self.__filename
    
    def get_root(self) -> list:
        return self.__root

    def insert(self, op : ops.Operation, line_number : int):
        """
        insert operation to operation tree.
        :param op: operation
        :type op: Operation
        """
        if isinstance(op, ops.Scope):
            # open scope
            if op.isOpened():
                self.__scope_stack.append((self.__current_seq, self.__current_node))
                self.__current_seq = self.__current_node.get_seq()
                InitialScan.add_params(self.__current_node) # add function's variables
            # close scope
            elif op.isClosed():
                self.__current_seq, self.__current_node = self.__scope_stack.pop()
        else:
            # add operation to current sequance
            self.__current_node = OpSeqNode(op, line_number, self.__filename)
            self.__current_seq.append(self.__current_node)

            # handle variable parsing
            InitialScan.action_switcher(op, self.__scope_stack)

    def unpack(self) -> list:
        """
        unpacks the operation tree.
        :return: operation stream
        :rtype: lst
        """
        return self.__unpack_recursive(self.__root, [])

    def __unpack_recursive(self, seq : list, oplst : list) -> list:
        """
        unpacks the operation tree recursivly.
        :param seq: offset operation sequance
        :type seq: lst
        :param ops: operation stream
        :type ops: lst
        :return: operation stream
        :rtype: lst
        """
        for node in seq:
            oplst.append(node.get_op())
            if len(node.get_seq()) > 0:
                oplst.append(ops.Scope('{'))
                oplst = self.__unpack_recursive(node.get_seq(), oplst)
                oplst.append(ops.Scope('}'))
        return oplst
    
    def __str__(self):
        """
        converts the operation tree to a readable text.
        :return: string
        :rtype: str
        """
        return self.__str__recursive(self.__root, "", 0)

    def __str__recursive(self, seq : list, string : str, tab_count : int):
        for node in seq:
            string += tab_count*'     ' + '+--- [' + str(node.get_line_number()) + '] ' + str(node.get_op()) + "\n"
            if node.has_scope():
                string = self.__str__recursive(node.get_seq(), string, tab_count + 1)
        return string


class CodeMap():
    # code map class, used to map the code
    def __init__(self, filename):
        self.__imports = []
        self.__func_map = {}
        self.__current_func = None
        self.__filename = filename

    # getters
    def get_func_map(self) -> dict:
        return self.__func_map

    def get_imports(self) -> list:
        return self.__imports
    
    def get_filename(self) -> str:
        return self.__filename

    def insert(self, op : ops.Operation, line_number : int, filename : str = None):
        """
        insert operation to the code map.
        :param op: operation
        :type op: Operation
        """
        filename = self.__filename if filename == None else filename
        if not isinstance(op, ops.Import):
            if isinstance(op, ops.Definition):
                self.__func_map[op.get_name()] = FunctionTree(filename) #  save functions map
                self.__current_func = self.__func_map[op.get_name()]
            self.__current_func.insert(op, line_number)
        else:
            self.__imports.append(op) # add imports seperatly

    def is_exists(self, func_name : str) -> bool:
        """
        search function's operation tree in code map.
        :param func_name: function name
        :type func_name: str
        :return: function's operation tree
        :rtype: FunctionTree
        """
        flag = True
        try:
            self.__func_map[func_name]
        except Exception:
            flag = False
        return flag

    def __str__(self):
        """
        converts the code map to a readable text.
        :return: string
        :rtype: str
        """
        return "::: CodeMap View :::\n" + \
               "code analytics @ '" + self.__filename + "'\n" + \
               "\nincluded files:\n  + " + '\n  + '.join([inc.get_file() for inc in self.__imports]) + \
               "\n\nfunction trees:\n\n" + '\n'.join(["['" + tree + "' at " + self.__func_map[tree].get_filename() + "]\n" + str(self.__func_map[tree]) for tree in self.__func_map.keys()])


class InitialScan():
    next_var_id : int = 0 # count uniqe variable ids
    params : list = []
    functions_to_parse : set = set()
    @staticmethod
    def action_switcher(op, scope_stack):
        """
        Handle selecting function to run on Operation objects
        :param op: operation to test
        :param scope_stack: scope stack
        :type op: Operation
        :type scope_stack: lst
        """
        switcher = {
            ops.Declaration: InitialScan.add_variable,
            ops.Variable: InitialScan.locate_variable,
            ops.Assign: InitialScan.scan_assign,
            ops.Expression: InitialScan.scan_expression,
            ops.Expression.ExprNode: InitialScan.scan_expression_rec,
            ops.Condition: InitialScan.scan_condition,
            ops.Condition.CondNode: InitialScan.scan_condition_rec,
            ops.Call: InitialScan.scan_call,
            ops.Definition: InitialScan.scan_definition,
            ops.Return: InitialScan.scan_return,
            ops.If: InitialScan.scan_if
        }
        func = switcher.get(type(op), None)
        if func != None:
            func(op, scope_stack)

    @staticmethod
    def add_variable(op : ops.Declaration, scope_stack):
        """
        Handle variable declaration
        """
        father = scope_stack[-1][1]
        father.get_var_map()[op.get_name()] = [InitialScan.next_var_id, op.get_type()]
        InitialScan.next_var_id += 1

    @staticmethod
    def locate_variable(op : ops.Variable, scope_stack_ref):
        """
        Handle variable refrencing
        """
        scope_stack = scope_stack_ref.copy()
        InitialScan.locate_variable_rec(op, scope_stack)

    @staticmethod
    def locate_variable_rec(op : ops.Variable, scope_stack):
        """
        locate variable scope and id according to name, scope.
        """
        if len(scope_stack) == 0:  # catch variable used before it's decleration
            raise SyntaxError('Variable definition for \'' + op.get_name() + '\' not found.')
        current_node = scope_stack.pop()[1]

        if op.get_name() in current_node.get_var_map().keys():
            result = current_node.get_var_map()[op.get_name()]
            op.set_id(result[0])
            op.set_type(result[1])
        else:
            InitialScan.locate_variable_rec(op, scope_stack)
        

    @staticmethod
    def scan_assign(op : ops.Assign, scope_stack):
        """
        Handle Assign Operation
        """
        # locate var assigned to
        InitialScan.action_switcher(op.get_var(), scope_stack)
        # check for variables in value used
        InitialScan.action_switcher(op.get_assign(), scope_stack)

    @staticmethod
    def scan_definition(op : ops.Definition, scope_stack):
        """
        Save function parameters to temporary variable; Scope not yet created.
        """
        for arg in op.get_args():
            InitialScan.params.append([arg.get_name(), InitialScan.next_var_id, arg.get_type()])
            InitialScan.next_var_id += 1
    
    @staticmethod
    def add_params(op : OpSeqNode):
        """
        Add function variables to it's scope.
        """
        for name, var_id, var_type in InitialScan.params:
            op.get_var_map()[name] = [var_id, var_type]
        InitialScan.params.clear()
    
    @staticmethod
    def scan_call(op : ops.Call, scope_stack):
        """
        Handle Call Operation
        """
        # check for variables in function arguments
        for arg in op.get_args():
            InitialScan.action_switcher(arg, scope_stack)
        InitialScan.functions_to_parse.add(op.get_name())

    @staticmethod
    def scan_expression(op : ops.Expression, scope_stack):
        """
        Handle Expression Operation
        """
        InitialScan.action_switcher(op.get_tree(), scope_stack)

    @staticmethod
    def scan_expression_rec(op : ops.Expression.ExprNode, scope_stack):
        """
        Handle ExprNode tree
        """
        InitialScan.action_switcher(op.get_left(), scope_stack)
        InitialScan.action_switcher(op.get_right(), scope_stack)

    @staticmethod
    def scan_condition(op : ops.Condition, scope_stack):
        """
        Handle Condition Operation
        """
        InitialScan.action_switcher(op.get_tree(), scope_stack)

    @staticmethod
    def scan_condition_rec(op : ops.Condition.CondNode, scope_stack):
        """
        Handle CondNode tree
        """
        InitialScan.action_switcher(op.get_left(), scope_stack)
        InitialScan.action_switcher(op.get_right(), scope_stack)

    @staticmethod
    def scan_return(op : ops.Return, scope_stack):
        """
        Handle Return Operation
        """
        InitialScan.action_switcher(op.get_ret(), scope_stack)

    @staticmethod
    def scan_if(op : ops.If, scope_stack):
        """
        Handle If Operation
        """
        InitialScan.action_switcher(op.get_condition(), scope_stack)

    @staticmethod
    def parse_call_trees(code_map : CodeMap):
        """
        Parsing called function's trees.
        """
        # copy global lists
        functions_to_parse = InitialScan.functions_to_parse.copy()
        # check if parser was initialized
        if Parser.code_lexer != None and Parser.factory != None:
            # for any function name in the list
            for func in InitialScan.functions_to_parse:
                # check if already parsed
                if not code_map.is_exists(func):
                    # for every library that was imported
                    for lib in code_map.get_imports():
                        lexemes, flag, line_offset = lib.search(func, Parser) # get function lexemes
                        if flag: # if found
                            # parse and insert
                            Parser.insert_operations_to_code_map(code_map, lexemes, line_offset, lib.get_file())
                            # update lists
                            functions_to_parse.remove(func)
                            break
            # update
            InitialScan.functions_to_parse = functions_to_parse
            


class Parser():
    code_lexer : Lexer = None
    factory : OperationFactory = None
    # code parser class
    @staticmethod
    def parse(code : str, lang : str, filename : str) -> CodeMap:
        """
        parses the code to a code map.
        :param code: code text
        :param lang: programming language specifier
        :type code: str
        :type lang: str
        :return: mapped code object
        :rtype: CodeMap
        """
        line_counter = 1
        Parser.code_lexer = Lexer(lang)
        lexemes = utils.ignore_lexemes(Parser.code_lexer.analyze(code), ["comment"]) # find lexemes and ignore comments
        code_map = CodeMap(filename)
        Parser.factory = OperationFactory(Parser.code_lexer, lang)

        Parser.insert_operations_to_code_map(code_map, lexemes, line_counter)

        InitialScan.parse_call_trees(code_map)
        return code_map

    @staticmethod
    def insert_operations_to_code_map(code_map : CodeMap, lexemes : list, line_counter : int, filename : str = None):
        for lexeme in lexemes:
            if lexeme["type"] == "endl":
                line_counter += 1
            else:
                code_map.insert(Parser.factory.produce(lexeme), line_counter, filename) # insert operation to map