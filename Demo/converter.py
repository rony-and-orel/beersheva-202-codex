import operations as ops
from parse import CodeMap
from reader import Reader

class Converter():
    @staticmethod
    def translate(code_map : CodeMap, lang : str) -> str:
        convert_obj = {
            "java": toJava
        }[lang](code_map)
        return convert_obj.convert()

class toJava():
    def __init__(self, code_map : CodeMap):
        self.__code_map = code_map
    
    def convert(self) -> str:
        scanner = Reader(self.__code_map)
        scope_count = 0
        result = ''

        depth  = lambda : scope_count * '\t'

        while True:
            try:
                opSeqNode, flag = scanner.step(do_automatic_jump=True)
                
                # add opening bracket
                if flag == Reader.state.AT_SCOPES_START:
                    scope_count += 1
                    result += ' {\n'
                    

                # add operation
                if flag == Reader.state.ONE_LINER and type(opSeqNode.get_op()) != ops.Definition:
                    result += ' {\n' + depth() + '\t' + self.stringify(opSeqNode.get_op())
                    if not opSeqNode.has_scope():
                        result += ';\n'
                    result += depth() + '}\n'
                else:
                    result += depth() + self.stringify(opSeqNode.get_op())
                    if not opSeqNode.has_scope():
                        result += ';\n'
                
                # add closing bracket
                if flag == Reader.state.AT_SCOPES_END:
                    scope_count -= 1
                    result += depth() + '}\n'

            except StopIteration:
                break

        return result
    
    @staticmethod
    def stringify(op):
        options = {
            ops.Assign : lambda op: toJava.stringify(op.get_var()) + ' = ' + toJava.stringify(op.get_assign()),
            ops.Variable : lambda op: op.get_name(),
            ops.Value : lambda op: str(op.get_val()),
            ops.Declaration : lambda op: toJava.stringify_declaration(op),
            ops.Expression : lambda op: toJava.stringify(op.get_tree()),
            ops.Expression.ExprNode : lambda op: toJava.stringify(op.get_left()) + ' ' + toJava.stringify(op.get_op()) + ' ' + toJava.stringify(op.get_right()),
            ops.Mathematic: lambda op: toJava.stringify_mathematic(op),
            ops.If : lambda op: 'if(' + toJava.stringify(op.get_condition()) + ')',
            ops.Else: lambda op: 'else',
            ops.Condition : lambda op: toJava.stringify(op.get_tree()),
            ops.Condition.CondNode : lambda op: toJava.stringify(op.get_left()) + ' ' + toJava.stringify(op.get_op()) + ' ' + toJava.stringify(op.get_right()),
            ops.Booleanic : lambda op: toJava.stringify_booleanic(op),
            ops.Definition : lambda op: 'public static ' + op.get_type() + ' ' + op.get_name() + '(' + ', '.join(toJava.stringify(arg) for arg in op.get_args()) + ')',
            ops.Call : lambda op: op.get_name() + '(' + ', '.join(toJava.stringify(arg) for arg in op.get_args()) + ')',
            ops.Return : lambda op: 'return ' + toJava.stringify(op.get_ret()),
        }
        func = options.get(type(op))
        if func != None:
            return func(op)
        # return str(op)
        raise Exception('Untranslated operator.')
    
    @staticmethod
    def stringify_declaration(op : ops.Declaration):
        decl_type = op.get_type()
        decl_type = decl_type.replace('char*', 'String', 1)
        decl_type = decl_type.replace('*', '[]')
        return decl_type + ' ' + op.get_name()

    @staticmethod
    def stringify_booleanic(op):
        types = {
            'or': '||',
            'and': '&&',
            'equal': '==',
            'lesser equal': '<=',
            'greater equal': '>=',
            'lesser': '<',
            'greater': '>',
            'not equal': '!=',
        }
        return types.get(op.get_type())
    
    @staticmethod
    def stringify_mathematic(op):
        types = {
            'add': '+',
            'sub': '-',
            'mul': '*',
            'div': '/',
            'xor': '^',
            'and': '&',
            'or': '|',
            'shl': '<<<',
            'shr': '>>>',
            'mod': '%',
            }
        return types.get(op.get_type())