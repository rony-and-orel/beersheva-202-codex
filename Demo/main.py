from parse import Parser
from reader import Reader
from autochecks import checks, Reporter
from converter import Converter

import os

def main():
    # read code sample
    with open('code_sample.c', 'r') as f:
        code_sample = f.read()
    Reporter.__init__()

    print("\nParsing code_sample.c...")
    code_map = Parser.parse(code_sample, "c", 'code_sample.c') # parse code
    print("Code parsing completed!\n")

    while True:
        print("""Welcome to Codex Demo!\nYou can:
        1. check 'code_sample.c'
        2. translate 'code_sample.c' to java
        3. show code mapping view
        4. exit""")
        option = input("Choose wisely: ")
        print()
        if option == "1":
            for check in checks:
                check(code_map)
            print("checkout 'report.txt' to see the results!")
        elif option == "2":
            with open("code_sample.java", "w") as f:
                f.write(Converter.translate(code_map, "java"))
            print("checkout 'code_sample.java' to see the results!")
        elif option == "3":
            print(code_map)
        elif option == "4":
            print("Goodbye! :)")
            break
        else:
            print("404: option not found :(")
        input("press enter to continue...")
        os.system('cls' if os.name=='nt' else 'clear')

    

if __name__ == '__main__':
    main()
