from parse import CodeMap, OpSeqNode
from enum import Enum

class Reader():
    class state(Enum):
        AT_SCOPES_START = 1
        IN_SCOPE = 0
        AT_SCOPES_END = -1
        ONE_LINER = 2
        BEYOND_BOUND = 3
    
    def __init__(self, code_map : CodeMap):
        self.__tree_map = code_map.get_func_map() # save function tree map
        self.__map_iter = iter(self.__tree_map) # initiate map iterator
        self.__cursor = 0 # position in current sequence
        self.__current_tree = self.__tree_map[next(self.__map_iter)] # set first function tree
        self.__current_seq = self.__current_tree.get_root() # set current sequance as root
        self.__scope_stack = []
        self.__tree_stack = [] # function tree stack (used as stack call and as memoization)
        self.__already_scanned = [] # blacklist for trees which were already scanned

    def step(self, skip_scope=False, do_automatic_jump=False) -> [OpSeqNode, Enum]:
        """
        do a step to the next operation in the sequence.
        :param skip_scope: skip scopes flag
        :param do_automatic_jump: do an automatic jump to the next function tree (flag)
        :type skip_scope: bool
        :type do_automatic_jump: bool
        :return: operation, scope flag
        :rtype: OpSeqNode, int
        """
        op = None
        flag = None
        if 0 <= self.__cursor < len(self.__current_seq): # if still in current sequence
            op = self.__current_seq[self.__cursor] # get current node

            if op.has_scope() and not skip_scope:
                flag = self.__get_flag()
                # get into scope
                self.__scope_stack.append((self.__current_seq, self.__cursor))
                self.__current_seq = op.get_seq()
                self.__cursor = 0 
            else:
                flag = self.__get_flag()
                self.__cursor += 1 # move forward
        else:
            if self.__scope_stack:
                # get back to parent's position
                self.__current_seq, self.__cursor = self.__scope_stack.pop()
                flag = self.__get_flag()
                self.__cursor += 1
            else:
                self.__cursor = len(self.__current_seq) # set position to the end of the sequence
                if do_automatic_jump:
                    self.jump() # iterate to the next tree
                else:
                    raise StopIteration("reached to the end of the sequence") # when reached bound
            op, flag = self.step(skip_scope=skip_scope, do_automatic_jump=do_automatic_jump) # do a step
        return op, flag
    
    def __get_flag(self) -> Enum:
        """
        returns flag according to cursor's position.
        :return: flag
        :rtype: Enum
        """
        flag = Reader.state.BEYOND_BOUND
        cursor = self.__cursor
        
        if cursor == 0 and cursor == len(self.__current_seq) - 1: # if one line operation
            flag = Reader.state.ONE_LINER
        elif cursor == 0: # if at start
            flag = Reader.state.AT_SCOPES_START
        elif cursor == len(self.__current_seq) - 1: # if at end
            flag = Reader.state.AT_SCOPES_END
        elif 0 < cursor < len(self.__current_seq) - 1: # if in middle of scope
            flag = Reader.state.IN_SCOPE

        return flag

    def back(self, skip_scope=False, do_automatic_ret=False) -> OpSeqNode:
        """
        do a step to the previous operation in the sequence.
        :param skip_scope: skip scopes flag
        :param do_automatic_ret: do an automatic return to the previous function tree (flag) 
        :type skip_scope: bool
        :type do_automatic_ret: bool
        :return: operation, scope flag
        :rtype: OpSeqNode, int
        """
        op = None
        if 0 <= self.__cursor < len(self.__current_seq): # if still in current sequence
            op = self.__current_seq[self.__cursor] # get current node

            if op.has_scope() and not skip_scope:
                # get into scope
                self.__scope_stack.append((self.__current_seq, self.__cursor))
                self.__current_seq = op.get_seq()
                self.__cursor = len(self.__current_seq) - 1
            else: 
                self.__cursor -= 1 # move backwards
        else:
            if self.__scope_stack:
                # get back to parent's position
                self.__current_seq, self.__cursor = self.__scope_stack.pop()
                self.__cursor -= 1
            else:
                self.__cursor = 0 # set position to the start of the sequence
                if do_automatic_ret:
                    self.ret()
                else:
                    raise StopIteration("reached to the start of the sequence") # when reached bound
            op = self.back(skip_scope=skip_scope, do_automatic_ret=do_automatic_ret) # go back

        return op
    
    def jump(self, func=None):
        """
        jump to a function tree.
        :param func: function key name
        :type func: str
        :return: None
        """
        self.__tree_stack.append((self.__current_tree, self.__current_seq, self.__scope_stack, self.__cursor)) # save values in stack
        if func == None:
            try:
                # iterate to the next function
                key = next(self.__map_iter)
                while(key in self.__already_scanned): # find a function that wasn't scanned
                    key = next(self.__map_iter)
                self.__current_tree = self.__tree_map[key] # set current function tree
                self.__already_scanned.append(key) # save to blacklist
            except Exception:
                raise StopIteration("reached to the end of function tree map")
        else:
            try:
                self.__current_tree = self.__tree_map[func]
                self.__already_scanned.append(func) # save to blacklist
            except Exception:
                raise KeyError("function not found")
        # set
        self.__cursor = 0
        self.__current_seq = self.__current_tree.get_root()

    def ret(self):
        """
        return to the previous function tree.
        :return: None
        """
        if self.__tree_stack:
            self.__current_tree, self.__current_seq, self.__scope_stack, self.__cursor = self.__tree_stack.pop() # retreive old values
        else:
            raise StopIteration("reached to the start of function tree map")