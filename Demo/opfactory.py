import regexes_for_c as regC
import operations as ops

class OperationFactory():
    # initializer for C language
    class __forC():
        def __init__(self, lexer):
            self.__lexer = lexer

        def parse_condition(self, match : str) -> ops.Condition.CondNode:
            """
            parse condition operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: Condition.CondNode
            """
            oplst = []
            switcher = {
                "expr": (ops.Expression, self.parse_expression),
                "op": (ops.Booleanic, self.parse_booleanic)
            }
            patterns = (
                (regC.BOOLEAN_PATTERN, "op"),
                (regC.EXPRESSION_PATTERN, "expr"),
            )

            for lexeme in self.__lexer.find_lexemes(match, patterns):
                op, parser = switcher[lexeme["type"]]
                oplst.append(op(parser(lexeme["match"])))

            bool_operations = [['equal', 'lesser equal', 'greater equal', 'not equal'], ['and', 'or']]

            for op_level in bool_operations:
                # parse the lexeme list with the given operands in op_level (parsing according to boolean operations order)
                i = 0
                while i < len(oplst) - 1:
                    if isinstance(oplst[i + 1], ops.Booleanic) and oplst[i + 1].get_type() in op_level:
                        oplst[i] = ops.Condition.CondNode(oplst[i], oplst[i + 1], oplst[i + 2])
                        oplst.pop(i + 1)
                        oplst.pop(i + 1)
                        i -= 1
                    i += 1
            return oplst[0]

        def parse_expression(self, match : str) -> ops.Expression.ExprNode:
            """
            parse expression operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: Expression.ExprNode
            """
            oplst = []
            switcher = {
                "val": (ops.Value, self.parse_value),
                "var": (ops.Variable, self.parse_var),
                "op": (ops.Mathematic, self.parse_mathematic),
                "call": (ops.Call, self.parse_call),
                "mem": (ops.Mem, self.parse_memory)
            }
            patterns = (
                (regC.FUNCTION_PATTERN, "call"),
                (regC.BINARY_PATTERN, "op"),
                (regC.ARITHMTIC_PATTERN, "op"),
                (regC.MEM_PATTERN, "mem"),
                (regC.VARIABLE_PATTERN, "var"),
                (regC.INT_PATTERN, "val"),
                (regC.HEX_PATTERN, "val"),
                (regC.DOUBLE_PATTERN, "val"),
                (regC.CHAR_PATTERN, "val")
            )

            for lexeme in self.__lexer.find_lexemes(match, patterns):
                op, parser = switcher[lexeme["type"]]
                oplst.append(op(parser(lexeme["match"])))

            math_operations = [['mul', 'div', 'mod', 'or', 'xor', 'and', 'shr', 'shl'], ['add', 'sub']]

            for op_level in math_operations:
                # parse the lexeme list with the given operands in op_level (parsing according to math operations order)
                i = 0
                while i < len(oplst) - 1:
                    if isinstance(oplst[i + 1], ops.Mathematic) and oplst[i + 1].get_type() in op_level:
                        oplst[i] = ops.Expression.ExprNode(oplst[i], oplst[i + 1], oplst[i + 2])
                        oplst.pop(i + 1)
                        oplst.pop(i + 1)
                        i -= 1
                    i += 1
            return oplst[0]

        def parse_definition(self, match : str) -> dict:
            """
            parse definition operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: dict
            """
            patterns = (
                (regC.RTYPE_PATTERN, "rtype"),
                (regC.FUNCTION_NAME_PATTERN, "name"),
                (regC.DECLERATION_PATTERN, "arg")
            )
            lexemes = self.__lexer.find_lexemes(match, patterns) # classify
            # parse to dict
            return {
                "rtype": lexemes[0]["match"],
                "name": lexemes[1]["match"],
                "args": [ops.Declaration(self.parse_decleration(lex["match"])) for lex in lexemes[2:]]
            }

        def parse_decleration(self, match : str) -> dict:
            """
            parse decleration operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: dict
            """
            return {
                "type": match.split(' ')[0],
                "name": match.split(' ')[1]
            }

        def parse_var(self, match : str) -> str:
            """
            parse variable operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: str
            """
            return match

        def parse_assign(self, match : str) -> dict:
            """
            parse assign operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: dict
            """
            patterns = (
                (regC.STR_PATTERN, "str"),
                (regC.CONDITION_PATTERN, "cond"),
                (regC.EXPRESSION_PATTERN, "expr")
            )
            switcher = {
                "expr": (ops.Expression, self.parse_expression),
                "cond": (ops.Condition, self.parse_condition),
                "str": (ops.Value, self.parse_value),
            }

            lexemes = self.__lexer.find_lexemes(match, patterns)
            builder, parser = switcher[lexemes[1]["type"]]
            return {
                "var": ops.Variable(lexemes[0]["match"]),
                "assign": builder(parser(lexemes[1]["match"]))
            }

        def parse_scope(self, match : str) -> bool:
            """
            parse scope operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: bool
            """
            return match[0] == "{"

        def parse_value(self, match : str) -> [hex, int, str, float]:
            """
            parse value operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: hex, int, str, float
            """
            patterns = (
                (regC.STR_PATTERN, "str"),
                (regC.HEX_PATTERN, "hex"),
                (regC.DOUBLE_PATTERN, "double"),
                (regC.CHAR_PATTERN, "char"),
                (regC.INT_PATTERN, "int")
            )
            vals = {
            "hex": hex,
            "int": int,
            "char": str,
            "double": float,
            "str": str
            }
            return vals[self.__lexer.find_lexemes(match, patterns)[0]["type"]](match)

        def parse_if(self, match : str) -> dict:
            """
            parse if operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: dict
            """
            match = match[3:-1] # cut
            patterns = (
                (regC.CONDITION_PATTERN, "cond"),
                (regC.EXPRESSION_PATTERN, "expr")
            )

            return self.parse_condition(self.__lexer.find_lexemes(match, patterns)[0]["match"])

        def parse_else(self, match : str):
            """
            parse else operation.
            :param match: lexeme match
            :type match: str
            :return: None
            """
            return None

        def parse_mathematic(self, match : str) -> str:
            """
            parse mathematic operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: str
            """
            types = {
                "+": "add",
                "-": "sub",
                "*": "mul",
                "/": "div",
                "^": "xor",
                "&": "and",
                "|": "or",
                "<<": "shl",
                ">>": "shr",
                "%": "mod"
            }
            return types[match]

        def parse_booleanic(self, match : str) -> str:
            """
            parse booleanic operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: str
            """
            types = {
                "||": "or",
                "&&": "and",
                "==": "equal",
                "<=": "lesser equal",
                "=>": "greater equal",
                "<": "lesser",
                ">": "greater",
                "!=": "not equal"
            }
            return types[match]

        def parse_return(self, match : str) -> ops.Operation:
            """
            parse return operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: Operation
            """
            match = match[len("return") + 1:] # cut
            patterns = (
                (regC.CONDITION_PATTERN, "cond"),
                (regC.EXPRESSION_PATTERN, "expr"),
            )
            switcher = {
                "expr": (ops.Expression, self.parse_expression),
                "cond": (ops.Condition, self.parse_condition),
            }

            lexemes = self.__lexer.find_lexemes(match, patterns)
            builder, parser = switcher[lexemes[0]["type"]]
            return builder(parser(lexemes[0]["match"]))

        def parse_memory(self, match : str) -> dict:
            """
            parse memory operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: dict
            """
            return {
                "type": ("get_address" if match[0] == '&' else "get_value"),
                "var": ops.Variable(self.parse_var(match[1:]))
            }

        def parse_call(self, match : str) -> dict:
            """
            parse call operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: dict
            """
            patterns = (
                (regC.STR_PATTERN, "str"),
                (regC.CONDITION_PATTERN, "cond"),
                (regC.EXPRESSION_PATTERN, "expr")
            )
            switcher = {
                "expr": (ops.Expression, self.parse_expression),
                "cond": (ops.Condition, self.parse_condition),
                "str": (ops.Value, self.parse_value),
            }
            name = match.split('(', 1)[0]
            match = match.split('(', 1)[1][:-1]
            lexemes = self.__lexer.find_lexemes(match, patterns)

            return {
                "name": name,
                "args": [switcher[arg["type"]][0](switcher[arg["type"]][1](arg["match"])) for arg in lexemes]
            }

        def parse_import(self, match : str) -> dict:
            """
            parse import operation.
            :param match: lexeme match
            :type match: str
            :return: parsed data structure
            :rtype: dict
            """
            return {
                "file": match.replace('#include', '').replace('"', '').replace(" ", '').replace('<', '').replace('>', ''),
                "search_regex": regC.FUNCTION_CODE
            }

    def __init__(self, lexer, lang : str):
        self.__initobj = {
            "c": OperationFactory.__forC
        }[lang](lexer) # select the initializer that matches the programming language

    def produce(self, lexeme : list) -> tuple:
        """
        matches the lexemes stream to an operation and creates it.
        :param lexemes: stream of lexemes
        :type lexemes: lst
        :return: matched operation instance, flag
        :rtype: tuple
        """
        builder, parser = None, None
        # operation builder and parser switcher
        type_switch = {
           "if" : (ops.If, self.__initobj.parse_if),
           "scope" : (ops.Scope, self.__initobj.parse_scope),
           "import" : (ops.Import, self.__initobj.parse_import),
           "decleration" : (ops.Declaration, self.__initobj.parse_decleration),
           "definition" : (ops.Definition, self.__initobj.parse_definition),
           "return" : (ops.Return, self.__initobj.parse_return),
           "assign" : (ops.Assign, self.__initobj.parse_assign),
           "else" : (ops.Else, self.__initobj.parse_else),
           "condition" : (ops.Condition, self.__initobj.parse_condition),
           "call" : (ops.Call, self.__initobj.parse_call),
           "expression" : (ops.Expression, self.__initobj.parse_expression),
           "mem" : (ops.Mem, self.__initobj.parse_memory)
        }

        try:
            builder, parser = type_switch[lexeme["type"]]
        except Exception:
            raise BaseException("could not classify lexeme >> " + str(lexeme))

        return builder(parser(lexeme["match"]))