"""
Utilities.
"""

def split_lexemes_by_element(lexemes, typof):
    """
    splits list of lexemes by type of lexeme.
    :param lexemes: stream of lexemes
    :type lexemes: lst
    :param typof: type of lexeme
    :type typof: str
    :return: splitted list
    :rtype: lst
    """
    current = []
    splitted = []
    for lex in lexemes:
        if lex["type"] == typof:
            splitted.append(current)
            current = []
        else:
            current.append(lex)
    splitted.append(current)
    return splitted

def ignore_lexemes(lexemes, types):
    """
    ignores spesific lexemes by type.
    :param lexemes: stream of lexemes
    :type lexemes: lst
    :param types: types of lexeme to ignore
    :type types: lst
    :return: filtered list
    :rtype: lst
    """
    flag = False
    filtered = []
    for lex in lexemes:
        flag = True
        for typeof in types:
            if lex["type"] == typeof:
                flag = False
        if flag:
            filtered.append(lex)
    return filtered