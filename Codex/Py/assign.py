x = 5
x=5

test, x = 6, 8
test,x=6,8

test = t
test=t

# for now unable to parse expression
#m = ((1+1) - 5) * ((((5))))
#m=((1+1)-5)*5

a,b,c,d=a,t,u,c
a, b, c, d = a, t, u, c
a,              b, c, d = a,      t, u   , c

a,b,c,d=a,t,u,c
a, _, c, d = _, t, u, c

x += 5 * u
x+=5*u

x -= 5 * u
x-=5*u

x *= 5 * u
x*=5*u

x /= 5 * u
x/=5*u

x /= 5 * u
x/=5*u

x %= 5 * u
x%=5*u

y = 3 + 6 * 7 - 4 % 7 * (test + test2 % x - 5)
y = 3+6*7-4%7*(test+test2%x-5)

# '_' is a valid variable name
_ = u
_=u

_, _ = 2, 3
_,_=2, 4

_, _ = _, _
_,_=_,_