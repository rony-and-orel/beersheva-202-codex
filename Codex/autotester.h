#pragma once
#include <iostream>
#include <filesystem>
#include <fstream> 
#include <string>
#include "parser.h"
#include "viewer.h"
#include "debugging_util.h"

namespace codex
{
	namespace core
	{
		namespace debugging
		{
			class Autotester
			{
			public:
				// what components to test
				enum class TestLevel
				{
					REGEXES = 0,
					LEXER,
					FACTORY,
					ALL,
				};

				// which files to display to the user; only works if test level
				enum class Interactivity
				{
					NONE = 0,
					VALID,
					ALL,
				};

			private:
				static std::pair<std::string, bool> testRegexes(const std::vector<lexical::Regex>& regexes)
				{
					std::pair<std::string, bool> output;
					output.second = true;

					std::unordered_map<lexical::LexemeType, const lexical::Regex&> map;

					for (const auto& regex : regexes)
					{
						auto it = map.find(regex.type);
						if (it != map.end())
						{
							output.second = false;
							output.first += color::paint("Conflict: ", color::TextType::Bad) + it->second.to_string() + ", " + regex.to_string() + "\n";
						}
						else
						{
							map.emplace(regex.type, regex);
						}
					}
					return output;
				}

				static std::pair<std::string, bool> testLexemes(const std::vector<lexical::Lexeme>& lexemes, size_t fileLength)
				{
					std::pair<std::string, bool> output;
					output.second = true;

					// count all areas detected by regexes:
					if (lexemes.size() != 0)
					{
						for (size_t i = 0; i < lexemes.size() - 1; i++)
						{
							if (lexemes[i].getSpan()[1] - lexemes[i].getSpan()[0] > 0 && lexemes[i].getSpan()[1] > lexemes[i + 1].getSpan()[0])
							{
								output.second = false;
								output.first += color::paint("Conflict: ", color::TextType::Bad) + lexemes[i].to_string() + ", " + lexemes[i + 1].to_string() + "\n";
							}
						}
					}
					return output;
				}

				static std::pair<std::string, bool> testFactory(const factories::io::FactoryOutput& produce)
				{
					std::pair<std::string, bool> output;
					output.second = produce.success;
					if (output.second)
					{
						for (auto& op : produce.ops)
						{
							if (op.getData() == nullptr)
							{
								output.first += color::paint("Found operation with null data\n", color::TextType::Bad);
								output.second = false;
							}
							else if (op.getData()->getType() == operations::OperationType::ERROR)
							{
								output.first += ual::Translator::convertOperation(op).first + '\n';
								output.second = false;
							}
						}
					}
					return output;
				}

				static void testTranslator(const std::filesystem::path& filePath, parsing::ProjectInfo& projInfo, std::string& output, bool interactive)
				{
					std::string filename(filePath.filename().string());

					util::cls();
					auto map = parsing::Parser::compile(projInfo);
					auto translated = debugging::view(map.code);
					if (translated.second)
					{
						if (interactive)
						{
							std::cout << "Currently viewing file " + color::paint(filename, color::ColorCode::FG_Green_Bright) + "\n\n";
							std::cout << translated.first << "\n";
							std::cout << color::paint("Is the output correct? y/n ", color::TextType::Question);

							if (util::getYesNoInput())
							{
								output += color::paint("Success", color::TextType::Good);
							}
							else
							{
								output += color::paint("Manually marked as error", color::TextType::Bad);
							}
						}
					}
					else
					{
						output += color::paint("Debugging translator unable to serialize code", color::TextType::Bad);
					}
				}


				static std::string testFile(std::filesystem::path filePath, languages::LanguageCode lang, TestLevel testLevel, Interactivity interactive)
				{
					std::string output;

					parsing::ProjectInfo projInfo{ lang, filePath.string(), util::readFile(filePath) };

					// test the Lexer output
					auto lexemes = lexical::Lexer::analyze(projInfo.content, lexical::getDefaultRegexSet(projInfo.language));

					auto lexemeTestResult = testLexemes(lexemes, projInfo.content.size());
					if (!lexemeTestResult.second)
					{
						output += color::paint("Lexer: ", color::TextType::Bad) + '\n' + lexemeTestResult.first;
					}
					else if(testLevel > TestLevel::LEXER)
					{
						// test the factory output
						auto produced = factories::MainFactory::produce(projInfo.language, lexemes, projInfo.path, projInfo.content);

						auto facTestResult = testFactory(produced);
						if (!facTestResult.second)
						{
							output += color::paint("Factory: ", color::TextType::Bad) + '\n' + facTestResult.first;
						}
						else if (testLevel > TestLevel::FACTORY && interactive >= Interactivity::VALID && (interactive == Interactivity::ALL || facTestResult.second))
						{
							testTranslator(filePath, projInfo, output, true);
						}
						else
						{
							output += color::paint("Success", color::TextType::Good);
						}
						if (testLevel > TestLevel::FACTORY)
						{
							testTranslator(filePath, projInfo, output, false);
						}
					}
					else
					{
						output += color::paint("Success", color::TextType::Good);
					}
					return output;
				}
			public:
				/*
				run the code autotester on a folder of test files
				input: language to test, relative path to the directory with all test files,
					if the program should ask the user for feedback, if iterate on all files or have the user choose a file
				output: none.
				*/
				static void run(languages::LanguageCode lang, const std::string& directory, TestLevel testLevel, Interactivity interactive, bool chooseFile)
				{
					const std::filesystem::path workPath = std::filesystem::current_path().append(directory);
					std::string output(color::paint("Summery:\n", color::ColorCode::FG_Cyan_Bright)); //output to print to screen at end of operation

					// test regexes
					auto regexTestResult = testRegexes(lexical::getDefaultRegexSet(lang));
					if (!regexTestResult.second)
					{
						output += color::paint("Regexes: ", color::TextType::Bad) + "\n" + regexTestResult.first + "\n";
					}

					if (testLevel > TestLevel::REGEXES)
					{
						// make sure path is a folder
						if (std::filesystem::is_directory(workPath))
						{
							if (chooseFile)
							{
								std::filesystem::path filePath = util::getFileInput(workPath);
								output += filePath.filename().string() + " - " + testFile(filePath, lang, testLevel, interactive) + '\n';
							}
							else
							{
								// get max length of file/dir name
								size_t maxSize = 0;
								for (const auto& entry : std::filesystem::directory_iterator(workPath))
								{
									size_t size = entry.path().filename().string().size();
									if (size > maxSize)
									{
										maxSize = size;
									}
								}

								// test entries in directory
								for (const auto& entry : std::filesystem::directory_iterator(workPath))
								{
									std::string filename(entry.path().filename().string());
									filename.resize(maxSize); //safe as max filename length is already known
									output += filename + " - ";

									// if entry is file
									if (!std::filesystem::is_directory(entry))
									{
										output += testFile(entry.path(), lang, testLevel, interactive);
									}
									else
									{
										output += color::paint("Not a file", color::TextType::Bad);
									}
									output += '\n';
								}
							}
						}
						else
						{
							output += color::paint("Autotester failed! specified path does not lead to a folder\n", color::TextType::Bad);
						}
					}

					util::cls();
					std::cout << output;
				}
			};
		}
	}
}