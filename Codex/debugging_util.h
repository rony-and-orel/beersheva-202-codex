#pragma once
#include <string>
#include <iterator>
#include <filesystem>
#include <iostream>
#include <fstream>
#include "color.h"

namespace codex
{
	namespace core
	{
		namespace debugging
		{
			namespace util
			{
				const std::string std_invalid_msg = color::paint("Invalid input! Please try again: ", color::TextType::Bad);

				/*
				Returns a value only when it is a number and when: min <= x <= max.
				input: min and maximum values.
				output: value.
				*/
				static int getValidInput(int min, int max)
				{
					int value = 0;
					bool valid = false;

					do // while invalid input
					{
						std::cin >> value;
						if (std::cin.fail())
						{
							std::cin.clear();
							std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
							std::cout << std_invalid_msg;
						}
						else if (value <= max && value >= min)
						{
							valid = true;
						}
						else
						{
							std::cout << std_invalid_msg;
						}
					} while (!valid);
					return value;
				}

				static bool getYesNoInput()
				{
					char ch;
					bool valid = false;
					do // while invalid input
					{
						std::cin >> ch;
						if (std::cin.fail())
						{
							std::cin.clear();
							std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
						}
						if (ch == 'y' || ch == 'n')
						{
							valid = true;
						}

						if (!valid)
						{
							std::cout << std_invalid_msg;
						}
					} while (!valid);

					return ch == 'y';
				}

				// clear screen
				static void cls()
				{
				#if defined _WIN32
					system("cls");
				#elif defined (__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
					system("clear");
				#endif
				}

				static std::string readFile(const std::filesystem::path& filePath)
				{
					std::ifstream fileData(filePath);
					return std::string((std::istreambuf_iterator<char>(fileData)), std::istreambuf_iterator<char>());
				}

				/*
				Choose a file from a directory.
				input: path to directory.
				output: file path; received path if it is invalid.
				*/
				static std::filesystem::path getFileInput(const std::filesystem::path& workPath)
				{
					std::filesystem::path result;

					if (std::filesystem::is_directory(workPath))
					{
						int count = 0, op = 0;
						std::string output(color::paint("Avaliable files:\n", color::ColorCode::FG_Cyan_Bright));
						std::vector<std::filesystem::directory_entry> entries;

						for (const auto& entry : std::filesystem::directory_iterator(workPath))
						{
							if (!std::filesystem::is_directory(entry))
							{
								output += std::to_string(count) + " - " + entry.path().filename().string() + '\n';

								entries.push_back(entry);
								count++;
							}
						}
						if (count == 0)
						{
							std::cout << color::paint("No files found in given path\n", color::TextType::Bad);
						}
						else if (count == 1)
						{
							std::cout << color::paint("Chosen only avaliable file: " + entries[0].path().filename().string() + "\n", color::TextType::Good);
							result = std::move(entries[0]);
						}
						else
						{
							std::cout << output;
							std::cout << color::paint("Please choose a file: ", color::TextType::InstructionWithInput);
							op = getValidInput(0, count - 1);
							result = std::move(entries[op]);
						}
					}
					else
					{
						std::cout << "Invalid path: " << workPath << std::endl;
					}
					return result;
				}
			}
		}
	}
}