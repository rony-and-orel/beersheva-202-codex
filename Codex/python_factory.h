#pragma once
#include <unordered_map>
#include <list>
#include <vector>
#include <algorithm>
#include <exception>
#include <string>
#include <functional>

#include "factory_io.h"
#include "mapping.h"

#include "lexeme.h"

#define IDENTATION_MULTIPLE 8
#define QOUTE_AMOUNT_MULTILINE 3

namespace codex::core::factories::python
{
	class Factory
	{
	public:
		/*
		Produces a list of operations by given lexemes.
		input: lexemes, in.filename and file content.
		output: operation list, list of comment references.
		*/
		static io::FactoryOutput produce(const std::vector<lexical::Lexeme>& lexemes, std::string_view filename, const std::string_view fileContent)
		{
			auto result = io::FactoryOutput();
			io::python::ParserArgs args{ result.issues, lexemes, lexemes.begin(), filename, 1, fileContent, false };
			std::stack<size_t> identationLevels = std::stack<size_t>(); //identation levels stack trace
			size_t calculatedLevel = 0; //calculated identation level

			identationLevels.push(0); //init
			while (args.offset != lexemes.end())
			{
				if (Parsers::Recognizers::isEndline(lexemes.end(), args.offset))
				{
					args.lineCount++;
					args.offset++;
				}
				else if (Parsers::Recognizers::isComment(lexemes.end(), args.offset))
				{
					//save ref and insert to operation list
					result.comments.push_back(operations::Comment::cast(Parsers::parseComment(args.pass()).op));

					result.ops.emplace_back(std::unique_ptr<operations::OperationData>(result.comments.back()));
				}
				else if (Parsers::Recognizers::isSpace(lexemes.end(), args.offset))
				{
					args.offset++; //ignore additional spaces
				}
				else if (Parsers::Recognizers::isIdentation(lexemes.end(), args.offset))
				{
					calculatedLevel = getIdentationLevel(args.offset->getMatch()); //calculate level
					
					//check for unnecessary identations
					if ((calculatedLevel <= identationLevels.top() && args.needsScope) || 
						(calculatedLevel > identationLevels.top() && !args.needsScope))
					{
						args.issueBoard.submit(issues::Issue::Type::SYNTAX, issues::common::SCOPE_EXPECTED, args.offset->getSpan(), args.lineCount);
						result.ops.emplace_back(std::unique_ptr<operations::OperationData>(Parsers::parseError(args.pass()).op));
					}
					else
					{
						while (calculatedLevel != identationLevels.top()) //until not reached that level
						{
							if (calculatedLevel < identationLevels.top()) //close scopes
							{
								identationLevels.pop();
								result.ops.emplace_back(std::unique_ptr<operations::OperationData>(
									new operations::Scope(
										args.lineCount,
										args.filename,
										operations::Scope::Type::CLOSING_SCOPE
									))
								);
							}
							else if (calculatedLevel > identationLevels.top()) //open scopes
							{
								identationLevels.push(calculatedLevel);
								result.ops.emplace_back(std::unique_ptr<operations::OperationData>(
									new operations::Scope(
										args.lineCount,
										args.filename,
										operations::Scope::Type::OPENING_SCOPE
									))
								);
							}
						}
					}
					args.offset++; //advance
					args.needsScope = false; //reset flag
				}
				else
				{
					result.ops.emplace_back(parseOperation(args.pass())); //parse operation and append
				}
			}

			return result;
		}

	private:
		/*
		Calculates the identation level of an identation by the python 3.9 standard.
		input: identation (text).
		output: identation level.
		*/
		static size_t getIdentationLevel(std::string_view iden)
		{
			size_t level = 0;

			for (char space : iden)
			{
				if (space == ' ')
				{
					level++;
				}
				else
				{
					level += IDENTATION_MULTIPLE - (level % IDENTATION_MULTIPLE);
				}
			}

			return level;
		}

		/*
		Parses the next operation.
		input: lexemes, offset, in.filename, line counter and tab counter.
		output: parsed operation data.
		*/
		static std::unique_ptr<operations::OperationData> parseOperation(io::python::ParserInput in)
		{
			auto result = io::python::ParserOutput(); //the chosen one

			//select the next operation and parse it
			if (Parsers::Recognizers::isImport(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseImport(in);
			}
			else if (Parsers::Recognizers::isElse(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseElse(in);
			}
			else if (Parsers::Recognizers::isIf(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseIf(in);
			}
			else if (Parsers::Recognizers::isWhile(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseWhile(in);
			}
			else if (Parsers::Recognizers::isAssign(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseAssign(in);
			}
			else if (Parsers::Recognizers::isForeach(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseForeach(in);
			}
			else if (Parsers::Recognizers::isReturn(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseReturn(in);
			}
			else if (Parsers::Recognizers::isExpression(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseExpression(in, false);
			}
			else if (Parsers::Recognizers::isOperator(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseOperator(in);
			}
			else if (Parsers::Recognizers::isCall(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseCall(in);
			}
			else if (Parsers::Recognizers::isValue(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseValue(in);
			}
			else if (Parsers::Recognizers::isVariable(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseVariable(in);
			}
			else if (Parsers::Recognizers::isContinue(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseContinue(in);
			}
			else if (Parsers::Recognizers::isBreak(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseBreak(in);
			}
			else if (Parsers::Recognizers::isFunctionDefinition(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseFunctionDefinition(in);
			}
			else if (Parsers::Recognizers::isClassDefinition(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseClassDefinition(in);
			}
			else
			{
				in.issues.submit(issues::Issue::Type::UNSUPPORTED, issues::common::UNEXPECTED_TOKEN + "'" + std::string(in.offset->getMatch()) + "'", in.offset->getSpan(), in.lineCount);
				result = Parsers::parseError(in);
			}

			if (!result.success) //check if the parsing was unsuccessful
			{
				result = Parsers::parseError(in);
			}

			return std::unique_ptr<operations::OperationData>(result.op);
		}

		/* Operation Parsers */
		class Parsers
		{
		public:
			/* Operation Recognizers */
			class Recognizers
			{
			public:
				typedef const std::function<bool(const LexIter&, const LexIter&)>& RecoMethod;

				/*
				standard desciption of recognizers utility methods:
				checks if the upcoming lexemes are related to a specific operation or other things (like values, seperators or special lexemes).
				input: end, offset.
				output: is flag.
				*/
				static inline bool isValue(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (isString(endIndicator, offset) || isNumber(endIndicator, offset) || isListStart(endIndicator, offset) || isMapStart(endIndicator, offset) || isBoolValue(endIndicator, offset) || isEmptyValue(endIndicator, offset));
				}

				static bool isVariable(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (isIdentifier(endIndicator, offset) && !isCall(endIndicator, offset));
				}

				static inline bool isIf(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::IF);
				}

				static inline bool isBreak(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::BREAK);
				}

				static inline bool isSpace(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::SPACE);
				}

				static inline bool isComma(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::COMA);
				}

				static inline bool isPeriod(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::PERIOD);
				}

				static inline bool isColon(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::COLON);
				}

				static inline bool isContinue(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::CONTINUE);
				}

				static inline bool isElse(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::ELSE || offset->getType() == lexical::LexemeType::ELIF);
				}

				static inline bool isElif(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::ELIF);
				}

				static inline bool isForeach(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::FOREACH);
				}

				static inline bool isWhile(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::WHILE);
				}

				static inline bool isImport(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::IMPORT || offset->getType() == lexical::LexemeType::FROM);
				}

				static inline bool isReturn(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::RETURN);
				}

				static inline bool isFunctionDefinition(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::FUNCTION_DEFINITION);
				}

				static inline bool isClassDefinition(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::CLASS_DEFINITION);
				}

				static inline bool isOperator(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator &&
						((offset->getType() >= lexical::LexemeType::MATH_PLUS && offset->getType() <= lexical::LexemeType::MATH_NOT)
							|| (offset->getType() >= lexical::LexemeType::IS && offset->getType() <= lexical::LexemeType::BOOL_NOT_EQUAL));
				}

				static inline bool isBracket(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::OPEN_BRACKET || offset->getType() == lexical::LexemeType::CLOSE_BRACKET);
				}

				static inline bool isClosingBracket(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::CLOSE_BRACKET);
				}

				static inline bool isOpeningBracket(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::OPEN_BRACKET);
				}

				static inline bool isComment(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::SINGLE_LINE_COMMENT);
				}

				static inline bool isEndline(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::ENDLINE);
				}

				static inline bool isString(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (isApos(endIndicator, offset) || isQout(endIndicator, offset));
				}

				static inline bool isApos(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::APOS);
				}

				static inline bool isQout(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::QOUT);
				}

				static inline bool isListStart(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::LIST_START);
				}

				static inline bool isListEnd(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::LIST_END);
				}

				static inline bool isMapStart(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::DICT_START);
				}

				static inline bool isMapEnd(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::DICT_END);
				}

				static inline bool isNumber(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::INT || offset->getType() == lexical::LexemeType::FLOAT);
				}

				static inline bool isBoolValue(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::BOOL_FALSE || offset->getType() == lexical::LexemeType::BOOL_TRUE);
				}

				static inline bool isEmptyValue(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::EMPTY);
				}

				static inline bool isIdentation(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::IDENTATION);
				}

				static inline bool isIdentifier(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::IDENTIFIER);
				}

				static inline bool isEqualizer(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() >= lexical::LexemeType::ADD_EQUALIZER && offset->getType() <= lexical::LexemeType::REGULAR_EQUALIZER);
				}

				static bool isMultilineStringLiteral(const LexIter endIndicator, const LexIter& offset)
				{
					auto lexIter = offset;
					bool flag = true;
					int count = 0;

					// accept only qout sequance of 3
					for (count = 0; count < QOUTE_AMOUNT_MULTILINE && lexIter != endIndicator && flag; count++, lexIter++)
					{
						if (!Recognizers::isQout(endIndicator, lexIter))
						{
							flag = false;
						}
					}

					return flag && count == QOUTE_AMOUNT_MULTILINE;
				}

				static bool isCall(const LexIter endIndicator, const LexIter& offset)
				{
					bool isFlag = false;
					LexIter lexIter = offset;

					if (isIdentifier(endIndicator, lexIter))
					{
						skipVariable(lexIter, endIndicator);
						if (isSpace(endIndicator, lexIter))
						{
							lexIter++;
						}
						if (isOpeningBracket(endIndicator, lexIter))
						{
							isFlag = true;
						}
					}

					return isFlag;
				}

				static bool isAssign(const LexIter endIndicator, const LexIter& offset)
				{
					bool hasEqualizer = false; //reached equalizer flag
					bool hasAssigned = false; //reached variable that could be assigned to
					bool stop = false;

					// iterate over lexemes
					for (LexIter lexIter = offset; lexIter != endIndicator && !stop; lexIter++)
					{
						// if it's not a variable or a coma
						if (!(isSpace(endIndicator, lexIter) || isComma(endIndicator, lexIter)))
						{
							if (isVariable(endIndicator, lexIter))
							{
								hasAssigned = true;
								skipVariable(lexIter, endIndicator);
								lexIter--;
							}
							else if (isEqualizer(endIndicator, lexIter))
							{
								hasEqualizer = true;
								stop = true;
							}
							else
							{
								stop = true;
							}
						}
					}
					return hasAssigned && hasEqualizer && stop;
				}

				static bool isExpression(const LexIter endIndicator, const LexIter& offset)
				{
					LexIter lexIter = offset;
					bool isFlag = true;
					bool stop = false;

					// iterate over lexemes
					while (lexIter != endIndicator && !stop)
					{
						if (isValue(endIndicator, lexIter))
						{
							skipLiteralValue(lexIter, endIndicator);
						}
						else if (isCall(endIndicator, lexIter))
						{
							skipCall(lexIter, endIndicator);
						}
						else if (isVariable(endIndicator, lexIter))
						{
							skipVariable(lexIter, endIndicator);
						}
						else if (!isSpace(endIndicator, lexIter) && !isOpeningBracket(endIndicator, lexIter))
						{
							if (!isOperator(endIndicator, lexIter))
							{
								isFlag = false;
							}
							stop = true;
						}
						else
						{
							lexIter++;
						}
					}

					return isFlag && stop;
				}
			private:

				/*
				skips literal values (map, string, list).
				input: iterator, end iterator.
				output: none.
				*/
				static void skipLiteralValue(LexIter& iter, const LexIter endIndicator)
				{
					if (isString(endIndicator, iter)) //skip string
					{
						if (isMultilineStringLiteral(endIndicator, iter))
						{
							iter += QOUTE_AMOUNT_MULTILINE - 1;
						}

						while (iter != endIndicator && !isString(endIndicator, iter))
						{
							iter++;
						}

						if (isQout(endIndicator, iter) || isApos(endIndicator, iter))
						{
							if (isMultilineStringLiteral(endIndicator, iter))
							{
								iter += QOUTE_AMOUNT_MULTILINE - 1;
							}
							else
							{
								iter++;
							}
						}
					}
					else if (isMapStart(endIndicator, iter)) //skip map
					{
						while (iter != endIndicator && !isMapEnd(endIndicator, iter))
						{
							iter++;
						}
						if (isMapEnd(endIndicator, iter))
						{
							iter++;
						}
					}
					else if (isListStart(endIndicator, iter)) //skip list
					{
						while (iter != endIndicator && !isListEnd(endIndicator, iter))
						{
							iter++;
						}
						if (isListEnd(endIndicator, iter))
						{
							iter++;
						}
					}
					else if (iter != endIndicator)
					{
						iter++;
					}
				}

				/*
				skips call.
				input: iterator, end iterator.
				output: none.
				*/
				static void skipCall(LexIter& iter, const LexIter endIndicator)
				{
					while (iter != endIndicator && !isClosingBracket(endIndicator, iter))
					{
						iter++;
					}
					if (isClosingBracket(endIndicator, iter))
					{
						iter++;
					}
				}

				/*
				skips variable.
				input: iterator, end iterator.
				output: none.
				*/
				static void skipVariable(LexIter& iter, const LexIter endIndicator)
				{
					while (iter != endIndicator && (isIdentifier(endIndicator, iter) || isPeriod(endIndicator, iter)))
					{
						iter++;
					}
				}
			};

			/*
			If parsing method.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseIf(io::python::ParserInput in)
			{
				operations::If* result = nullptr;
				auto cond = io::python::ParserOutput(); //condition
				bool success = true;

				skip(in.lexemes.end(), in.offset); //ignore 'if'

				cond = getAssignable(in, false); //get condition
				success = cond.success;

				if (success) // if succeeded
				{
					result = new operations::If(in.lineCount, in.filename, operations::Expression::cast(cond.op));
					success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isColon); //ignore 'colon'

					if (!success && in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "':'", in.offset->getSpan(), in.lineCount);
					}
				}
				in.needsScope = true;

				return { result, success };
			}

			/*
			Else parsing method.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseElse(io::python::ParserInput in)
			{
				auto cond = io::python::ParserOutput(); //condition
				operations::Else* result = nullptr;
				bool isElif = Recognizers::isElif(in.lexemes.end(), in.offset);
				bool success = true;

				skip(in.lexemes.end(), in.offset); //ignore 'else'

				if (isElif)
				{
					cond = getAssignable(in, false); //get condition
					success = cond.success;
				}
				if (success) // if succeeded
				{
					result = new operations::Else(in.lineCount, in.filename, operations::Expression::cast(cond.op));
					success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isColon); //ignore 'colon'

					if (!success && in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "':'", in.offset->getSpan(), in.lineCount);
					}


				}
				in.needsScope = true;

				return { result, success };
			}

			/*
			Variable parsing method.
			input: lexemes, offset, in.filename and line counter.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseVariable(io::python::ParserInput in)
			{
				auto name = getFullName(in); //get name
				operations::Variable* result = nullptr;
				bool success = name.second;

				if (success) // if succeeded
				{
					result = new operations::Variable(in.lineCount, in.filename, { name.first, operations::VariableType::NONE, operations::AccessModefier::NONE, 0 });
				}

				return { result, success };
			}

			/*
			Value parsing method.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseValue(io::python::ParserInput in)
			{
				auto result = io::python::BuilderOutput();
				bool success = true;

				//switch between possible values and build the matched value
				if (Parsers::Recognizers::isString(in.lexemes.end(), in.offset))
				{
					result = buildStringValue(in);
				}
				else if (Parsers::Recognizers::isNumber(in.lexemes.end(), in.offset))
				{
					result = buildNumberValue(in);
				}
				else if (Parsers::Recognizers::isListStart(in.lexemes.end(), in.offset))
				{
					result = buildListValue(in);
				}
				else if (Parsers::Recognizers::isMapStart(in.lexemes.end(), in.offset))
				{
					result = buildMapValue(in);
				}
				else if (Parsers::Recognizers::isBoolValue(in.lexemes.end(), in.offset))
				{
					result = buildBoolValue(in);
				}
				else if (Parsers::Recognizers::isEmptyValue(in.lexemes.end(), in.offset))
				{
					result = buildEmptyValue(in);
				}
				else
				{
					success = false; //if nothing matches

					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::UNSUPPORTED, issues::common::UNRECOGNIZED_VAL, in.offset->getSpan(), in.lineCount);
					}
				}

				return { new operations::Value(in.lineCount, in.filename, std::unique_ptr<operations::values::BasicValue>(result.value)), success && result.success };
			}

			/*
			Arithmatic parsing method.
			input: lexemes, offset, in.filename and line counter.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseOperator(io::python::ParserInput in)
			{
				operations::Operator::Type type = operations::Operator::Type::UNKNOWN;
				bool success = true;

				//switch between possible arithmatic operation types
				switch (in.offset->getType())
				{
				case lexical::LexemeType::MATH_PLUS:
					type = operations::Operator::Type::ADD;
					break;
				case lexical::LexemeType::MATH_MINUS:
					type = operations::Operator::Type::SUB;
					break;
				case lexical::LexemeType::MATH_MOD:
					type = operations::Operator::Type::MOD;
					break;
				case lexical::LexemeType::MATH_MULT:
					type = operations::Operator::Type::MUL;
					break;
				case lexical::LexemeType::MATH_DIV:
					type = operations::Operator::Type::DIV;
					break;
				case lexical::LexemeType::MATH_SHR:
					type = operations::Operator::Type::SHR;
					break;
				case lexical::LexemeType::MATH_SHL:
					type = operations::Operator::Type::SHL;
					break;
				case lexical::LexemeType::MATH_AND:
					type = operations::Operator::Type::BIN_AND;
					break;
				case lexical::LexemeType::MATH_OR:
					type = operations::Operator::Type::BIN_OR;
					break;
				case lexical::LexemeType::MATH_NOT:
					type = operations::Operator::Type::BIN_NOT;
					break;
				case lexical::LexemeType::MATH_XOR:
					type = operations::Operator::Type::XOR;
					break;
				case lexical::LexemeType::BOOL_NOT:
					type = operations::Operator::Type::BOOL_NOT;
					break;
				case lexical::LexemeType::BOOL_AND:
					type = operations::Operator::Type::BOOL_AND;
					break;
				case lexical::LexemeType::BOOL_OR:
					type = operations::Operator::Type::BOOL_OR;
					break;
				case lexical::LexemeType::BOOL_EQUAL:
					type = operations::Operator::Type::EQUAL;
					break;
				case lexical::LexemeType::BOOL_LESS:
					type = operations::Operator::Type::LESS;
					break;
				case lexical::LexemeType::BOOL_GREATER:
					type = operations::Operator::Type::GREATER;
					break;
				case lexical::LexemeType::BOOL_LESS_EQUAL:
					type = operations::Operator::Type::LESS_EQUAL;
					break;
				case lexical::LexemeType::BOOL_GREATER_EQUAL:
					type = operations::Operator::Type::GREATER_EQUAL;
					break;
				case lexical::LexemeType::BOOL_NOT_EQUAL:
					type = operations::Operator::Type::NOT_EQUAL;
					break;
				case lexical::LexemeType::IS:
					type = operations::Operator::Type::SAME_AS;
					break;
				default:
					success = false; //if nothing matches
					break;
				}
				if (success)
				{
					in.offset++;
				}
				else if (in.offset != in.lexemes.end())
				{
					in.issues.submit(issues::Issue::Type::UNSUPPORTED, issues::common::UNRECOGNIZED_OPERATOR, in.offset->getSpan(), in.lineCount);
				}

				return { new operations::Operator(in.lineCount, in.filename, type), success };
			}

			/*
			Expression parsing method.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseExpression(io::python::ParserInput in, bool isInclosed)
			{
				LexIter start = in.offset;
				operations::Operator* container = nullptr; //op container
				operations::Expression* result = nullptr;
				auto op = io::python::ParserOutput(); //return value container
				auto comps = std::list<operations::OperationData*>(); //components
				auto element = io::python::ParserOutput(); //return value container
				bool success = true;
				bool stop = false;

				//lambdas for arithmatic operation ordering
				const std::vector<std::function<bool(operations::Operator::Type)>> levels = {
					[&](operations::Operator::Type type) { return type == operations::Operator::Type::BIN_NOT; },
					[&](operations::Operator::Type type) { return type >= operations::Operator::Type::DIV && type < operations::Operator::Type::BIN_NOT; },
					[&](operations::Operator::Type type) { return type == operations::Operator::Type::SUB; },
					[&](operations::Operator::Type type) { return type >= operations::Operator::Type::ADD && type < operations::Operator::Type::SUB; },
					[&](operations::Operator::Type type) { return type >= operations::Operator::Type::GREATER && type <= operations::Operator::Type::NOT_EQUAL; },
					[&](operations::Operator::Type type) { return type == operations::Operator::Type::BOOL_NOT; },
					[&](operations::Operator::Type type) { return type >= operations::Operator::Type::BOOL_AND && type < operations::Operator::Type::BOOL_NOT; }
				};

				while (in.offset != in.lexemes.end() && !stop && success)
				{
					if (Recognizers::isSpace(in.lexemes.end(), in.offset))
					{
						in.offset++; //skip spaces
					}
					else if (Recognizers::isOpeningBracket(in.lexemes.end(), in.offset))
					{
						in.offset++;
						element = getAssignable(in, true); //get element that is inside the brackets
						success = element.success;

						comps.emplace_back(element.op);
					}
					else if (Recognizers::isClosingBracket(in.lexemes.end(), in.offset))
					{
						if (isInclosed) //skip bracket only when expression is inclosed in brackets
						{
							in.offset++;
						}
						stop = true;
					}
					else if (Recognizers::isOperator(in.lexemes.end(), in.offset))
					{
						op = parseOperator(in); //get op
						success = op.success;

						comps.emplace_back(op.op);
					}
					else if (Recognizers::isCall(in.lexemes.end(), in.offset) || Recognizers::isValue(in.lexemes.end(), in.offset) || Recognizers::isVariable(in.lexemes.end(), in.offset))
					{
						element = getExpressionElement(in); //get expression element
						success = element.success;

						comps.emplace_back(element.op);
					}
					else
					{
						stop = true;
					}
				}

				// iterate over levels
				for (auto levelcheck = levels.begin(); levelcheck != levels.end() && comps.size() != 1 && success; levelcheck++)
				{
					// for every element in the expression
					for (OpIter compIter = comps.begin(); compIter != comps.end() && success; compIter++)
					{
						if ((*compIter)->getType() == operations::OperationType::OPERATOR) //if it's an op
						{
							container = operations::Operator::cast(*compIter); //cast
							if ((*levelcheck)(container->opType)) //parse nodes only for the current operation level
							{
								//switch between operations
								switch (container->opType)
								{
								case operations::Operator::Type::BIN_NOT:
								case operations::Operator::Type::BOOL_NOT:
									//check if operands are valid
									if (isValidOperand(comps.end(), std::next(compIter)))
									{
										//parse
										container->operands.emplace_back(operations::Assignable::cast(*std::next(compIter)));

										//consume operands and create condition
										compIter = comps.erase(compIter, std::next(std::next(compIter)));
										compIter = comps.emplace(compIter, new operations::Expression(in.lineCount, in.filename, std::unique_ptr<operations::Operator>(container), false));
									}
									else
									{
										success = false;
									}
									break;
								case operations::Operator::Type::SUB:
									//if there is an op before sub (like 1 + -2)
									if ((compIter != comps.begin() && (*std::prev(compIter))->getType() == operations::OperationType::OPERATOR) || compIter == comps.begin())
									{
										//check if operands are valid
										if (isValidOperand(comps.end(), std::next(compIter)))
										{
											//parse
											container->operands.emplace_back(operations::Assignable::cast(*std::next(compIter)));

											//consume operands and create condition
											compIter = comps.erase(compIter, std::next(std::next(compIter)));
											compIter = comps.emplace(compIter, new operations::Expression(in.lineCount, in.filename, std::unique_ptr<operations::Operator>(container), false));
										}
										else
										{
											success = false;
										}
										break;
									}
								case operations::Operator::Type::XOR:
								case operations::Operator::Type::SHR:
								case operations::Operator::Type::SHL:
								case operations::Operator::Type::BIN_OR:
								case operations::Operator::Type::MUL:
								case operations::Operator::Type::MOD:
								case operations::Operator::Type::DIV:
								case operations::Operator::Type::BIN_AND:
								case operations::Operator::Type::EQUAL:
								case operations::Operator::Type::LESS:
								case operations::Operator::Type::GREATER:
								case operations::Operator::Type::LESS_EQUAL:
								case operations::Operator::Type::GREATER_EQUAL:
								case operations::Operator::Type::NOT_EQUAL:
								case operations::Operator::Type::BOOL_AND:
								case operations::Operator::Type::BOOL_OR:
								case operations::Operator::Type::SAME_AS:
								case operations::Operator::Type::ADD:
									//check if operands are valid
									if (compIter != comps.begin() && isValidOperand(comps.end(), std::next(compIter)) && isValidOperand(comps.end(), std::prev(compIter)))
									{
										//parse
										container->operands.emplace_back(operations::Assignable::cast(*std::prev(compIter)));
										container->operands.emplace_back(operations::Assignable::cast(*std::next(compIter)));

										//consume operands and create expression
										compIter = comps.erase(std::prev(compIter), std::next(std::next(compIter)));
										compIter = comps.emplace(compIter, new operations::Expression(in.lineCount, in.filename, std::unique_ptr<operations::Operator>(container), false));
									}
									else
									{
										success = false;
									}
									break;
								default:
									success = false;
									break;
								}
							}
						}
					}
				}

				//issue submits
				if (!success)
				{
					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::INVALID_EXPR, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
					}
					else if (start != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::INVALID_EXPR, { start->getSpan()[0], (ptrdiff_t)in.fileContent.size() - 1 }, in.lineCount);
					}
				}

				if (success && comps.size() == 1)
				{
					result = operations::Expression::cast(*comps.begin());
					result->isInclosed = isInclosed;
				}

				return { result, success && comps.size() == 1 };
			}

			/*
			Assign parsing method.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseAssign(io::python::ParserInput in)
			{
				auto limit = [&](const LexIter& end, const LexIter& offset) {
					return Recognizers::isEndline(end, offset) || Recognizers::isComment(end, offset) || Recognizers::isIdentation(end, offset);
				};

				LexIter start = in.offset;
				auto vars = getVarList(in, false);
				auto type = getAssignType(in.offset);
				auto vals = getAssignableList(in, limit);
				auto assigned = std::list<std::pair<std::unique_ptr<operations::Variable>, std::unique_ptr<operations::Assignable>>>(); //list of varible assigns
				auto valIter = vals.first.begin(); //values iterator
				auto varIter = vars.first.begin(); //variables iterator
				bool success = true;

				if (vars.second && type.second && vals.second) //if succeeded
				{
					success = type.first != operations::Assign::Type::REGULAR ? (vals.first.size() == 1 && vars.first.size() == 1) : (vals.first.size() == vars.first.size());
					if (success)
					{
						while (varIter != vars.first.end() && valIter != vals.first.end())
						{
							assigned.emplace_back(std::move(*varIter), std::move(*valIter)); //set variable and assigned value
							varIter++;
							valIter++;
						}
					}
					else
					{
						if (type.first != operations::Assign::Type::REGULAR)
						{
							if (in.offset != in.lexemes.end())
							{
								in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::MULTIASSIGN_INVALID, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
							}
							else if (start != in.lexemes.end())
							{
								in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::MULTIASSIGN_INVALID, { start->getSpan()[0], (ptrdiff_t)in.fileContent.size() - 1 }, in.lineCount);
							}
						}
						else
						{
							if (in.offset != in.lexemes.end())
							{
								in.issues.submit(issues::Issue::Type::SYNTAX, vals.first.size() < vars.first.size() ? issues::common::FEW_VALUES_ASSIGNED : issues::common::TO_MUCH_VALUES_ASSIGNED, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
							}
							else if (start != in.lexemes.end())
							{
								in.issues.submit(issues::Issue::Type::SYNTAX, vals.first.size() < vars.first.size() ? issues::common::FEW_VALUES_ASSIGNED : issues::common::TO_MUCH_VALUES_ASSIGNED, { start->getSpan()[0], (ptrdiff_t)in.fileContent.size() - 1 }, in.lineCount);
							}
						}
					}
				}
				else
				{
					success = false;
				}

				return { new operations::Assign(in.lineCount, in.filename, std::move(assigned), type.first), success };
			}

			/*
			Function definition parsing method.
			input: in.lexemes, offset, in.filename and line counter.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseFunctionDefinition(io::python::ParserInput in)
			{
				operations::FunctionDefinition* result = nullptr;
				auto params = std::pair<std::vector<std::unique_ptr<operations::VariableDeclaration>>, bool>(); //function parameters
				std::string_view name = std::string_view(); //function name
				bool success = true;

				skip(in.lexemes.end(), in.offset); //skip 'def'

				if (Recognizers::isIdentifier(in.lexemes.end(), in.offset))
				{
					name = in.offset->getMatch(); //get name
					skip(in.lexemes.end(), in.offset); //skip name
				}
				else
				{
					success = false;
					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::INVALID_NAME, in.offset->getSpan(), in.lineCount);
					}
				}

				if (success)
				{
					params = getParamList(in);
					success = params.second;

					if (success)
					{
						result = new operations::FunctionDefinition(in.lineCount, in.filename, operations::VariableType::NONE, name, std::move(params.first));
						success = skipIf(in.lexemes.end(), in.offset, Recognizers::isColon); //ignore 'colon'

						if (!success && in.offset != in.lexemes.end())
						{
							in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "':'", in.offset->getSpan(), in.lineCount);
						}
					}
				}
				in.needsScope = true;

				return { result, success };
			}

			/*
			Class definition parsing method.
			input: lexemes, offset, in.filename and line counter.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseClassDefinition(io::python::ParserInput in)
			{
				operations::ClassDefinition* result = nullptr;
				auto name = std::string_view(); //class name
				auto inherits = std::pair<std::vector<operations::Paths>, bool>();
				bool success = true;

				in.issues.submit(issues::Issue::Type::INFO, "class operation is unsupported in Lua.", in.offset->getSpan(), in.lineCount);
				skip(in.lexemes.end(), in.offset);

				if (Recognizers::isIdentifier(in.lexemes.end(), in.offset))
				{
					name = in.offset->getMatch(); //get name
					skip(in.lexemes.end(), in.offset); //skip name
				}
				else
				{
					success = false;
					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::INVALID_NAME, in.offset->getSpan(), in.lineCount);
					}
				}

				if (success)
				{
					if (Recognizers::isOpeningBracket(in.lexemes.end(), in.offset)) //check if there is an inheritance list
					{
						inherits = getInheritanceList(in);
						success = inherits.second;
					}

					if (success) //if succeeded
					{
						result = new operations::ClassDefinition(in.lineCount, in.filename, name, std::move(inherits.first));
						success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isColon); //ignore 'colon'

						if (!success && in.offset != in.lexemes.end())
						{
							in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "':'", in.offset->getSpan(), in.lineCount);
						}
					}
				}
				in.needsScope = true;

				return { result, success };
			}

			/*
			Call parsing method.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseCall(io::python::ParserInput in)
			{
				operations::Call* result = nullptr;
				auto name = getFullName(in);
				auto args = getArgumentList(in);
				bool success = args.second && name.second;

				if (success) //if succeeded
				{
					result = new operations::Call(in.lineCount, in.filename, name.first, std::move(args.first));
				}

				return { result, success };
			}

			/*
			Return parsing method.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseReturn(io::python::ParserInput in)
			{
				auto limit = [&](const LexIter& end, const LexIter& offset) {
					return Recognizers::isEndline(end, offset) || Recognizers::isComment(end, offset);
				};

				auto retArgs = std::pair<std::vector<std::unique_ptr<operations::Assignable>>, bool>();
				operations::Return* result = nullptr;
				bool success = true;

				skip(in.lexemes.end(), in.offset); //ignore 'return'
				retArgs = getAssignableList(in, limit);
				success = retArgs.second;

				if (success) //if succeeded
				{
					result = new operations::Return(in.lineCount, in.filename, std::move(retArgs.first));
				}

				return { result, success };
			}

			/*
			Import parsing method.
			input: lexemes, offset, in.filename and line counter.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseImport(io::python::ParserInput in)
			{
				LexIter start = in.offset;
				auto modules = std::pair<std::vector<operations::Paths>, bool>(); //modules names
				auto specifiers = std::pair<std::vector<operations::Paths>, bool>(); //specifiers names
				bool success = true;

				in.issues.submit(issues::Issue::Type::INFO, issues::common::MULTIFILE_IS_UNSUPPORTED, in.offset->getSpan(), in.lineCount); //submit issue
				if (in.offset->getType() == lexical::LexemeType::FROM) //if has import specifiers
				{
					advance(in.lexemes.end(), in.offset);
					modules = getIdentifiersList(in, false); //get names
					advance(in.lexemes.end(), in.offset);
					specifiers = getIdentifiersList(in, false); //get names
					success = modules.second && modules.first.size() == 1 && specifiers.second; //set flag
				}
				else
				{
					in.offset++;
					modules = getIdentifiersList(in, false); //get names
					success = modules.second;
				}

				//submit issues
				if (!success)
				{
					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::INVALID_IMPORT, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
					}
					else if (start != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::INVALID_IMPORT, { start->getSpan()[0], (ptrdiff_t)in.fileContent.size() - 1 }, in.lineCount);
					}
				}

				return { new operations::Import(in.lineCount, in.filename, modules.first, specifiers.first), success };
			}

			/*
			Foreach parsing method.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseForeach(io::python::ParserInput in)
			{
				LexIter start = in.offset;
				auto iters = std::pair<std::vector<std::unique_ptr<operations::Variable>>, bool>(); //foreach iterators
				auto obj = io::python::ParserOutput(); //iteratable object
				operations::Foreach* result = nullptr;
				bool success = true;

				skip(in.lexemes.end(), in.offset); //skip 'for'

				iters = getVarList(in, false);
				success = iters.second;

				if (success)
				{
					skip(in.lexemes.end(), in.offset);

					obj = getIteratableObject(in);
					success = obj.success;
					if (success) //if succeeded
					{
						result = new operations::Foreach(in.filename, in.lineCount, std::move(iters.first), operations::Iteratable::cast(obj.op));
						advanceIf(in.lexemes.end(), in.offset, Recognizers::isSpace);
						success = skipIf(in.lexemes.end(), in.offset, Recognizers::isColon); //ignore 'colon'

						if (!success && in.offset != in.lexemes.end())
						{
							in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "':'", in.offset->getSpan(), in.lineCount);
						}
					}
					else if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::CANNOT_ITERATE, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
					}
				}
				else if (in.offset != in.lexemes.end())
				{
					in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::MISSING_ITERS, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
				}
				in.needsScope = true;

				return { result, success };
			}

			/*
			While parsing method.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseWhile(io::python::ParserInput in)
			{
				operations::While* result = nullptr;
				auto cond = io::python::ParserOutput(); //condition
				bool success = true;

				skip(in.lexemes.end(), in.offset); //ignore 'while'

				cond = getAssignable(in, false); //get condition
				success = cond.success;

				if (success) //if succeeded
				{
					result = new operations::While(in.lineCount, in.filename, operations::Expression::cast(cond.op));
					advanceIf(in.lexemes.end(), in.offset, Recognizers::isSpace);
					success = skipIf(in.lexemes.end(), in.offset, Recognizers::isColon); //ignore 'colon'

					if (!success && in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "':'", in.offset->getSpan(), in.lineCount);
					}
				}
				in.needsScope = true;

				return { result, success };
			}

			/*
			Break parsing method.
			input: lexemes, offset, in.filename and line counter.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseBreak(io::python::ParserInput in)
			{
				in.offset++;
				return { new operations::Break(in.lineCount, in.filename), true };
			}

			/*
			Continue parsing method.
			input: lexemes, offset, in.filename and line counter.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseContinue(io::python::ParserInput in)
			{
				in.issues.submit(issues::Issue::Type::INFO, "continue operation is unsupported in Lua.", in.offset->getSpan(), in.lineCount);
				in.offset++;
				return { new operations::Continue(in.lineCount, in.filename), true };
			}

			/*
			Error parsing method.
			input: lexemes, offset, in.filename and line counter.
			output: parsed pointer, new offset, success flag
			*/
			static io::python::ParserOutput parseError(io::python::ParserInput in)
			{
				operations::Error* result = nullptr;
				auto issue = in.issues.last();

				if (issue.second)
				{
					result = new operations::Error(in.lineCount, in.filename, *issue.first);

					while (in.offset != in.lexemes.end() && in.offset->getSpan()[0] < issue.first->span[1])
					{
						in.offset++;
					}
				}
				else if (in.offset != in.lexemes.end())
				{
					in.issues.submit(in.offset->getSpan(), in.lineCount);
					result = new operations::Error(in.lineCount, in.filename, *in.issues.last().first);
					in.offset++;
				}
				else
				{
					in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::REACHED_EOF, { (ptrdiff_t)in.fileContent.size() - 1, (ptrdiff_t)in.fileContent.size() - 1 }, in.lineCount);
					result = new operations::Error(in.lineCount, in.filename, *in.issues.last().first);
				}

				return { result, issue.second };
			}

			/*
			Builds string value.
			input: lexemes, offset, in.filename, line counter.
			output: parsed value, success flag.
			*/
			static io::python::ParserOutput parseComment(io::python::ParserInput in)
			{
				std::string result = std::string();
				std::array<ptrdiff_t, SPAN_SIZE> span = { in.offset->getSpan()[1], 0 };

				in.offset++;
				while (in.offset != in.lexemes.end() && !Recognizers::isEndline(in.lexemes.end(), in.offset))
				{
					in.offset++;
				}

				span[1] = in.offset == in.lexemes.end() ? in.fileContent.size() - 1 : in.offset->getSpan()[0];
				result = std::string(in.fileContent.substr(span[0], span[1] - span[0]));

				return { new operations::Comment(in.lineCount, in.filename, result, span), true };
			}
		private:
			/* Helper Methods */

			/*
			Advances the offset iterator and skips whitespaces.
			input: offset.
			output: new offset.
			*/
			static inline void skip(LexIter endIndicator, LexIter& offset)
			{
				advance(endIndicator, offset);
				if (Recognizers::isSpace(endIndicator, offset))
				{
					offset++;
				}
			}

			static inline bool skipIf(LexIter endIndicator, LexIter& offset, Recognizers::RecoMethod&& check)
			{
				bool success = check(endIndicator, offset) && offset != endIndicator;
				if (success)
				{
					skip(endIndicator, offset);
				}
				return success;
			}

			static inline void advance(LexIter endIndicator, LexIter& offset)
			{
				if (offset != endIndicator)
				{
					offset++;
				}
			}

			static inline bool advanceIf(LexIter endIndicator, LexIter& offset, Recognizers::RecoMethod& check)
			{
				bool success = check(endIndicator, offset);
				if (success)
				{
					advance(endIndicator, offset);
				}
				return success;
			}

			static inline void advanceUntil(LexIter endIndicator, LexIter& offset, Recognizers::RecoMethod& until)
			{
				while (offset != endIndicator && !until(endIndicator, offset))
				{
					offset++;
				}
			}

			/*
			Returns the namespace and the name of the identifier.
			input: lexemes, offset.
			output: namespace, name, success flag.
			*/
			static std::pair<operations::Paths, bool> getFullName(io::python::ParserInput in)
			{
				operations::Paths name = operations::Paths();
				LexIter start = in.offset;
				bool expectPeriod = false; //expect period flag
				bool isError = false;

				//accept identifiers and periods
				while (in.offset != in.lexemes.end() && !isError && (Recognizers::isPeriod(in.lexemes.end(), in.offset) || Recognizers::isIdentifier(in.lexemes.end(), in.offset)))
				{
					if (expectPeriod && Recognizers::isPeriod(in.lexemes.end(), in.offset)) //check if it's a period and if it was expected
					{
						expectPeriod = false;
						in.offset++;
					}
					else if ((!expectPeriod) && Recognizers::isIdentifier(in.lexemes.end(), in.offset))
					{
						name.emplace_back(in.offset->getMatch()); //add to namespace
						expectPeriod = true;
						in.offset++;
					}
					else
					{
						isError = true;
					}
				}

				//submit issues
				if (isError && in.offset != in.lexemes.end())
				{
					in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::INVALID_NAME, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
				}
				else if (name.empty() && in.offset != in.lexemes.end())
				{
					in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EMPTY_NAME, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
				}

				return { name, !isError && !name.empty() };
			}

			/*
			Returns a list of identifiers (with a full name).
			input: lexemes, offset.
			output: list of full identifer names, success flag.
			*/
			static std::pair<std::vector<operations::Paths>, bool> getIdentifiersList(io::python::ParserInput in, bool acceptEmptyList)
			{
				auto name = std::pair<operations::Paths, bool>(); //full name container
				auto names = std::vector<operations::Paths>(); //name list
				bool isError = false;
				bool stop = false;

				//accept periods, identifiers and coma
				while (in.offset != in.lexemes.end() && !isError && !stop)
				{
					if (Recognizers::isIdentifier(in.lexemes.end(), in.offset))
					{
						name = getFullName(in);

						if (name.second) //if succeeded
						{
							//append to name list
							names.emplace_back(name.first);
						}
						else
						{
							isError = true;
						}
					}
					else if (Recognizers::isComma(in.lexemes.end(), in.offset) || Recognizers::isSpace(in.lexemes.end(), in.offset))
					{
						skip(in.lexemes.end(), in.offset);
					}
					else
					{
						stop = true;
					}
				}

				if (!acceptEmptyList && names.empty() && in.offset != in.lexemes.end())
				{
					if (isError && in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EMPTY_NAME_LIST, in.offset->getSpan(), in.lineCount);
					}
				}

				return { std::move(names), !isError && (acceptEmptyList ? true : !names.empty()) };
			}

			/*
			Returns a list of variables.
			input: lexemes, offset, accept empty list flag.
			output: list of variables, success flag.
			*/
			static std::pair<std::vector<std::unique_ptr<operations::Variable>>, bool> getVarList(io::python::ParserInput in, bool acceptEmptyList)
			{
				auto vars = std::vector<std::unique_ptr<operations::Variable>>();
				auto names = getIdentifiersList(in, acceptEmptyList);

				if (names.second) //if succeeded
				{
					//create variables from names
					for (auto name : names.first)
					{
						vars.emplace_back(new operations::Variable(in.lineCount, in.filename, { name, operations::VariableType::NONE, operations::AccessModefier::NONE, 0 }));
					}
				}

				return { std::move(vars), names.second };
			}

			/*
			Returns a list of parameters.
			input: lexemes, offset.
			output: list of parameters, success flag.
			*/
			static std::pair<std::vector<std::unique_ptr<operations::VariableDeclaration>>, bool> getParamList(io::python::ParserInput in)
			{
				auto params = std::vector<std::unique_ptr<operations::VariableDeclaration>>();
				auto vars = std::pair<std::vector<std::unique_ptr<operations::Variable>>, bool>();
				bool success = true;

				success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isOpeningBracket); //ignore '('

				if (success)
				{
					vars = getVarList(in, true);
					success = vars.second;
					if (success)
					{
						//create varibale declarations from variables
						for (auto varIter = vars.first.begin(); varIter != vars.first.end(); varIter++)
						{
							params.emplace_back(new operations::VariableDeclaration(in.lineCount, in.filename, std::move(*varIter)));
						}

						success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isClosingBracket); //ignore ')'

						if (!success && in.offset != in.lexemes.end())
						{
							in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "')'", in.offset->getSpan(), in.lineCount);
						}
					}
				}
				else if (in.offset != in.lexemes.end())
				{
					in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "'('", in.offset->getSpan(), in.lineCount);
				}

				return { std::move(params), success };
			}

			/*
			Returns a list of inherited classes names.
			input: lexemes, offset.
			output: list of inherited classes names, success flag.
			*/
			static std::pair<std::vector<operations::Paths>, bool> getInheritanceList(io::python::ParserInput in)
			{
				std::pair<std::vector<operations::Paths>, bool> inherits = std::pair<std::vector<operations::Paths>, bool>();
				bool success = true;

				in.offset++; //skip '('

				if (success)
				{
					inherits = getIdentifiersList(in, true); //get classes names
					success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isClosingBracket); //skip '('

					if (!success && in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "')'", in.offset->getSpan(), in.lineCount);
					}
				}

				return { inherits.first, success };
			}

			/*
			Returns a parsed iteratable object.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed pointer, new offset, success flag.
			*/
			static io::python::ParserOutput getIteratableObject(io::python::ParserInput in)
			{
				auto result = io::python::ParserOutput();
				bool success = true;

				//switch between possible matches and parse
				if (Recognizers::isValue(in.lexemes.end(), in.offset))
				{
					result = parseValue(in);
				}
				else if (Recognizers::isVariable(in.lexemes.end(), in.offset))
				{
					result = parseVariable(in);
				}
				else if (Recognizers::isCall(in.lexemes.end(), in.offset))
				{
					result = parseCall(in);
				}
				else
				{
					result.success = false;

					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::UNSUPPORTED, issues::common::UNRECOGNIZED_VAL, in.offset->getSpan(), in.lineCount);
					}
				}

				return result;
			}

			/*
			Returns argument list of a call.
			input: lexemes, offset, in.filename, line counter and file content.
			output: arguments list, success flag.
			*/
			static std::pair<std::vector<std::unique_ptr<operations::Assignable>>, bool> getArgumentList(io::python::ParserInput in)
			{
				auto result = std::pair<std::vector<std::unique_ptr<operations::Assignable>>, bool>();
				bool success = true;

				advanceIf(in.lexemes.end(), in.offset, Recognizers::isSpace); //ignore space
				success = skipIf(in.lexemes.end(), in.offset, Recognizers::isOpeningBracket); //skip '('
				if (success)
				{
					result = getAssignableList(in, Recognizers::isClosingBracket); //get arguments
					success = result.second && skipIf(in.lexemes.end(), in.offset, Recognizers::isClosingBracket);
					if (!success && in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "')'", in.offset->getSpan(), in.lineCount);
					}
				}
				else if (in.offset != in.lexemes.end())
				{
					in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "'('", in.offset->getSpan(), in.lineCount);
				}

				return { std::move(result.first), success };
			}

			/*
			Returns the assign operation type.
			input: offset.
			output: assign operation type.
			*/
			static std::pair<operations::Assign::Type, bool> getAssignType(LexIter& offset)
			{
				operations::Assign::Type type = operations::Assign::Type::UNKNOWN;
				bool isError = false;

				//switch between lexeme types and convert to an assign operation type
				switch (offset->getType())
				{
				case lexical::LexemeType::REGULAR_EQUALIZER:
					type = operations::Assign::Type::REGULAR;
					break;
				case lexical::LexemeType::ADD_EQUALIZER:
					type = operations::Assign::Type::ADD;
					break;
				case lexical::LexemeType::SUB_EQUALIZER:
					type = operations::Assign::Type::SUB;
					break;
				case lexical::LexemeType::MOD_EQUALIZER:
					type = operations::Assign::Type::MOD;
					break;
				case lexical::LexemeType::MULT_EQUALIZER:
					type = operations::Assign::Type::MUL;
					break;
				case lexical::LexemeType::DIV_EQUALIZER:
					type = operations::Assign::Type::DIV;
					break;
				case lexical::LexemeType::SHR_EQUALIZER:
					type = operations::Assign::Type::SHR;
					break;
				case lexical::LexemeType::SHL_EQUALIZER:
					type = operations::Assign::Type::SHL;
					break;
				case lexical::LexemeType::AND_EQUALIZER:
					type = operations::Assign::Type::AND;
					break;
				case lexical::LexemeType::OR_EQUALIZER:
					type = operations::Assign::Type::OR;
					break;
				case lexical::LexemeType::XOR_EQUALIZER:
					type = operations::Assign::Type::XOR;
					break;
				default:
					isError = true;
					break;
				}
				if (!isError)
				{
					offset++;
				}

				return { type, !isError };
			}

			/*
			Returns assignable operation object.
			input: lexemes, offset, in.filename, line counter and file content.
			output: assignable object, success flag.
			*/
			static io::python::ParserOutput getAssignable(io::python::ParserInput in, bool isInclosed)
			{
				auto result = io::python::ParserOutput();

				//switch between possible operations and parse
				if (Parsers::Recognizers::isExpression(in.lexemes.end(), in.offset))
				{
					result = parseExpression(in, isInclosed);
				}
				else if (Parsers::Recognizers::isCall(in.lexemes.end(), in.offset))
				{
					result = parseCall(in);
				}
				else if (Parsers::Recognizers::isValue(in.lexemes.end(), in.offset))
				{
					result = parseValue(in);
				}
				else if (Parsers::Recognizers::isVariable(in.lexemes.end(), in.offset))
				{
					result = parseVariable(in);
				}
				else
				{
					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::UNSUPPORTED, issues::common::UNRECOGNIZED_VAL, in.offset->getSpan(), in.lineCount);
					}
				}

				return result;
			}

			/*
			Returns an assignable expression object.
			input: lexemes, offset, in.filename, line counter and file content.
			output: assignable object, success flag.
			*/
			static io::python::ParserOutput getExpressionElement(io::python::ParserInput in)
			{
				auto result = io::python::ParserOutput();

				//switch between possible operations and parse
				if (Parsers::Recognizers::isCall(in.lexemes.end(), in.offset))
				{
					result = parseCall(in);
				}
				else if (Parsers::Recognizers::isValue(in.lexemes.end(), in.offset))
				{
					result = parseValue(in);
				}
				else if (Parsers::Recognizers::isVariable(in.lexemes.end(), in.offset))
				{
					result = parseVariable(in);
				}

				return result;
			}

			/*
			Returns a list of assignable operation objects.
			input: lexemes, offset, in.filename, line counter, file content and a limit identification lambda.
			output: list of assignable objects, success flag.
			*/
			static std::pair<std::vector<std::unique_ptr<operations::Assignable>>, bool> getAssignableList(io::python::ParserInput in, Recognizers::RecoMethod& until)
			{
				auto elements = std::vector<std::unique_ptr<operations::Assignable>>();
				auto result = io::python::ParserOutput();
				bool isError = false;
				bool stop = false;

				while (in.offset != in.lexemes.end() && !stop)
				{
					if (until(in.lexemes.end(), in.offset)) //if the limit is reached
					{
						stop = true; //stop
					}
					else if (!(Recognizers::isComma(in.lexemes.end(), in.offset) || Recognizers::isSpace(in.lexemes.end(), in.offset)))
					{
						result = getAssignable(in, false); //get element
						if (result.success) //if succeeded
						{
							elements.emplace_back(operations::Assignable::cast(result.op)); //append
						}
						else
						{
							stop = true;
							isError = true;
						}
					}
					else
					{
						in.offset++; //ignore coma and space
					}
				}

				return { std::move(elements), !isError };
			}

			/*
			Builds list value.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed value, success flag.
			*/
			static io::python::BuilderOutput buildListValue(io::python::ParserInput in)
			{
				auto limit = [&](const LexIter& end, const LexIter& offset) {
					return Recognizers::isListEnd(end, offset);
				};
				LexIter start = in.offset;
				auto result = std::pair<std::vector<std::unique_ptr<operations::Assignable>>, bool>();
				bool isMutable = true;
				bool success = true;

				skip(in.lexemes.end(), in.offset);
				result = getAssignableList(in, limit);
				success = result.second;

				if (success)
				{
					advanceIf(in.lexemes.end(), in.offset, Recognizers::isSpace);
					success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isListEnd);

					if (!success && in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "']'", in.offset->getSpan(), in.lineCount);
					}
				}

				return { new operations::values::List(std::move(result.first), isMutable), success };
			}

			/*
			Builds numeric value.
			input: lexemes, offset, in.filename, line counter.
			output: parsed value, success flag.
			*/
			static io::python::BuilderOutput buildNumberValue(io::python::ParserInput in)
			{
				long double result = 0;
				bool success = true;

				try
				{
					result = std::stod(std::string(in.offset->getMatch()));
				}
				catch (std::exception&)
				{
					success = false;
					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::UNRECOGNIZED_VAL, in.offset->getSpan(), in.lineCount);
					}
				}
				in.offset++;

				return { new operations::values::Number(result), success };
			}

			/*
			Builds Operator value.
			input: lexemes, offset, in.filename, line counter.
			output: parsed value, success flag.
			*/
			static io::python::BuilderOutput buildBoolValue(io::python::ParserInput in)
			{
				bool result = in.offset->getType() == lexical::LexemeType::BOOL_TRUE ? true : false;
				in.offset++;

				return { new operations::values::Bool(result), true };
			}

			/*
			Builds Operator value.
			input: lexemes, offset, in.filename, line counter.
			output: parsed value, success flag.
			*/
			static io::python::BuilderOutput buildEmptyValue(io::python::ParserInput in)
			{
				in.offset++;
				return { new operations::values::Empty(), true };
			}

			/*
			Builds map value.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed value, success flag.
			*/
			static io::python::BuilderOutput buildMapValue(io::python::ParserInput in)
			{
				auto map = std::unordered_map<std::unique_ptr<operations::values::BasicValue>, std::unique_ptr<operations::Assignable>>();
				auto key = io::python::BuilderOutput(); //key container
				auto value = io::python::ParserOutput(); //value container
				bool expectKey = true; //expect key flag
				bool isOpened = false; //is dict open flag
				bool isError = false;

				do
				{
					if (Recognizers::isSpace(in.lexemes.end(), in.offset)) //skip spaces
					{
						in.offset++;
					}
					else if (Recognizers::isComma(in.lexemes.end(), in.offset)) //skip coma
					{
						in.offset++;
					}
					else if (Recognizers::isMapStart(in.lexemes.end(), in.offset)) //if opened
					{
						isOpened = true;
						skip(in.lexemes.end(), in.offset);
					}
					else if (Recognizers::isMapEnd(in.lexemes.end(), in.offset)) //if closed
					{
						isOpened = false;
						skip(in.lexemes.end(), in.offset);
					}
					else if (isOpened) //if it's already open
					{
						if (expectKey)
						{
							//switch between possible key values and build
							if (Parsers::Recognizers::isString(in.lexemes.end(), in.offset))
							{
								key = buildStringValue(in);
							}
							else if (Parsers::Recognizers::isNumber(in.lexemes.end(), in.offset))
							{
								key = buildNumberValue(in);
							}
							else if (Parsers::Recognizers::isMapStart(in.lexemes.end(), in.offset))
							{
								key = buildMapValue(in);
							}
							else
							{
								isError = true;
								if (in.offset != in.lexemes.end())
								{
									in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::VAL_CANNOT_KEY, in.offset->getSpan(), in.lineCount);
								}
							}
							expectKey = false;

							advanceIf(in.lexemes.end(), in.offset, Recognizers::isSpace);
							isError = !skipIf(in.lexemes.end(), in.offset, Recognizers::isColon); //skip colon
						}
						else
						{
							value = getAssignable(in, false); //get value

							if (value.success && key.success) //if succeeded
							{
								map.emplace(key.value, operations::Assignable::cast(value.op)); //append to map
							}
							else
							{
								isError = true;
							}
							expectKey = true;
						}
					}
					else
					{
						isError = true;
						if (in.offset != in.lexemes.end())
						{
							in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::UNEXPECTED_TOKEN + "'" + std::string(in.offset->getMatch()) + "'", in.offset->getSpan(), in.lineCount);
						}
					}
				} while (in.offset != in.lexemes.end() && isOpened && !isError); //run until the dict is closed

				//check if literal is closed
				if (!isError && isOpened)
				{
					isError = true;
					//submit issues
					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "'}'", in.offset->getSpan(), in.lineCount);
					}
					else
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "'}'", { (ptrdiff_t)in.fileContent.size(), (ptrdiff_t)in.fileContent.size() }, in.lineCount);
					}
				}
				

				return { new operations::values::Map(std::move(map)), !isError };
			}

			/*
			Builds string value.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed value, success flag.
			*/
			static io::python::BuilderOutput buildStringValue(io::python::ParserInput in)
			{
				std::string result = "";
				std::array<ptrdiff_t, SPAN_SIZE> span = { 0 }; // string span
				bool isQout = Recognizers::isQout(in.lexemes.end(), in.offset); // is qout or apos
				bool isMultiline = false;
				bool stop = false;
				bool success = true;

				if (Recognizers::isMultilineStringLiteral(in.lexemes.end(), in.offset)) //check
				{
					std::advance(in.offset, QOUTE_AMOUNT_MULTILINE - 1); //skip literal
					isMultiline = true;
				}
				span[0] = in.offset->getSpan()[1]; //take last literal end
				in.offset++; //skip last literal

				while (in.offset != in.lexemes.end() && !stop && success)
				{
					if (isMultiline && Recognizers::isEndline(in.lexemes.end(), in.offset)) //count new lines in multiline string
					{
						in.lineCount++;
					}
					else if (!isMultiline && Recognizers::isEndline(in.lexemes.end(), in.offset)) //if has illegal newline
					{
						success = false;
					}

					// if reched the end of the string
					if ((isMultiline && Recognizers::isMultilineStringLiteral(in.lexemes.end(), in.offset)) ||
						(!isMultiline && isQout && Recognizers::isQout(in.lexemes.end(), in.offset)) || (!isQout && Recognizers::isApos(in.lexemes.end(), in.offset)))
					{
						stop = true;
					}
					else
					{
						in.offset++; //skip
					}
				}

				if (success && stop)
				{
					span[1] = in.offset->getSpan()[0]; //set end
					result = std::string(in.fileContent.substr(span[0], span[1] - span[0])); //copy content

					//skip end literal
					if (isMultiline)
					{
						std::advance(in.offset, QOUTE_AMOUNT_MULTILINE);
					}
					else
					{
						in.offset++;
					}
				}

				return { new operations::values::String(result), success && stop };
			}

			/*
			Checks if the operands is a valid arithmatic/Operator operation operand.
			input: operation list end indicator, iterator to element.
			output: is valid flag.
			*/
			static bool isValidOperand(OpIter endIndicator, OpIter element)
			{
				return element != endIndicator
					&& ((*element)->getType() == operations::OperationType::EXPRESSION
						|| (*element)->getType() == operations::OperationType::VALUE
						|| (*element)->getType() == operations::OperationType::VARIABLE
						|| (*element)->getType() == operations::OperationType::CALL);
			}
		};
	};
}