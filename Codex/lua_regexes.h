#pragma once
#include <string>
#include <vector>

#include "lexeme.h"

namespace codex::core::lexical::lua::regexes
{
	const std::string ESCAPE_BACKSLASH = R"((?<!\\)(\\\\)*)"; //if matched, then letter proceeding has not been escaped
	const std::string COMA = R"((\,))";

	const std::string NONWORD_PREFIX = R"((?:^|(?<=\W)))";
	const std::string NONWORD_POSTFIX = R"((?=\W|$))";

	//standard for object names in lua
	const std::string LUA_DECL = R"(([a-zA-Z_]\w*))";


	/* basic types */
	// NIL is in keywords
	// BOOLEAN is in keywords
	const std::string STR_SINGLE_LINE_SINGLE_QUOTE = ESCAPE_BACKSLASH + "'";
	const std::string STR_SINGLE_LINE_DOUBLE_QUOTE = ESCAPE_BACKSLASH + R"(")";
	const std::string STR_MULTI_LINE_START = R"((?<!--)\[\[)";
	const std::string STR_MULTI_LINE_END = R"(\]\](?!--))";

	const std::string INT = R"((?<!([\w\.\d]))\d+(?!([\w\.\d])))";
	const std::string FLOAT = R"((?<!\w)\d+\.\d+(?!\w))";


	/* keywords, lua identifier */
	const std::string LUA_KEYWORD = "(and|break|do|else|elseif|end|false|for|function|goto|if|in|local|nil|not|or|repeat|return|then|true|until|while)";
	const std::string AND = NONWORD_PREFIX + "and" + NONWORD_POSTFIX;
	const std::string BREAK = NONWORD_PREFIX + "break" + NONWORD_POSTFIX;
	const std::string DO = NONWORD_PREFIX + "do" + NONWORD_POSTFIX;
	const std::string ELSE = NONWORD_PREFIX + "else" + NONWORD_POSTFIX;
	const std::string ELSEIF = NONWORD_PREFIX + "elseif" + NONWORD_POSTFIX;
	const std::string END = NONWORD_PREFIX + "end" + NONWORD_POSTFIX;
	const std::string FALSE = NONWORD_PREFIX + "false" + NONWORD_POSTFIX;
	const std::string FOR = NONWORD_PREFIX + "for" + NONWORD_POSTFIX;
	const std::string FUNCTION = NONWORD_PREFIX + "function" + NONWORD_POSTFIX;
	const std::string GOTO = NONWORD_PREFIX + "goto" + NONWORD_POSTFIX;
	const std::string IF = NONWORD_PREFIX + "if" + NONWORD_POSTFIX;
	const std::string IN = NONWORD_PREFIX + "in" + NONWORD_POSTFIX;
	const std::string LOCAL = NONWORD_PREFIX + "local" + NONWORD_POSTFIX;
	const std::string NIL = NONWORD_PREFIX + "nil" + NONWORD_POSTFIX;
	const std::string NOT = NONWORD_PREFIX + "not" + NONWORD_POSTFIX;
	const std::string OR = NONWORD_PREFIX + "or" + NONWORD_POSTFIX;
	const std::string REPEAT = NONWORD_PREFIX + "repeat" + NONWORD_POSTFIX;
	const std::string RETURN = NONWORD_PREFIX + "return" + NONWORD_POSTFIX;
	const std::string THEN = NONWORD_PREFIX + "then" + NONWORD_POSTFIX;
	const std::string TRUE = NONWORD_PREFIX + "true" + NONWORD_POSTFIX;
	const std::string UNTIL = NONWORD_PREFIX + "until" + NONWORD_POSTFIX;
	const std::string WHILE = NONWORD_PREFIX + "while" + NONWORD_POSTFIX;

	const std::string LUA_IDENTIFIER = R"((?<=\b)(?!()" + LUA_KEYWORD + R"()))" + LUA_DECL;


	/* field seperator */
	const std::string FIELD_SEP = ",|;";


	/* binary operators */
	const std::string ADDITION = R"(\+)";
	const std::string SUBSTRUCTION = R"((?<!-)-(?!-))";
	const std::string MULTIPLY = R"(\*)";
	const std::string DIVITION = R"(\/)";
	const std::string FLOOR_DIVITION = "//";
	const std::string EXPONENTIATION = R"(\^)";
	const std::string MODULO = "%";
	const std::string BITWISE_AND = "&";
	const std::string BITWISE_XOR = "~(?!=)";
	const std::string BITWISE_OR = R"(\|)";
	const std::string RIGHT_SHIFT = ">>(?!=)";
	const std::string LEFT_SHIFT = "<<(?!=)";
	const std::string CONCAT = R"(\.\.)";
	const std::string LESSER = "(?<!<)<(?!<|=)";
	const std::string LESSER_EQUAL = "(?<!<)<=";
	const std::string GREATER = "(?<!>)>(?!>|=)";
	const std::string GREATER_EQUAL = "(?<!>)>=";
	const std::string EQUAL = "==";
	const std::string NOT_EQUAL = "~=";

	// AND is in keywords section
	// OR is in keywords section


	/* unary operators */
	// NEGATION operator is same as SUBSTRUCTION operator
	// NOT is in keywords section
	const std::string LENGTH = "#";
	// BITWISE_NOT is same as BITWISE_XOR


	/* brackets */
	const std::string BRACKET_OPEN_REGULAR = R"(\()";
	const std::string BRACKET_CLOSE_REGULAR = R"(\))";
	const std::string BRACKET_OPEN_CURLY = R"(\{)";
	const std::string BRACKET_CLOSE_CURLY = R"(\})";
	const std::string BRACKET_OPEN_SQUARE = R"((?<!\[)\[(?!\[))";
	const std::string BRACKET_CLOSE_SQUARE = R"((?<!\])\](?!\]))";


	/* comment */
	const std::string COMMENT = R"((?<!\]\])--(?!\[\[))";
	const std::string COMMENT_MULTI_START = R"(--\[\[)";
	const std::string COMMENT_MULTI_END = R"(\]\]--)";


	/* generic */
	const std::string REGULAR_EQUALIZER = "(?<!=|>|<|~)=(?!=)";
	const std::string PERIOD = R"((?<=\w)\.(?=[a-zA-Z_]))";
	const std::string NEWLINE = R"(\n)";


	const std::vector<Regex> default_set{
		//{ , LexemeType:: },

		/* basic types*/
		{ STR_SINGLE_LINE_SINGLE_QUOTE, LexemeType::STR_SINGLE_LINE_SINGLE_QUOTE },
		{ STR_SINGLE_LINE_DOUBLE_QUOTE, LexemeType::STR_SINGLE_LINE_DOUBLE_QUOTE },
		{ STR_MULTI_LINE_START, LexemeType::STR_MULTI_LINE_START },
		{ STR_MULTI_LINE_END, LexemeType::STR_MULTI_LINE_END },
		{ INT, LexemeType::INT },
		{ FLOAT, LexemeType::FLOAT },


		/* keywords, lua identifier */
		{ AND, LexemeType::BOOL_AND },
		{ BREAK, LexemeType::BREAK },
		{ DO, LexemeType::DO },
		{ ELSE, LexemeType::ELSE },
		{ ELSEIF, LexemeType::ELIF },
		{ END, LexemeType::LUA_END },
		{ FALSE, LexemeType::BOOL_FALSE },
		{ FOR, LexemeType::FOREACH },
		{ FUNCTION, LexemeType::FUNCTION_DEFINITION },
		{ GOTO, LexemeType::GOTO },
		{ IF, LexemeType::IF },
		{ IN, LexemeType::IN },
		//{ LOCAL, LexemeType::LOCAL },
		{ NIL, LexemeType::NIL },
		{ NOT, LexemeType::BOOL_NOT },
		{ OR, LexemeType::BOOL_OR },
		{ REPEAT, LexemeType::DO_WHILE },
		{ RETURN, LexemeType::RETURN },
		{ THEN, LexemeType::THEN },
		{ TRUE, LexemeType::BOOL_TRUE },
		{ UNTIL, LexemeType::UNTIL },
		{ WHILE, LexemeType::WHILE },

		{ LUA_IDENTIFIER, LexemeType::IDENTIFIER },
		{COMA, lexical::LexemeType::COMA},

		/* field seperator */
		//{ FIELD_SEP, LexemeType::FIELD_SEP },

		/* binary operators */
		{ ADDITION, LexemeType::MATH_PLUS },
		{ SUBSTRUCTION, LexemeType::MATH_MINUS },
		{ MULTIPLY, LexemeType::MATH_MULT },
		{ DIVITION, LexemeType::MATH_DIV },
		//{ FLOOR_DIVITION, LexemeType:: },
		//{ EXPONENTIATION, LexemeType:: },
		{ MODULO, LexemeType::MATH_MOD },
		{ BITWISE_AND, LexemeType::MATH_AND },
		{ BITWISE_XOR, LexemeType::MATH_XOR },
		{ BITWISE_OR, LexemeType::MATH_OR },
		{ RIGHT_SHIFT, LexemeType::MATH_SHR },
		{ LEFT_SHIFT, LexemeType::MATH_SHL },
		//{ CONCAT, LexemeType:: },
		{ LESSER, LexemeType::BOOL_LESS },
		{ LESSER_EQUAL, LexemeType::BOOL_LESS_EQUAL },
		{ GREATER, LexemeType::BOOL_GREATER },
		{ GREATER_EQUAL, LexemeType::BOOL_GREATER_EQUAL },
		{ EQUAL, LexemeType::BOOL_EQUAL },
		{ NOT_EQUAL, LexemeType::BOOL_NOT_EQUAL },

		/* unary operators */
		//{ LENGTH, LexemeType:: },

		/* brackets */
		{ BRACKET_OPEN_REGULAR, LexemeType::OPEN_REGULAR },
		{ BRACKET_CLOSE_REGULAR, LexemeType::CLOSE_REGULAR },
		{ BRACKET_OPEN_CURLY, LexemeType::OPEN_CURLY },
		{ BRACKET_CLOSE_CURLY, LexemeType::CLOSE_CURLY },
		{ BRACKET_OPEN_SQUARE, LexemeType::OPEN_SQUARE },
		{ BRACKET_CLOSE_SQUARE, LexemeType::CLOSE_SQUARE },

		/* comment */
		{ COMMENT, LexemeType::SINGLE_LINE_COMMENT },
		{ COMMENT_MULTI_START, LexemeType::COMMENT_MULTI_START },
		{ COMMENT_MULTI_END, LexemeType::COMMENT_MULTI_END },

		/* general */
		{ REGULAR_EQUALIZER, LexemeType::REGULAR_EQUALIZER },
		{ PERIOD, LexemeType::PERIOD },
		{ NEWLINE, LexemeType::ENDLINE },

	};

	constexpr const bool DIVIDE_MODULES_BY_FILES = true;
}