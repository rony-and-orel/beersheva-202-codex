#pragma once
#include <list>
#include "lexeme.h"
#include "mapping.h"
#include "issue_board.h"
#include "util.h"

namespace codex::core::factories
{
	typedef std::vector<lexical::Lexeme>::const_iterator LexIter;
	typedef std::list<operations::OperationData*>::const_iterator OpIter;

	namespace io
	{
		struct FactoryOutput
		{
			std::list<mapping::Operation> ops;
			std::list<operations::Comment*> comments;
			issues::IssueBoard issues;
			bool success;

			FactoryOutput() :
				ops(), comments(), issues(), success(true)
			{}

			FactoryOutput(std::list<mapping::Operation>& passedOps, std::list<operations::Comment*>& passedComments, issues::IssueBoard passedIssues, bool passedSuccess) :
				ops(std::move(passedOps)), comments(passedComments), issues(std::move(passedIssues)), success(passedSuccess)
			{}
		};

		namespace lua
		{
			enum class ScopeStatus
			{
				NO_CHANGE = 0,
				CLOSE,
				OPEN,
				CLOSE_AND_OPEN
			};

			struct ParserInput
			{
				issues::IssueBoard& issues;
				const std::vector<lexical::Lexeme>& lexemes;
				LexIter& offset;
				const std::string_view filename;
				size_t& lineCount;
				const std::string_view fileContent;
				ScopeStatus& scopeStatus;
			};

			struct ParserArgs
			{
				issues::IssueBoard& issueBoard;
				const std::vector<lexical::Lexeme>& lexemes;
				LexIter offset;
				const std::string_view filename;
				size_t lineCount;
				const std::string_view fileContent;
				ScopeStatus scopeStatus;

				lua::ParserInput pass()
				{
					return { issueBoard, lexemes, offset, filename, lineCount, fileContent, scopeStatus };
				}
			};

			struct ParserOutput
			{
				operations::OperationData* op;
				bool success;
			};

			struct BuilderOutput
			{
				operations::values::BasicValue* value;
				bool success;
			};
		}

		namespace python
		{
			struct ParserInput
			{
				issues::IssueBoard& issues;
				const std::vector<lexical::Lexeme>& lexemes;
				LexIter& offset;
				const std::string_view filename;
				size_t& lineCount;
				const std::string_view fileContent;
				bool& needsScope;
			};

			struct ParserArgs
			{
				issues::IssueBoard& issueBoard;
				const std::vector<lexical::Lexeme>& lexemes;
				LexIter offset;
				const std::string_view filename;
				size_t lineCount;
				const std::string_view fileContent;
				bool needsScope;

				python::ParserInput pass()
				{
					return { issueBoard, lexemes, offset, filename, lineCount, fileContent, needsScope };
				}
			};

			struct ParserOutput
			{
				operations::OperationData* op;
				bool success;
			};

			struct BuilderOutput
			{
				operations::values::BasicValue* value;
				bool success;
			};

			struct Memoization
			{
				util::LookupTable<operations::Paths, std::string, size_t> vars;
			};
		}
	}
}