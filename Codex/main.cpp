#include <iostream>
#include <filesystem>
#include <fstream>
#include <regex>
#include <string>

#include "engine.h"

using namespace std;

int main()
{
	codex::Engine::run();

	return 0;
}

