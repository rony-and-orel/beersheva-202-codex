#pragma once

#include <vector>
#include <stack>
#include <unordered_map>
#include <string>
#include <memory>

#include "util.h"
#include "operations.h"

namespace codex
{
	namespace core
	{
		namespace mapping
		{
			typedef size_t Id;

			class Scope;
			class Operation;

			class Scope
			{
			public:
				explicit Scope() noexcept = default;

				Scope(const Scope&) = delete;
				Scope& operator=(const Scope&) = delete;

				Scope(Scope&& other) noexcept :
					_seq(std::move(other._seq))
				{}

				Scope& operator=(Scope&& other) noexcept
				{
					_seq = std::move(other._seq);

					return *this;
				}

				bool isEmpty() const noexcept
				{
					return _seq.empty();
				}

				constexpr std::list<Operation>* getSeq() noexcept
				{
					return &_seq;
				}

			private:
				std::list<Operation> _seq; //operation sequance
			};

			class Operation
			{
			public:
				//c'tor
				explicit Operation(std::unique_ptr<operations::OperationData>&& data) noexcept :
					_data(std::move(data)), _scope()
				{}

				Operation(const Operation&) = delete;
				Operation& operator=(const Operation&) = delete;

				//added c'tor implementation for std::move
				Operation(Operation&& other) noexcept :
					_scope(std::move(other._scope)), _data(std::move(other._data))
				{}

				//added c'tor implementation for std::move
				Operation& operator=(Operation&& other) noexcept
				{
					_scope = std::move(other._scope);
					_data = std::move(other._data);
					return *this;
				}

				Scope& getScope() noexcept
				{
					return _scope;
				}

				operations::OperationData* getData() const noexcept
				{
					return _data.get();
				}

			private:
				Scope _scope; //operation's scope
				std::unique_ptr<operations::OperationData> _data; //operation data
			};

			class CodeMap
			{
			public:
				class Cursor;

				explicit CodeMap() noexcept :
					_root(), _cursor(_root)
				{}

				explicit CodeMap(const CodeMap&) = delete;
				CodeMap& operator=(const CodeMap&) = delete;

				explicit CodeMap(CodeMap&& other) noexcept :
					_root(std::move(other._root)), _cursor(_root)
				{
					other.clear();
				}

				CodeMap& operator=(CodeMap&& other) noexcept
				{
					_root = std::move(other._root);
					_cursor = Cursor(_root);

					other.clear();

					return *this;
				}

				~CodeMap() = default;

				void clear()
				{
					_root = Scope();
					_cursor = Cursor(_root);
				}

				void append(Operation&& op) noexcept
				{
					operations::OperationType type = op.getData()->getType();

					if (type == operations::OperationType::SCOPE)
					{
						if (operations::Scope::cast(op.getData())->scopeType == operations::Scope::Type::OPENING_SCOPE)
						{
							_cursor.down();
						}
						else
						{
							_cursor.up();
							_cursor++;
						}
					}
					else
					{
						_cursor.insert(std::move(op));
					}
				}

				/* const and non-const getters */

				const Scope& getRoot() const
				{
					return _root;
				}

				Scope& getRoot()
				{
					return _root;
				}

				class Cursor
				{
				public:
					explicit Cursor(Scope& scope) noexcept :
						_stack()
					{
						_seq = scope.getSeq();
						_iterator = _seq->begin();
					}

					explicit Cursor(const Cursor& other) noexcept :
						_seq(other._seq), _iterator(other._iterator), _stack(other._stack)
					{
					}

					Cursor& operator=(const Cursor& other)
					{
						_seq = other._seq;
						_iterator = other._iterator;
						_stack = other._stack;

						return *this;
					}

					explicit Cursor(Cursor&& other) noexcept :
						_seq(std::move(other._seq)), _iterator(std::move(other._iterator))
					{}

					Cursor& operator=(Cursor&& other) noexcept
					{
						_seq = std::move(other._seq);
						_iterator = std::move(other._iterator);
						return *this;
					}

					//Note: will crash if position is invalid; use isValidOp() to test validity
					Operation& getOp()
					{
						return *_iterator;
					}

					bool isValidOp()
					{
						return _iterator != _seq->end();
					}

					std::list<Operation>& getSeq()
					{
						return *_seq;
					}

					std::list<Operation>::iterator getIterator()
					{
						return _iterator;
					}

					//Go up a scope level
					bool up() noexcept
					{
						bool flag = false;

						if (!_stack.empty())
						{
							_seq = _stack.top().first;
							_iterator = _stack.top().second;
							_stack.pop();
							flag = true;
						}
						return flag;
					}

					//Go down a scope level
					bool down() noexcept
					{
						bool flag = false;
						if (_iterator != _seq->end())
						{
							_stack.emplace(_seq, _iterator);
							_seq = _iterator->getScope().getSeq();
							_iterator = _seq->begin();
							flag = true;
						}
						return flag;
					}

					bool operator++(int) noexcept
					{
						bool flag = false;
						if (_iterator != _seq->end())
						{
							_iterator++;
							flag = true;
						}
						return flag;
					}

					bool operator--(int) noexcept
					{
						bool flag = false;
						if (_iterator != _seq->begin())
						{
							_iterator--;
							flag = true;
						}
						return flag;
					}

					/*
					adds a new Operation at current position. recommended to use operator++ after insert.
					input: Operation.
					output: none.
					*/

					std::list<Operation>::iterator insert(Operation&& op) noexcept
					{
						_iterator = _seq->insert(_seq->end(), std::move(op));
						return _iterator;
					}
				private:
					std::list<Operation>* _seq;
					std::list<Operation>::iterator _iterator; // of _seq
					std::stack<std::pair<std::list<Operation>*, std::list<Operation>::iterator>> _stack;
				};

			private:
				Scope _root;
				Cursor _cursor;
			};
		}
	}
}
