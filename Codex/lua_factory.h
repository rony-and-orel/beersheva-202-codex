#pragma once
#include <unordered_map>
#include <tuple>
#include <list>
#include <vector>
#include <algorithm>
#include <memory>
#include <string>
#include <functional>

#include "factory_io.h"
#include "mapping.h"

#include "lexeme.h"

namespace codex::core::factories::lua
{
	class Factory
	{
	public:
		/*
		Produces a list of operations by given lexemes.
		input: lexemes, in.filename and file content.
		output: operation list, list of comment references.
		*/
		static io::FactoryOutput produce(const std::vector<lexical::Lexeme>& lexemes, std::string_view filename, const std::string_view fileContent)
		{
			auto result = io::FactoryOutput();
			io::lua::ParserArgs args{ result.issues, lexemes, lexemes.begin(), filename, 1, fileContent, io::lua::ScopeStatus::NO_CHANGE };

			while (args.offset != lexemes.end())
			{
				if (Parsers::Recognizers::isEndline(lexemes.end(), args.offset))
				{
					args.lineCount++;
					args.offset++;
				}
				else if (Parsers::Recognizers::isComment(lexemes.end(), args.offset))
				{
					//save ref and insert to operation list
					result.comments.push_back(operations::Comment::cast(Parsers::parseComment(args.pass()).op));

					result.ops.emplace_back(std::unique_ptr<operations::OperationData>(result.comments.back()));
				}
				else if (args.scopeStatus != io::lua::ScopeStatus::NO_CHANGE || 
					Parsers::Recognizers::isEnd(args.lexemes.end(), args.offset))
				{
					if (args.scopeStatus == io::lua::ScopeStatus::CLOSE || 
						args.scopeStatus == io::lua::ScopeStatus::CLOSE_AND_OPEN ||
						Parsers::Recognizers::isEnd(args.lexemes.end(), args.offset))
					{
						result.ops.emplace_back(std::unique_ptr<operations::OperationData>(
							new operations::Scope(
								args.lineCount,
								args.filename,
								operations::Scope::Type::CLOSING_SCOPE
							))
						);
						if (Parsers::Recognizers::isEnd(args.lexemes.end(), args.offset))
						{
							args.offset++;
						}
					}
					if (args.scopeStatus == io::lua::ScopeStatus::OPEN ||
						args.scopeStatus == io::lua::ScopeStatus::CLOSE_AND_OPEN)
					{
						result.ops.emplace_back(std::unique_ptr<operations::OperationData>(
							new operations::Scope(
								args.lineCount,
								args.filename,
								operations::Scope::Type::OPENING_SCOPE
							))
						);
					}
					args.scopeStatus = io::lua::ScopeStatus::NO_CHANGE;
				}
				else
				{
					result.ops.emplace_back(parseOperation(args.pass())); //parse operation and append
				}
			}

			return result;
		}

	private:
		/*
		Parses the next operation.
		input: lexemes, offset, in.filename, line counter and tab counter.
		output: parsed operation data.
		*/
		static std::unique_ptr<operations::OperationData> parseOperation(io::lua::ParserInput in)
		{
			auto result = io::lua::ParserOutput(); //the chosen one

			//select the next operation and parse it

			if (Parsers::Recognizers::isElse(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseElse(in);
			}
			else if (Parsers::Recognizers::isIf(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseIf(in);
			}
			else if (Parsers::Recognizers::isWhile(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseWhile(in);
			}
			else if (Parsers::Recognizers::isAssign(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseAssign(in);
			}
			else if (Parsers::Recognizers::isForeach(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseForeach(in);
			}
			else if (Parsers::Recognizers::isReturn(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseReturn(in);
			}
			else if (Parsers::Recognizers::isExpression(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseExpression(in, false);
			}
			else if (Parsers::Recognizers::isOperator(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseOperator(in);
			}
			else if (Parsers::Recognizers::isCall(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseCall(in);
			}
			else if (Parsers::Recognizers::isValue(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseValue(in);
			}
			else if (Parsers::Recognizers::isVariable(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseVariable(in);
			}
			else if (Parsers::Recognizers::isBreak(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseBreak(in);
			}
			else if (Parsers::Recognizers::isFunctionDefinition(in.lexemes.end(), in.offset))
			{
				result = Parsers::parseFunctionDefinition(in);
			}
			else
			{
				in.issues.submit(issues::Issue::Type::UNSUPPORTED, issues::common::UNEXPECTED_TOKEN + "'" + std::string(in.offset->getMatch()) + "'", in.offset->getSpan(), in.lineCount);
				result = Parsers::parseError(in);
				in.offset++;
			}

			if (!result.success) //check if the parsing was unsuccessful
			{
				result = Parsers::parseError(in);
			}

			return std::unique_ptr<operations::OperationData>(result.op);
		}

		/* Operation Parsers */
		class Parsers
		{
		public:
			/* Operation Recognizers */
			class Recognizers
			{
			public:
				typedef const std::function<bool(const LexIter&, const LexIter&)>& RecoMethod;

				static inline bool isFunctionDefinition(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::FUNCTION_DEFINITION);
				}

				static inline bool isReturn(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::RETURN);
				}

				static inline bool isForeach(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::FOREACH);
				}

				static inline bool isWhile(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::WHILE);
				}

				static inline bool isIf(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::IF);
				}

				static inline bool isBreak(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::BREAK);
				}

				static inline bool isEndline(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::ENDLINE);
				}

				static inline bool isComment(const LexIter endIndicator, const LexIter& offset)
				{
					return isSingleLineComment(endIndicator, offset) || isMultilineCommentStart(endIndicator, offset);
				}

				static inline bool isSingleLineComment(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::SINGLE_LINE_COMMENT);
				}

				static inline bool isMultilineCommentStart(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::COMMENT_MULTI_START);
				}

				static inline bool isMultilineCommentEnd(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::COMMENT_MULTI_END);
				}
				
				static inline bool isEnd(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::LUA_END);
				}

				static inline bool isValue(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (isStringLiteral(endIndicator, offset) || isNumber(endIndicator, offset) || isOpenCruly(endIndicator, offset) || isBoolValue(endIndicator, offset) || isEmptyValue(endIndicator, offset));
				}

				static bool isVariable(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (isIdentifier(endIndicator, offset) && !isCall(endIndicator, offset));
				}

				static inline bool isComma(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::COMA);
				}

				static inline bool isPeriod(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::PERIOD);
				}

				static inline bool isIdentifier(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::IDENTIFIER);
				}

				static inline bool isStringLiteral(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (isApos(endIndicator, offset) || isQout(endIndicator, offset) || isMultilineStringStart(endIndicator, offset));
				}

				static inline bool isApos(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::STR_SINGLE_LINE_SINGLE_QUOTE);
				}

				static inline bool isQout(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::STR_SINGLE_LINE_DOUBLE_QUOTE);
				}
				
				static inline bool isMultilineStringStart(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::STR_MULTI_LINE_START);
				}
				
				static inline bool isMultilineStringEnd(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::STR_MULTI_LINE_END);
				}

				static inline bool isOpenRegular(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::OPEN_REGULAR);
				}

				static inline bool isClosingRegular(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::CLOSE_REGULAR);
				}

				static inline bool isOpenCruly(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::OPEN_CURLY);
				}
				
				static inline bool isDo(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::DO);
				}

				static inline bool isBoolValue(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::BOOL_FALSE || offset->getType() == lexical::LexemeType::BOOL_TRUE);
				}

				static inline bool isEmptyValue(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::NIL);
				}
				
				static inline bool isThen(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::THEN);
				}

				static inline bool isClosingCruly(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::CLOSE_CURLY);
				}

				static inline bool isNumber(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::INT || offset->getType() == lexical::LexemeType::FLOAT);
				}

				static inline bool isEqualizer(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::REGULAR_EQUALIZER);
				}

				static inline bool isOperator(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator &&
						((offset->getType() >= lexical::LexemeType::MATH_PLUS && offset->getType() <= lexical::LexemeType::MATH_NOT)
							|| (offset->getType() >= lexical::LexemeType::BOOL_NOT && offset->getType() <= lexical::LexemeType::BOOL_NOT_EQUAL));
				}

				static inline bool isElse(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::ELSE || offset->getType() == lexical::LexemeType::ELIF);
				}

				static inline bool isElif(const LexIter endIndicator, const LexIter& offset)
				{
					return offset != endIndicator && (offset->getType() == lexical::LexemeType::ELIF);
				}

				static bool isCall(const LexIter endIndicator, const LexIter& offset)
				{
					bool isFlag = false;
					LexIter lexIter = offset;

					if (isIdentifier(endIndicator, lexIter))
					{
						skipVariable(lexIter, endIndicator);
						if (isOpenRegular(endIndicator, lexIter))
						{
							isFlag = true;
						}
					}

					return isFlag;
				}

				static bool isAssign(const LexIter endIndicator, const LexIter& offset)
				{
					bool hasEqualizer = false; //reached equalizer flag
					bool hasAssigned = false; //reached variable that could be assigned to
					bool stop = false;

					// iterate over lexemes
					for (LexIter lexIter = offset; lexIter != endIndicator && !stop; lexIter++)
					{
						// if it's not a variable or a coma
						if (!isComma(endIndicator, lexIter))
						{
							if (isVariable(endIndicator, lexIter))
							{
								hasAssigned = true;
								skipVariable(lexIter, endIndicator);
								lexIter--;
							}
							else if (isEqualizer(endIndicator, lexIter))
							{
								hasEqualizer = true;
								stop = true;
							}
							else
							{
								stop = true;
							}
						}
					}
					return hasAssigned && hasEqualizer && stop;
				}

				static bool isExpression(const LexIter endIndicator, const LexIter& offset)
				{
					LexIter lexIter = offset;
					bool isFlag = true;
					bool stop = false;

					// iterate over lexemes
					while (lexIter != endIndicator && !stop)
					{
						if (isValue(endIndicator, lexIter))
						{
							skipLiteralValue(lexIter, endIndicator);
						}
						else if (isCall(endIndicator, lexIter))
						{
							skipCall(lexIter, endIndicator);
						}
						else if (isVariable(endIndicator, lexIter))
						{
							skipVariable(lexIter, endIndicator);
						}
						else if (!isOpenRegular(endIndicator, lexIter))
						{
							if (!isOperator(endIndicator, lexIter))
							{
								isFlag = false;
							}
							stop = true;
						}
						else
						{
							lexIter++;
						}
					}

					return isFlag && stop;
				}
			private:

				/*
				skips literal values (map, string, list).
				input: iterator, end iterator.
				output: none.
				*/
				static void skipLiteralValue(LexIter& iter, const LexIter endIndicator)
				{
					if (isStringLiteral(endIndicator, iter)) //skip string
					{
						while (iter != endIndicator && !isStringLiteral(endIndicator, iter))
						{
							iter++;
						}

						if (isStringLiteral(endIndicator, iter))
						{
							iter++;
						}
					}
					else if (isOpenCruly(endIndicator, iter)) //skip map
					{
						while (iter != endIndicator && !isClosingCruly(endIndicator, iter))
						{
							iter++;
						}
						if (isOpenCruly(endIndicator, iter))
						{
							iter++;
						}
					}
					else if (iter != endIndicator)
					{
						iter++;
					}
				}

				/*
				skips call.
				input: iterator, end iterator.
				output: none.
				*/
				static void skipCall(LexIter& iter, const LexIter endIndicator)
				{
					while (iter != endIndicator && !isClosingRegular(endIndicator, iter))
					{
						iter++;
					}
					if (isClosingRegular(endIndicator, iter))
					{
						iter++;
					}
				}

				/*
				skips variable.
				input: iterator, end iterator.
				output: none.
				*/
				static void skipVariable(LexIter& iter, const LexIter endIndicator)
				{
					while (iter != endIndicator && (isIdentifier(endIndicator, iter) || isPeriod(endIndicator, iter)))
					{
						iter++;
					}
				}
			};

			static io::lua::ParserOutput parseIf(io::lua::ParserInput in)
			{
				operations::If* result = nullptr;
				auto cond = io::lua::ParserOutput(); //condition
				bool success = true;

				advance(in.lexemes.end(), in.offset); //ignore 'if'

				cond = getAssignable(in, false); //get condition
				success = cond.success;

				if (success) // if succeeded
				{
					result = new operations::If(in.lineCount, in.filename, operations::Expression::cast(cond.op));
					success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isThen); //ignore 'colon'

					if (!success && in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "'then", in.offset->getSpan(), in.lineCount);
					}
				}
				in.scopeStatus = io::lua::ScopeStatus::OPEN;

				return { result, success };
			}

			static io::lua::ParserOutput parseElse(io::lua::ParserInput in)
			{
				auto cond = io::lua::ParserOutput(); //condition
				operations::Else* result = nullptr;
				bool isElif = Recognizers::isElif(in.lexemes.end(), in.offset);
				bool success = true;

				advance(in.lexemes.end(), in.offset); //ignore 'else'

				if (isElif)
				{
					cond = getAssignable(in, false); //get condition
					success = cond.success;
				}
				if (success) // if succeeded
				{
					result = new operations::Else(in.lineCount, in.filename, operations::Expression::cast(cond.op));
					
					if (isElif)
					{
						success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isThen); //ignore 'colon'
					}
					if (!success && in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "'then'", in.offset->getSpan(), in.lineCount);
					}
				}
				in.scopeStatus = io::lua::ScopeStatus::CLOSE_AND_OPEN;

				return { result, success };
			}

			static io::lua::ParserOutput parseVariable(io::lua::ParserInput in)
			{
				auto name = getFullName(in); //get name
				operations::Variable* result = nullptr;
				bool success = name.second;

				if (success) // if succeeded
				{
					result = new operations::Variable(in.lineCount, in.filename, { name.first, operations::VariableType::NONE, operations::AccessModefier::NONE, 0 });
				}

				return { result, success };
			}

			static io::lua::ParserOutput parseOperator(io::lua::ParserInput in)
			{
				operations::Operator::Type type = operations::Operator::Type::UNKNOWN;
				bool success = true;

				//switch between possible arithmatic operation types
				switch (in.offset->getType())
				{
				case lexical::LexemeType::MATH_PLUS:
					type = operations::Operator::Type::ADD;
					break;
				case lexical::LexemeType::MATH_MINUS:
					type = operations::Operator::Type::SUB;
					break;
				case lexical::LexemeType::MATH_MOD:
					type = operations::Operator::Type::MOD;
					break;
				case lexical::LexemeType::MATH_MULT:
					type = operations::Operator::Type::MUL;
					break;
				case lexical::LexemeType::MATH_DIV:
					type = operations::Operator::Type::DIV;
					break;
				case lexical::LexemeType::MATH_SHR:
					type = operations::Operator::Type::SHR;
					break;
				case lexical::LexemeType::MATH_SHL:
					type = operations::Operator::Type::SHL;
					break;
				case lexical::LexemeType::MATH_AND:
					type = operations::Operator::Type::BIN_AND;
					break;
				case lexical::LexemeType::MATH_OR:
					type = operations::Operator::Type::BIN_OR;
					break;
				case lexical::LexemeType::MATH_NOT:
					type = operations::Operator::Type::BIN_NOT;
					break;
				case lexical::LexemeType::MATH_XOR:
					type = operations::Operator::Type::XOR;
					break;
				case lexical::LexemeType::BOOL_NOT:
					type = operations::Operator::Type::BOOL_NOT;
					break;
				case lexical::LexemeType::BOOL_AND:
					type = operations::Operator::Type::BOOL_AND;
					break;
				case lexical::LexemeType::BOOL_OR:
					type = operations::Operator::Type::BOOL_OR;
					break;
				case lexical::LexemeType::BOOL_EQUAL:
					type = operations::Operator::Type::EQUAL;
					break;
				case lexical::LexemeType::BOOL_LESS:
					type = operations::Operator::Type::LESS;
					break;
				case lexical::LexemeType::BOOL_GREATER:
					type = operations::Operator::Type::GREATER;
					break;
				case lexical::LexemeType::BOOL_LESS_EQUAL:
					type = operations::Operator::Type::LESS_EQUAL;
					break;
				case lexical::LexemeType::BOOL_GREATER_EQUAL:
					type = operations::Operator::Type::GREATER_EQUAL;
					break;
				case lexical::LexemeType::BOOL_NOT_EQUAL:
					type = operations::Operator::Type::NOT_EQUAL;
					break;
				default:
					success = false; //if nothing matches
					break;
				}
				if (success)
				{
					in.offset++;
				}
				else if (in.offset != in.lexemes.end())
				{
					in.issues.submit(issues::Issue::Type::UNSUPPORTED, issues::common::UNRECOGNIZED_OPERATOR, in.offset->getSpan(), in.lineCount);
				}

				return { new operations::Operator(in.lineCount, in.filename, type), success };
			}

			static io::lua::ParserOutput parseValue(io::lua::ParserInput in)
			{
				auto result = io::lua::BuilderOutput();
				bool success = true;

				//switch between possible values and build the matched value
				if (Parsers::Recognizers::isStringLiteral(in.lexemes.end(), in.offset))
				{
					result = buildStringValue(in);
				}
				else if (Parsers::Recognizers::isNumber(in.lexemes.end(), in.offset))
				{
					result = buildNumberValue(in);
				}
				else if (Parsers::Recognizers::isOpenCruly(in.lexemes.end(), in.offset))
				{
					result = buildListValue(in);
				}
				else if (Parsers::Recognizers::isBoolValue(in.lexemes.end(), in.offset))
				{
					result = buildBoolValue(in);
				}
				else if (Parsers::Recognizers::isEmptyValue(in.lexemes.end(), in.offset))
				{
					result = buildEmptyValue(in);
				}
				else
				{
					success = false; //if nothing matches

					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::UNSUPPORTED, issues::common::UNRECOGNIZED_VAL, in.offset->getSpan(), in.lineCount);
					}
				}

				return { new operations::Value(in.lineCount, in.filename, std::unique_ptr<operations::values::BasicValue>(result.value)), success && result.success };
			}

			static io::lua::ParserOutput parseExpression(io::lua::ParserInput in, bool isInclosed)
			{
				LexIter start = in.offset;
				operations::Operator* container = nullptr; //op container
				operations::Expression* result = nullptr;
				auto op = io::lua::ParserOutput(); //return value container
				auto comps = std::list<operations::OperationData*>(); //components
				auto element = io::lua::ParserOutput(); //return value container
				bool success = true;
				bool stop = false;

				//lambdas for arithmatic operation ordering
				const std::vector<std::function<bool(operations::Operator::Type)>> levels = {
					[&](operations::Operator::Type type) { return type == operations::Operator::Type::BIN_NOT; },
					[&](operations::Operator::Type type) { return type >= operations::Operator::Type::DIV && type < operations::Operator::Type::BIN_NOT; },
					[&](operations::Operator::Type type) { return type == operations::Operator::Type::SUB; },
					[&](operations::Operator::Type type) { return type >= operations::Operator::Type::ADD && type < operations::Operator::Type::SUB; },
					[&](operations::Operator::Type type) { return type >= operations::Operator::Type::GREATER && type <= operations::Operator::Type::NOT_EQUAL; },
					[&](operations::Operator::Type type) { return type == operations::Operator::Type::BOOL_NOT; },
					[&](operations::Operator::Type type) { return type >= operations::Operator::Type::BOOL_AND && type < operations::Operator::Type::BOOL_NOT; }
				};

				while (in.offset != in.lexemes.end() && !stop && success)
				{
					if (Recognizers::isOpenRegular(in.lexemes.end(), in.offset))
					{
						in.offset++;
						element = getAssignable(in, true); //get element that is inside the brackets
						success = element.success;

						comps.emplace_back(element.op);
					}
					else if (Recognizers::isClosingRegular(in.lexemes.end(), in.offset))
					{
						if (isInclosed) //skip bracket only when expression is inclosed in brackets
						{
							in.offset++;
						}
						stop = true;
					}
					else if (Recognizers::isOperator(in.lexemes.end(), in.offset))
					{
						op = parseOperator(in); //get op
						success = op.success;

						comps.emplace_back(op.op);
					}
					else if (Recognizers::isCall(in.lexemes.end(), in.offset) || Recognizers::isValue(in.lexemes.end(), in.offset) || Recognizers::isVariable(in.lexemes.end(), in.offset))
					{
						element = getExpressionElement(in); //get expression element
						success = element.success;

						comps.emplace_back(element.op);
					}
					else
					{
						stop = true;
					}
				}

				// iterate over levels
				for (auto levelcheck = levels.begin(); levelcheck != levels.end() && comps.size() != 1 && success; levelcheck++)
				{
					// for every element in the expression
					for (OpIter compIter = comps.begin(); compIter != comps.end() && success; compIter++)
					{
						if ((*compIter)->getType() == operations::OperationType::OPERATOR) //if it's an op
						{
							container = operations::Operator::cast(*compIter); //cast
							if ((*levelcheck)(container->opType)) //parse nodes only for the current operation level
							{
								//switch between operations
								switch (container->opType)
								{
								case operations::Operator::Type::BIN_NOT:
								case operations::Operator::Type::BOOL_NOT:
									//check if operands are valid
									if (isValidOperand(comps.end(), std::next(compIter)))
									{
										//parse
										container->operands.emplace_back(operations::Assignable::cast(*std::next(compIter)));

										//consume operands and create condition
										compIter = comps.erase(compIter, std::next(std::next(compIter)));
										compIter = comps.emplace(compIter, new operations::Expression(in.lineCount, in.filename, std::unique_ptr<operations::Operator>(container), false));
									}
									else
									{
										success = false;
									}
									break;
								case operations::Operator::Type::SUB:
									//if there is an op before sub (like 1 + -2)
									if ((compIter != comps.begin() && (*std::prev(compIter))->getType() == operations::OperationType::OPERATOR) || compIter == comps.begin())
									{
										//check if operands are valid
										if (isValidOperand(comps.end(), std::next(compIter)))
										{
											//parse
											container->operands.emplace_back(operations::Assignable::cast(*std::next(compIter)));

											//consume operands and create condition
											compIter = comps.erase(compIter, std::next(std::next(compIter)));
											compIter = comps.emplace(compIter, new operations::Expression(in.lineCount, in.filename, std::unique_ptr<operations::Operator>(container), false));
										}
										else
										{
											success = false;
										}
										break;
									}
								case operations::Operator::Type::XOR:
								case operations::Operator::Type::SHR:
								case operations::Operator::Type::SHL:
								case operations::Operator::Type::BIN_OR:
								case operations::Operator::Type::MUL:
								case operations::Operator::Type::MOD:
								case operations::Operator::Type::DIV:
								case operations::Operator::Type::BIN_AND:
								case operations::Operator::Type::EQUAL:
								case operations::Operator::Type::LESS:
								case operations::Operator::Type::GREATER:
								case operations::Operator::Type::LESS_EQUAL:
								case operations::Operator::Type::GREATER_EQUAL:
								case operations::Operator::Type::NOT_EQUAL:
								case operations::Operator::Type::BOOL_AND:
								case operations::Operator::Type::BOOL_OR:
								case operations::Operator::Type::ADD:
									//check if operands are valid
									if (compIter != comps.begin() && isValidOperand(comps.end(), std::next(compIter)) && isValidOperand(comps.end(), std::prev(compIter)))
									{
										//parse
										container->operands.emplace_back(operations::Assignable::cast(*std::prev(compIter)));
										container->operands.emplace_back(operations::Assignable::cast(*std::next(compIter)));

										//consume operands and create expression
										compIter = comps.erase(std::prev(compIter), std::next(std::next(compIter)));
										compIter = comps.emplace(compIter, new operations::Expression(in.lineCount, in.filename, std::unique_ptr<operations::Operator>(container), false));
									}
									else
									{
										success = false;
									}
									break;
								default:
									success = false;
									break;
								}
							}
						}
					}
				}

				//issue submits
				if (!success)
				{
					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::INVALID_EXPR, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
					}
					else if (start != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::INVALID_EXPR, { start->getSpan()[0], (ptrdiff_t)in.fileContent.size() - 1 }, in.lineCount);
					}
				}

				if (success && comps.size() == 1)
				{
					result = operations::Expression::cast(*comps.begin());
					result->isInclosed = isInclosed;
				}

				return { result, success && comps.size() == 1 }; 
			}

			static io::lua::ParserOutput parseAssign(io::lua::ParserInput in)
			{
				auto limit = [&](const LexIter& end, const LexIter& offset) {
					return Recognizers::isEndline(end, offset) || Recognizers::isComment(end, offset);
				};

				LexIter start = in.offset;
				auto vars = getVarList(in, false);
				auto assigned = std::list<std::pair<std::unique_ptr<operations::Variable>, std::unique_ptr<operations::Assignable>>>(); //list of varible assigns
				auto varIter = vars.first.begin(); //variables iterator
				bool success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isEqualizer);
				auto vals = getAssignableList(in, limit);
				auto valIter = vals.first.begin(); //values iterator

				if (vars.second && vals.second) //if succeeded
				{
					success = vals.first.size() == vars.first.size();
					if (success)
					{
						while (varIter != vars.first.end() && valIter != vals.first.end())
						{
							assigned.emplace_back(std::move(*varIter), std::move(*valIter)); //set variable and assigned value
							varIter++;
							valIter++;
						}
					}
					else
					{
						if (in.offset != in.lexemes.end())
						{
							in.issues.submit(issues::Issue::Type::SYNTAX, vals.first.size() < vars.first.size() ? issues::common::FEW_VALUES_ASSIGNED : issues::common::TO_MUCH_VALUES_ASSIGNED, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
						}
						else if (start != in.lexemes.end())
						{
							in.issues.submit(issues::Issue::Type::SYNTAX, vals.first.size() < vars.first.size() ? issues::common::FEW_VALUES_ASSIGNED : issues::common::TO_MUCH_VALUES_ASSIGNED, { start->getSpan()[0], (ptrdiff_t)in.fileContent.size() - 1 }, in.lineCount);
						}
					}
				}
				else
				{
					success = false;
				}

				return { new operations::Assign(in.lineCount, in.filename, std::move(assigned), operations::Assign::Type::REGULAR), success };
			}

			static io::lua::ParserOutput parseFunctionDefinition(io::lua::ParserInput in)
			{
				operations::FunctionDefinition* result = nullptr;
				auto params = std::pair<std::vector<std::unique_ptr<operations::VariableDeclaration>>, bool>(); //function parameters
				std::string_view name = std::string_view(); //function name
				bool success = true;

				advance(in.lexemes.end(), in.offset); //skip 'function'

				if (Recognizers::isIdentifier(in.lexemes.end(), in.offset))
				{
					name = in.offset->getMatch(); //get name
					advance(in.lexemes.end(), in.offset); //skip name
				}
				else
				{
					success = false;
					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::INVALID_NAME, in.offset->getSpan(), in.lineCount);
					}
				}

				if (success)
				{
					params = getParamList(in);
					success = params.second;

					if (success)
					{
						result = new operations::FunctionDefinition(in.lineCount, in.filename, operations::VariableType::NONE, name, std::move(params.first));
					}
				}
				in.scopeStatus = io::lua::ScopeStatus::OPEN;

				return { result, success };
			}

			static io::lua::ParserOutput parseCall(io::lua::ParserInput in)
			{
				operations::Call* result = nullptr;
				auto name = getFullName(in);
				auto args = getArgumentList(in);
				bool success = args.second && name.second;

				if (success) //if succeeded
				{
					result = new operations::Call(in.lineCount, in.filename, name.first, std::move(args.first));
				}

				return { result, success };
			}

			static io::lua::ParserOutput parseReturn(io::lua::ParserInput in)
			{
				auto limit = [&](const LexIter& end, const LexIter& offset) {
					return Recognizers::isEnd(end, offset) || Recognizers::isEndline(end, offset);
				};

				auto retArgs = std::pair<std::vector<std::unique_ptr<operations::Assignable>>, bool>();
				operations::Return* result = nullptr;
				bool success = true;

				advance(in.lexemes.end(), in.offset); //ignore 'return'
				retArgs = getAssignableList(in, limit);
				success = retArgs.second;

				if (success) //if succeeded
				{
					result = new operations::Return(in.lineCount, in.filename, std::move(retArgs.first));
				}

				return { result, success };
			}

			static io::lua::ParserOutput parseForeach(io::lua::ParserInput in)
			{
				LexIter start = in.offset;
				auto iters = std::pair<std::vector<std::unique_ptr<operations::Variable>>, bool>(); //foreach iterators
				auto obj = io::lua::ParserOutput(); //iteratable object
				operations::Foreach* result = nullptr;
				bool success = true;

				advance(in.lexemes.end(), in.offset); //skip 'for'

				iters = getVarList(in, false);
				success = iters.second;

				if (success)
				{
					advance(in.lexemes.end(), in.offset);

					obj = getIteratableObject(in);
					success = obj.success;
					if (success) //if succeeded
					{
						result = new operations::Foreach(in.filename, in.lineCount, std::move(iters.first), operations::Iteratable::cast(obj.op));
						success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isDo); //ignore 'colon'

						if (!success && in.offset != in.lexemes.end())
						{
							in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "'do'", in.offset->getSpan(), in.lineCount);
						}
					}
					else if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::CANNOT_ITERATE, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
					}
				}
				else if (in.offset != in.lexemes.end())
				{
					in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::MISSING_ITERS, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
				}
				in.scopeStatus = io::lua::ScopeStatus::OPEN;

				return { result, success };
			}

			static io::lua::ParserOutput parseWhile(io::lua::ParserInput in)
			{
				operations::While* result = nullptr;
				auto cond = io::lua::ParserOutput(); //condition
				bool success = true;

				advance(in.lexemes.end(), in.offset); //ignore 'while'

				cond = getAssignable(in, false); //get condition
				success = cond.success;

				if (success) //if succeeded
				{
					result = new operations::While(in.lineCount, in.filename, operations::Expression::cast(cond.op));
					success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isDo); //ignore 'colon'

					if (!success && in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "'do'", in.offset->getSpan(), in.lineCount);
					}
				}
				in.scopeStatus = io::lua::ScopeStatus::OPEN;

				return { result, success };
			}

			static io::lua::ParserOutput parseBreak(io::lua::ParserInput in)
			{
				in.offset++;
				return { new operations::Break(in.lineCount, in.filename), true };
			}

			/*
			Error parsing method.
			input: lexemes, offset, in.filename and line counter.
			output: parsed pointer, new offset, success flag
			*/
			static io::lua::ParserOutput parseError(io::lua::ParserInput in) //DON'T TOUCH THIS
			{
				operations::Error* result = nullptr;
				auto issue = in.issues.last();

				if (issue.second)
				{
					result = new operations::Error(in.lineCount, in.filename, *issue.first);
				}
				else if (in.offset != in.lexemes.end())
				{
					in.issues.submit(in.offset->getSpan(), in.lineCount);
					result = new operations::Error(in.lineCount, in.filename, *in.issues.last().first);
				}
				else
				{
					in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::REACHED_EOF, { (ptrdiff_t)in.fileContent.size(), (ptrdiff_t)in.fileContent.size() }, in.lineCount);
					result = new operations::Error(in.lineCount, in.filename, *in.issues.last().first);
				}

				return { result, issue.second };
			}

			static io::lua::ParserOutput parseComment(io::lua::ParserInput in)
			{
				std::string result = std::string();
				std::array<ptrdiff_t, SPAN_SIZE> span = { in.offset->getSpan()[1], 0 };
				bool isMultiline = false;
				bool success = true;

				if (Recognizers::isSingleLineComment(in.lexemes.end(), in.offset))
				{
					in.offset++;
					while (in.offset != in.lexemes.end() && !Recognizers::isEndline(in.lexemes.end(), in.offset))
					{
						in.offset++;
					}
				}
				else
				{
					isMultiline = true;
					in.offset++;
					while (in.offset != in.lexemes.end() && !Recognizers::isMultilineCommentEnd(in.lexemes.end(), in.offset))
					{
						if (Recognizers::isEndline(in.lexemes.end(), in.offset))
						{
							in.lineCount++;
						}
						in.offset++;
					}
				}

				span[1] = in.offset == in.lexemes.end() ? in.fileContent.size() - 1 : in.offset->getSpan()[0];
				success = isMultiline ? advanceIf(in.lexemes.end(), in.offset, Recognizers::isMultilineCommentEnd) : success;
				result = std::string(in.fileContent.substr(span[0], span[1] - span[0]));

				return { new operations::Comment(in.lineCount, in.filename, result, span), success };
			}
		
		private:
			static inline void advance(LexIter endIndicator, LexIter& offset)
			{
				if (offset != endIndicator)
				{
					offset++;
				}
			}

			static inline bool advanceIf(LexIter endIndicator, LexIter& offset, Recognizers::RecoMethod& check)
			{
				bool success = check(endIndicator, offset);
				if (success)
				{
					advance(endIndicator, offset);
				}
				return success;
			}

			static inline void advanceUntil(LexIter endIndicator, LexIter& offset, Recognizers::RecoMethod& until)
			{
				while (offset != endIndicator && !until(endIndicator, offset))
				{
					offset++;
				}
			}
			
			static std::pair<operations::Paths, bool> getFullName(io::lua::ParserInput in)
			{
				operations::Paths name = operations::Paths();
				LexIter start = in.offset;
				bool expectPeriod = false; //expect period flag
				bool isError = false;

				//accept identifiers and periods
				while (in.offset != in.lexemes.end() && !isError && (Recognizers::isPeriod(in.lexemes.end(), in.offset) || Recognizers::isIdentifier(in.lexemes.end(), in.offset)))
				{
					if (expectPeriod && Recognizers::isPeriod(in.lexemes.end(), in.offset)) //check if it's a period and if it was expected
					{
						expectPeriod = false;
						in.offset++;
					}
					else if ((!expectPeriod) && Recognizers::isIdentifier(in.lexemes.end(), in.offset))
					{
						name.emplace_back(in.offset->getMatch()); //add to namespace
						expectPeriod = true;
						in.offset++;
					}
					else
					{
						isError = true;
					}
				}

				//submit issues
				if (isError && in.offset != in.lexemes.end())
				{
					in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::INVALID_NAME, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
				}
				else if (name.empty() && in.offset != in.lexemes.end())
				{
					in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EMPTY_NAME, { start->getSpan()[0], in.offset->getSpan()[1] }, in.lineCount);
				}

				return { name, !isError && !name.empty() };
			}

			static std::pair<std::vector<operations::Paths>, bool> getIdentifiersList(io::lua::ParserInput in, bool acceptEmptyList)
			{
				auto name = std::pair<operations::Paths, bool>(); //full name container
				auto names = std::vector<operations::Paths>(); //name list
				bool isError = false;
				bool stop = false;

				//accept periods, identifiers and coma
				while (in.offset != in.lexemes.end() && !isError && !stop)
				{
					if (Recognizers::isIdentifier(in.lexemes.end(), in.offset))
					{
						name = getFullName(in);

						if (name.second) //if succeeded
						{
							//append to name list
							names.emplace_back(name.first);
						}
						else
						{
							isError = true;
						}
					}
					else if (Recognizers::isComma(in.lexemes.end(), in.offset))
					{
						advance(in.lexemes.end(), in.offset);
					}
					else
					{
						stop = true;
					}
				}

				if (!acceptEmptyList && names.empty() && in.offset != in.lexemes.end())
				{
					if (isError && in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EMPTY_NAME_LIST, in.offset->getSpan(), in.lineCount);
					}
				}

				return { std::move(names), !isError && (acceptEmptyList ? true : !names.empty()) };
			}

			static std::pair<std::vector<std::unique_ptr<operations::Variable>>, bool> getVarList(io::lua::ParserInput in, bool acceptEmptyList)
			{
				auto vars = std::vector<std::unique_ptr<operations::Variable>>();
				auto names = getIdentifiersList(in, acceptEmptyList);

				if (names.second) //if succeeded
				{
					//create variables from names
					for (auto name : names.first)
					{
						vars.emplace_back(new operations::Variable(in.lineCount, in.filename, { name, operations::VariableType::NONE, operations::AccessModefier::NONE, 0 }));
					}
				}

				return { std::move(vars), names.second };
			}

			static std::pair<std::vector<std::unique_ptr<operations::VariableDeclaration>>, bool> getParamList(io::lua::ParserInput in)
			{
				auto params = std::vector<std::unique_ptr<operations::VariableDeclaration>>();
				auto vars = std::pair<std::vector<std::unique_ptr<operations::Variable>>, bool>();
				bool success = true;

				success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isOpenRegular); //ignore '('

				if (success)
				{
					vars = getVarList(in, true);
					success = vars.second;
					if (success)
					{
						//create varibale declarations from variables
						for (auto varIter = vars.first.begin(); varIter != vars.first.end(); varIter++)
						{
							params.emplace_back(new operations::VariableDeclaration(in.lineCount, in.filename, std::move(*varIter)));
						}

						success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isClosingRegular); //ignore ')'

						if (!success && in.offset != in.lexemes.end())
						{
							in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "')'", in.offset->getSpan(), in.lineCount);
						}
					}
				}
				else if (in.offset != in.lexemes.end())
				{
					in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "'('", in.offset->getSpan(), in.lineCount);
				}

				return { std::move(params), success };
			}

			static std::pair<std::vector<std::unique_ptr<operations::Assignable>>, bool> getArgumentList(io::lua::ParserInput in)
			{
				auto result = std::pair<std::vector<std::unique_ptr<operations::Assignable>>, bool>();
				bool success = true;

				success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isOpenRegular); //skip '('
				if (success)
				{
					result = getAssignableList(in, Recognizers::isClosingRegular); //get arguments
					success = result.second && advanceIf(in.lexemes.end(), in.offset, Recognizers::isClosingRegular);
					if (!success && in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "')'", in.offset->getSpan(), in.lineCount);
					}
				}
				else if (in.offset != in.lexemes.end())
				{
					in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "'('", in.offset->getSpan(), in.lineCount);
				}

				return { std::move(result.first), success };
			}

			static io::lua::ParserOutput getAssignable(io::lua::ParserInput in, bool isInclosed)
			{
				auto result = io::lua::ParserOutput();

				//switch between possible operations and parse
				if (Parsers::Recognizers::isExpression(in.lexemes.end(), in.offset))
				{
					result = parseExpression(in, isInclosed);
				}
				else if (Parsers::Recognizers::isCall(in.lexemes.end(), in.offset))
				{
					result = parseCall(in);
				}
				else if (Parsers::Recognizers::isValue(in.lexemes.end(), in.offset))
				{
					result = parseValue(in);
				}
				else if (Parsers::Recognizers::isVariable(in.lexemes.end(), in.offset))
				{
					result = parseVariable(in);
				}
				else
				{
					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::UNSUPPORTED, issues::common::UNRECOGNIZED_VAL, in.offset->getSpan(), in.lineCount);
					}
				}

				return result;
			}

			static io::lua::ParserOutput getExpressionElement(io::lua::ParserInput in)
			{
				auto result = io::lua::ParserOutput();

				//switch between possible operations and parse
				if (Parsers::Recognizers::isCall(in.lexemes.end(), in.offset))
				{
					result = parseCall(in);
				}
				else if (Parsers::Recognizers::isValue(in.lexemes.end(), in.offset))
				{
					result = parseValue(in);
				}
				else if (Parsers::Recognizers::isVariable(in.lexemes.end(), in.offset))
				{
					result = parseVariable(in);
				}

				return result;
			}

			static std::pair<std::vector<std::unique_ptr<operations::Assignable>>, bool> getAssignableList(io::lua::ParserInput in, Recognizers::RecoMethod& until)
			{
				auto elements = std::vector<std::unique_ptr<operations::Assignable>>();
				auto result = io::lua::ParserOutput();
				bool isError = false;
				bool stop = false;

				while (in.offset != in.lexemes.end() && !stop)
				{
					if (until(in.lexemes.end(), in.offset)) //if the limit is reached
					{
						stop = true; //stop
					}
					else if (!(Recognizers::isComma(in.lexemes.end(), in.offset)))
					{
						result = getAssignable(in, false); //get element
						if (result.success) //if succeeded
						{
							elements.emplace_back(operations::Assignable::cast(result.op)); //append
						}
						else
						{
							stop = true;
							isError = true;
						}
					}
					else
					{
						in.offset++; //ignore coma and space
					}
				}

				return { std::move(elements), !isError };
			}

			static io::lua::ParserOutput getIteratableObject(io::lua::ParserInput in)
			{
				auto result = io::lua::ParserOutput();
				bool success = true;

				//switch between possible matches and parse
				if (Recognizers::isValue(in.lexemes.end(), in.offset))
				{
					result = parseValue(in);
				}
				else if (Recognizers::isVariable(in.lexemes.end(), in.offset))
				{
					result = parseVariable(in);
				}
				else if (Recognizers::isCall(in.lexemes.end(), in.offset))
				{
					result = parseCall(in);
				}
				else
				{
					result.success = false;

					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::UNSUPPORTED, issues::common::UNRECOGNIZED_VAL, in.offset->getSpan(), in.lineCount);
					}
				}

				return result;
			}

			static bool isValidOperand(OpIter endIndicator, OpIter element)
			{
				return element != endIndicator
					&& ((*element)->getType() == operations::OperationType::EXPRESSION
						|| (*element)->getType() == operations::OperationType::VALUE
						|| (*element)->getType() == operations::OperationType::VARIABLE
						|| (*element)->getType() == operations::OperationType::CALL);
			}

			/*
			Builds list value.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed value, success flag.
			*/
			static io::lua::BuilderOutput buildListValue(io::lua::ParserInput in)
			{
				auto limit = [&](const LexIter& end, const LexIter& offset) {
					return Recognizers::isClosingCruly(end, offset);
				};
				LexIter start = in.offset;
				auto result = std::pair<std::vector<std::unique_ptr<operations::Assignable>>, bool>();
				bool isMutable = true;
				bool success = true;

				advance(in.lexemes.end(), in.offset);
				result = getAssignableList(in, limit);
				success = result.second;

				if (success)
				{
					success = advanceIf(in.lexemes.end(), in.offset, Recognizers::isClosingCruly);

					if (!success && in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::EXPECTED_TOKEN + "'}'", in.offset->getSpan(), in.lineCount);
					}
				}

				return { new operations::values::List(std::move(result.first), isMutable), success };
			}

			/*
			Builds numeric value.
			input: lexemes, offset, in.filename, line counter.
			output: parsed value, success flag.
			*/
			static io::lua::BuilderOutput buildNumberValue(io::lua::ParserInput in)
			{
				long double result = 0;
				bool success = true;

				try
				{
					result = std::stod(std::string(in.offset->getMatch()));
				}
				catch (std::exception&)
				{
					success = false;
					if (in.offset != in.lexemes.end())
					{
						in.issues.submit(issues::Issue::Type::SYNTAX, issues::common::UNRECOGNIZED_VAL, in.offset->getSpan(), in.lineCount);
					}
				}
				in.offset++;

				return { new operations::values::Number(result), success };
			}

			/*
			Builds Operator value.
			input: lexemes, offset, in.filename, line counter.
			output: parsed value, success flag.
			*/
			static io::lua::BuilderOutput buildBoolValue(io::lua::ParserInput in)
			{
				bool result = in.offset->getType() == lexical::LexemeType::BOOL_TRUE ? true : false;
				in.offset++;

				return { new operations::values::Bool(result), true };
			}

			/*
			Builds Operator value.
			input: lexemes, offset, in.filename, line counter.
			output: parsed value, success flag.
			*/
			static io::lua::BuilderOutput buildEmptyValue(io::lua::ParserInput in)
			{
				in.offset++;
				return { new operations::values::Empty(), true };
			}

			/*
			Builds string value.
			input: lexemes, offset, in.filename, line counter and file content.
			output: parsed value, success flag.
			*/
			static io::lua::BuilderOutput buildStringValue(io::lua::ParserInput in)
			{
				std::string result = "";
				std::array<ptrdiff_t, SPAN_SIZE> span = { 0 }; // string span
				auto endLiteralRec = Recognizers::isStringLiteral;
				bool isMultiline = false;
				bool stop = false;
				bool success = true;

				if (Recognizers::isMultilineStringStart(in.lexemes.end(), in.offset)) //check
				{
					endLiteralRec = Recognizers::isMultilineStringEnd;
					isMultiline = true;
				}
				else if(Recognizers::isApos(in.lexemes.end(), in.offset))
				{
					endLiteralRec = Recognizers::isApos;
				}
				else if (Recognizers::isQout(in.lexemes.end(), in.offset))
				{
					endLiteralRec = Recognizers::isQout;
				}

				span[0] = in.offset->getSpan()[1]; //take last literal end
				in.offset++; //skip last literal

				while (in.offset != in.lexemes.end() && !stop && success)
				{
					if (isMultiline && Recognizers::isEndline(in.lexemes.end(), in.offset)) //count new lines in multiline string
					{
						in.lineCount++;
					}
					else if (!isMultiline && Recognizers::isEndline(in.lexemes.end(), in.offset)) //if has illegal newline
					{
						success = false;
					}

					// if reched the end of the string
					if (endLiteralRec(in.lexemes.end(), in.offset))
					{
						stop = true;
					}
					else
					{
						in.offset++; //skip
					}
				}

				if (success && stop)
				{
					span[1] = in.offset->getSpan()[0]; //set end
					result = std::string(in.fileContent.substr(span[0], span[1] - span[0])); //copy content
					in.offset++;
				}

				return { new operations::values::String(result), success && stop };
			}
		};
	};
}