#pragma once
#include <string>
#include <vector>

#include "lexeme.h"

namespace codex::core::lexical::python::regexes
{
	//helper patterns
	const std::string ESCAPE_BACKSLASH = R"((?<!\\)(\\\\)*)"; //if matched, then letter proceeding has not been escaped
	const std::string PREFIX = R"((?:^|(?<=\s)))";
	const std::string POSTFIX = R"((?=\s|$))";
	const std::string NONWORD_PREFIX = R"((?:^|(?<=\W)))";
	const std::string NONWORD_POSTFIX = R"((?=\W|$))";

	//lexemes regexes
	const std::string COLON = R"((\:))";
	const std::string COMA = R"((\,))";
	const std::string PERIOD = R"(((?<!\d)\.(?!\d)))";
	const std::string SPACE = R"((?<=\S)([\t\ ]+))";
	const std::string APOS = "(" + ESCAPE_BACKSLASH + "')";
	const std::string QOUT = "(" + ESCAPE_BACKSLASH + "\")";

	const std::string COMMENT = R"((\#))";
	const std::string ENDLINE = R"((\n))";
	const std::string IDENTATION = R"(((?<=\n)[\t ]*(?![\s\#])))";
	const std::string IDENTIFIER = R"(((?<=\b)(?!((False|await|else|import|pass|None|break|except|in|raise|True|class|finally|is|return|and|continue|for|lambda|try|as|def|from|nonlocal|while|assert|del|global|not|with|async|elif|if|or|yield)(\W|$)))([A-Za-z_]\w*)))";

	const std::string MATH_PLUS = R"((\+(?!\=)))";
	const std::string MATH_MINUS = R"((\-(?!\=)))";
	const std::string MATH_MOD = R"((\%(?!\=)))";
	const std::string MATH_MULT = R"(((?<!\*)\*(?![\=\*])))";
	const std::string MATH_DIV = R"((\/(?!\=)))";
	const std::string MATH_SHR = R"((>>(?!\=)))";
	const std::string MATH_SHL = R"((<<(?!\=)))";
	const std::string MATH_XOR = R"((\^(?!\=)))";
	const std::string MATH_AND = R"((\&(?!\=)))";
	const std::string MATH_OR = R"((\|(?!\=)))";
	const std::string MATH_NOT = R"((\~))";

	const std::string BOOL_NOT = "(" + PREFIX + "not" + POSTFIX + ")";
	const std::string BOOL_AND = "(" + PREFIX + "and" + POSTFIX + ")";
	const std::string BOOL_OR = "(" + PREFIX + "or" + POSTFIX + ")";
	const std::string BOOL_EQUAL = "(==)";
	const std::string BOOL_LESS = R"(((?<!<)<(?!<)))";
	const std::string BOOL_GREATER = R"(((?<!>)>(?!>)))";
	const std::string BOOL_LESS_EQUAL = R"(((?<!<)<=))";
	const std::string BOOL_GREATER_EQUAL = R"(((?<!>)>=))";
	const std::string BOOL_NOT_EQUAL = "(!=)";

	const std::string INT = "(" + NONWORD_PREFIX + R"(((?<![\.\d])\d+(?![\.\d])))" + NONWORD_POSTFIX + ")";
	const std::string FLOAT = "(" + NONWORD_PREFIX + R"(((\d+\.\d+)|(\.\d+)|(\d+\.)))" + NONWORD_POSTFIX + ")";
	const std::string BOOL_TRUE = "(" + NONWORD_PREFIX + "True" + NONWORD_POSTFIX + ")";
	const std::string BOOL_FALSE = "(" + NONWORD_PREFIX + "False" + NONWORD_POSTFIX + ")";

	const std::string REGULAR_EQUALIZER = R"(((?<![\!\=\+\-\*\/\<\>&\|\^\%])(\=)(?!\=)))";
	const std::string ADD_EQUALIZER = R"(((?<!\=)(\+\=)(?!\=)))";
	const std::string SUB_EQUALIZER = R"(((?<!\=)(\-\=)(?!\=)))";
	const std::string MULT_EQUALIZER = R"(((?<!\=)(\*\=)(?!\=)))";
	const std::string MOD_EQUALIZER = R"(((?<!\=)(\%\=)(?!\=)))";
	const std::string DIV_EQUALIZER = R"(((?<!\=)(\/\=)(?!\=)))";
	const std::string SHL_EQUALIZER = R"(((?<!\=)(<<\=)(?!\=)))";
	const std::string SHR_EQUALIZER = R"(((?<!\=)(>>\=)(?!\=)))";
	const std::string XOR_EQUALIZER = R"(((?<!\=)(\^\=)(?!\=)))";
	const std::string AND_EQUALIZER = R"(((?<!\=)(&\=)(?!\=)))";
	const std::string OR_EQUALIZER = R"(((?<!\=)(\|\=)(?!\=)))";

	const std::string BREAK = "(" + PREFIX + "break" + POSTFIX + ")";
	const std::string CONTINUE = "(" + PREFIX + "pass" + POSTFIX + ")";
	const std::string IF = "(" + PREFIX + "if" + POSTFIX + ")";
	const std::string ELIF = "(" + PREFIX + "(elif)" + POSTFIX + ")";
	const std::string ELSE = "(" + PREFIX + "(else)" + NONWORD_POSTFIX + ")";
	const std::string NONE = "(" + NONWORD_PREFIX + "(None)" + NONWORD_POSTFIX + ")";
	const std::string RETURN = "(" + PREFIX + "return" + POSTFIX + ")";
	const std::string CLASS_DEFINITION = "(" + PREFIX + "class" + POSTFIX + ")";
	const std::string FUNCTION_DEFINITION = "(" + PREFIX + "def" + POSTFIX + ")";
	const std::string IN = "(" + PREFIX + "in" + POSTFIX + ")";
	const std::string IS = "(" + PREFIX + "is" + POSTFIX + ")";
	const std::string FOR = "(" + PREFIX + "for" + POSTFIX + ")";
	const std::string WHILE = "(" + PREFIX + "while" + POSTFIX + ")";
	const std::string WITH = "(" + PREFIX + "with" + POSTFIX + ")";
	const std::string FROM = "(" + PREFIX + "from" + POSTFIX + ")";
	const std::string IMPORT = "(" + PREFIX + "import" + POSTFIX + ")";
	const std::string ALIAS = "(" + IDENTIFIER + PREFIX + "as" + POSTFIX + IDENTIFIER + ")";

	const std::string DICT_START = R"((\{))";
	const std::string DICT_END = R"((\}))";
	const std::string LIST_START = R"((\[))";
	const std::string LIST_END = R"((\]))";
	const std::string OPEN_BRACKET = R"((\())";
	const std::string CLOSE_BRACKET = R"((\)))";


	const std::vector<lexical::Regex> default_set{
		{COMMENT, lexical::LexemeType::SINGLE_LINE_COMMENT},
		{SPACE, lexical::LexemeType::SPACE},
		{ENDLINE, lexical::LexemeType::ENDLINE},
		{IDENTATION, lexical::LexemeType::IDENTATION},
		{IMPORT, lexical::LexemeType::IMPORT},
		{FROM, lexical::LexemeType::FROM},
		{IDENTIFIER, lexical::LexemeType::IDENTIFIER},
		{MATH_PLUS, lexical::LexemeType::MATH_PLUS},
		{MATH_MINUS, lexical::LexemeType::MATH_MINUS},
		{MATH_MOD, lexical::LexemeType::MATH_MOD},
		{MATH_MULT, lexical::LexemeType::MATH_MULT},
		{MATH_DIV, lexical::LexemeType::MATH_DIV},
		{MATH_SHR, lexical::LexemeType::MATH_SHR},
		{MATH_SHL, lexical::LexemeType::MATH_SHL},
		{MATH_XOR, lexical::LexemeType::MATH_XOR},
		{MATH_AND, lexical::LexemeType::MATH_AND},
		{MATH_OR, lexical::LexemeType::MATH_OR},
		{MATH_NOT, lexical::LexemeType::MATH_NOT},
		{BOOL_NOT, lexical::LexemeType::BOOL_NOT},
		{BOOL_AND, lexical::LexemeType::BOOL_AND},
		{BOOL_OR, lexical::LexemeType::BOOL_OR},
		{BOOL_EQUAL, lexical::LexemeType::BOOL_EQUAL},
		{BOOL_GREATER, lexical::LexemeType::BOOL_GREATER},
		{BOOL_LESS, lexical::LexemeType::BOOL_LESS},
		{BOOL_LESS_EQUAL, lexical::LexemeType::BOOL_LESS_EQUAL},
		{BOOL_GREATER_EQUAL, lexical::LexemeType::BOOL_GREATER_EQUAL},
		{BOOL_NOT_EQUAL, lexical::LexemeType::BOOL_NOT_EQUAL},
		{INT, lexical::LexemeType::INT},
		{FLOAT, lexical::LexemeType::FLOAT},
		{BOOL_TRUE, lexical::LexemeType::BOOL_TRUE},
		{BOOL_FALSE, lexical::LexemeType::BOOL_FALSE},
		{APOS, lexical::LexemeType::APOS},
		{QOUT, lexical::LexemeType::QOUT},
		{PERIOD, lexical::LexemeType::PERIOD},
		{COLON, lexical::LexemeType::COLON},
		{COMA, lexical::LexemeType::COMA},
		{ADD_EQUALIZER, lexical::LexemeType::ADD_EQUALIZER},
		{SUB_EQUALIZER, lexical::LexemeType::SUB_EQUALIZER},
		{MULT_EQUALIZER, lexical::LexemeType::MULT_EQUALIZER},
		{MOD_EQUALIZER, lexical::LexemeType::MOD_EQUALIZER},
		{DIV_EQUALIZER, lexical::LexemeType::DIV_EQUALIZER},
		{SHR_EQUALIZER, lexical::LexemeType::SHR_EQUALIZER},
		{SHL_EQUALIZER, lexical::LexemeType::SHL_EQUALIZER},
		{XOR_EQUALIZER, lexical::LexemeType::XOR_EQUALIZER},
		{OR_EQUALIZER, lexical::LexemeType::OR_EQUALIZER},
		{AND_EQUALIZER, lexical::LexemeType::AND_EQUALIZER},
		{REGULAR_EQUALIZER, lexical::LexemeType::REGULAR_EQUALIZER},
		{BREAK, lexical::LexemeType::BREAK},
		{CONTINUE, lexical::LexemeType::CONTINUE},
		{IF, lexical::LexemeType::IF},
		{ELIF, lexical::LexemeType::ELIF},
		{ELSE, lexical::LexemeType::ELSE},
		{RETURN, lexical::LexemeType::RETURN},
		{CLASS_DEFINITION, lexical::LexemeType::CLASS_DEFINITION},
		{FUNCTION_DEFINITION, lexical::LexemeType::FUNCTION_DEFINITION},
		{IN, lexical::LexemeType::IN},
		{FOR, lexical::LexemeType::FOREACH},
		{WHILE, lexical::LexemeType::WHILE},
		{IS, lexical::LexemeType::IS},
		{WITH, lexical::LexemeType::WITH},
		{DICT_START, lexical::LexemeType::DICT_START},
		{DICT_END, lexical::LexemeType::DICT_END},
		{LIST_START, lexical::LexemeType::LIST_START},
		{LIST_END, lexical::LexemeType::LIST_END},
		{OPEN_BRACKET, lexical::LexemeType::OPEN_BRACKET},
		{CLOSE_BRACKET, lexical::LexemeType::CLOSE_BRACKET},
		{ALIAS, lexical::LexemeType::ALIAS},
		{NONE, lexical::LexemeType::EMPTY},
	};

	constexpr const bool DIVIDE_MODULES_BY_FILES = true;
}