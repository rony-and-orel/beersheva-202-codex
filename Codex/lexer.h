#pragma once
#include <array>
#include <vector>
#include <set>
#include <string>
#include <compare>
#include <regex>
#include <iostream>
#include <algorithm>
#include <boost/regex.hpp>
#include "languages.h"
#include "lexeme.h"

namespace codex::core::lexical
{
	/*
	returns the default regex set of a language by language code.
	input: language code.
	output: regex list.
	*/
	static std::vector<lexical::Regex> getDefaultRegexSet(languages::LanguageCode lang)
	{
		std::vector<Regex> default_set = std::vector<Regex>();
		switch (lang)
		{
		case languages::LanguageCode::LUA:
			default_set = lua::regexes::default_set;
			break;
		case languages::LanguageCode::PY:
			default_set = python::regexes::default_set;
			break;
		}
		return default_set;
	}

	class Lexer
	{
	public:
		/*
		Finds and returns all lexems found in code area using regex patterns.
		input: code, patterns.
		output: lexemes found in code.
		*/
		static std::vector<Lexeme> analyze(const std::string& code, std::vector<Regex> const& patterns)
		{
			std::vector<Lexeme> lexemes = std::vector<Lexeme>();
			//compares lexemes by span
			auto sorter = [&](const Lexeme& lex1, const Lexeme& lex2) {
				return std::less<ptrdiff_t>()(lex1.getSpan()[1], lex2.getSpan()[1]);
			};

			lexemes.reserve(code.size());
			for (const Regex& pattern : patterns) //for every pattern
			{
				//find matches and append
				std::vector<Lexeme> returnedLexemes = matchPattern(code, pattern);
				lexemes.insert(lexemes.end(), std::make_move_iterator(returnedLexemes.begin()),
					std::make_move_iterator(returnedLexemes.end()));
			}

			std::sort(lexemes.begin(), lexemes.end(), sorter); //sort lexemes
			return lexemes;
		}

		/*
		Filters lexemes by type.
		Lexemes with a matched type would be excluded from the list.
		input: lexeme list, operation type.
		output: none.
		*/
		static void filterLexemes(std::vector<Lexeme>& lexemes, LexemeType type)
		{
			auto filter = [&](const Lexeme& lex) { return lex.getType() == type; };

			lexemes.erase(std::remove_if(lexemes.begin(), lexemes.end(), filter), lexemes.end());
		}

		/*
		Finds and returns all lexems found in code area using a regex pattern.
		input: code, pattern.
		output: lexemes found in code.
		*/
		static std::vector<Lexeme> matchPattern(const std::string& code, const Regex& pattern)
		{
			std::vector<Lexeme> lexemes; //lexeme list

			//for every match found
			for (boost::sregex_iterator reg_iter = boost::sregex_iterator(std::begin(code), std::end(code), pattern.pattern); reg_iter != boost::sregex_iterator(); ++reg_iter)
			{
				//get match
				boost::smatch match = *reg_iter;
				//get span
				std::array<ptrdiff_t, 2> arr{ match.position(), match.position() + match.length() };
				//append
				lexemes.emplace_back(std::string_view(code.c_str() + match.position(), (size_t)match.length()), arr, pattern.type);
			}
			return lexemes;
		}
	};
}