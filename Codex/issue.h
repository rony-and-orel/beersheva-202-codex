#pragma once
#include <string>
#include <array>

#include "lexeme.h"

namespace codex
{
	namespace core
	{
		namespace issues
		{
			namespace common
			{
				const std::string UNEXPECTED_TOKEN = "Unexpected token: ";
				const std::string EXPECTED_TOKEN = "Unable to parse operation - expected: ";
				const std::string REACHED_EOF = "Unable to parse beyond EOF - maybe we forgot something?";
				const std::string INVALID_NAME = "Inadequate identifier name.";
				const std::string EMPTY_NAME = "Identifier name cannot be empty.";
				const std::string UNRECOGNIZED_VAL = "Value cannot be recognized.";
				const std::string UNRECOGNIZED_OPERATOR = "Unrecognized operator.";
				const std::string INVALID_EXPR = "Invalid expression.";
				const std::string INVALID_IMPORT = "Invalid import statement.";
				const std::string FEW_VALUES_ASSIGNED = "Expecting more values to be assigned.";
				const std::string TO_MUCH_VALUES_ASSIGNED = "To much values are assigned.";
				const std::string MULTIASSIGN_INVALID = "Cannot parse multiple assigns, only one assigment is allowed in this context.";
				const std::string MULTIFILE_IS_UNSUPPORTED = "Work with multiple source files and translations between standard libraries and modules are still not supported. So you may need to edit the code a little bit.";
				const std::string MISSING_ITERS = "Iterators are missing in statement.";
				const std::string CANNOT_ITERATE = "Invalid iteration object.";
				const std::string EMPTY_NAME_LIST = "Expected identifiers.";
				const std::string VAL_CANNOT_KEY = "Value must be immutable in order to be used as key.";
				const std::string SCOPE_EXPECTED = "Invalid scope boundary: scope status didn't required a change.";
			}

			struct Issue
			{
				enum class Type
				{
					UNKNOWN = 0,
					UNSUPPORTED,
					SYNTAX,
					WARNING,
					LOGICAL,
					OPTIMIZE,
					INFO,
					FAILURE
				};

				Type type; //issue type
				std::string msg; //message
				std::array<ptrdiff_t, SPAN_SIZE> span; //the span of the code which raised the issue
				size_t line; //the line where the issue was raised

				explicit Issue(std::array<ptrdiff_t, SPAN_SIZE> passedSpan, size_t passedLine) noexcept :
					type(Type::UNKNOWN), msg("Unknown error had occurred."), span(passedSpan), line(passedLine)
				{}

				explicit Issue(Type passedType, std::string passedMsg, std::array<ptrdiff_t, SPAN_SIZE> passedSpan, size_t passedLine) noexcept :
					type(passedType), msg(passedMsg), span(passedSpan), line(passedLine)
				{}

				explicit Issue(const Issue& other) noexcept :
					type(other.type), msg(other.msg), span(other.span), line(other.line)
				{}

				explicit Issue(Issue&& other) noexcept :
					type(std::move(other.type)), msg(std::move(other.msg)), span(std::move(other.span)), line(std::move(other.line))
				{
					other.type = Issue::Type::UNKNOWN;
					other.msg = "";
					other.span = { 0 };
					other.line = 0;
				}

				Issue& operator=(const Issue& other) noexcept
				{
					type = other.type;
					msg = other.msg;
					span = other.span;
					line = other.line;

					return *this;
				}

				Issue& operator=(Issue&& other) noexcept
				{
					type = std::move(other.type);
					msg = std::move(other.msg);
					span = std::move(other.span);
					line = std::move(other.line);

					return *this;
				}
			};
		}
	}
}