#pragma once
#include <vector>
#include <fstream>
#include <stack>
#include "factories.h"
#include "lexer.h"
#include "mapping.h"

namespace codex
{
	namespace core
	{
		namespace parsing
		{
			struct ProjectInfo
			{
				languages::LanguageCode language;
				std::string path;
				std::string content;
			};

			struct ParserResult
			{
				mapping::CodeMap code;
				issues::IssueBoard issues;
			};

			class Parser
			{
			public:
				static ParserResult compile(ProjectInfo& projectInfo)
				{
					auto regexes = lexical::getDefaultRegexSet(projectInfo.language);
					auto lexemes = lexical::Lexer::analyze(projectInfo.content, regexes);
					auto produced = factories::MainFactory::produce(projectInfo.language, lexemes, projectInfo.path, projectInfo.content);
					auto map = mapping::CodeMap();

					if (produced.success)
					{
						for (auto& op : produced.ops)
						{
							map.append(std::move(op));
						}
					}

					return { mapping::CodeMap(std::move(map)), produced.issues };
				}
			};
		}
	}
}