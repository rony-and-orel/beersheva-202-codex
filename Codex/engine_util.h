#pragma once

#include <boost/algorithm/string.hpp>

namespace codex
{
	namespace engine
	{
		namespace util
		{
			/*
			Un-escapes a string.
			input: string reference.
			output: none.
			*/
			void unescape(std::string& str)
			{
				boost::replace_all(str, R"(\\)", "\\");
			}
		}
	}
}