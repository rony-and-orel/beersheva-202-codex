#pragma once
#include <string>
#include <array>
#include <unordered_map>
#include <memory>
#include <vector>
#include <list>

#include "lexeme.h"
#include "issue.h"

namespace codex
{
	namespace core
	{
		namespace operations
		{
			typedef size_t Id;

			enum class OperationType
			{
				NONE = 0,
				NOP,
				IF,
				ELSE,
				VARIABLE,
				VALUE,
				OPERATOR,
				EXPRESSION,
				ASSIGN,
				VARIABLE_DECLARATION,
				FUNCTION_DEFINITION,
				CLASS_DEFINITION,
				MODULE_DEFINITION,
				CALL,
				SCOPE,
				MEMORY,
				RETURN,
				IMPORT,
				FOR,
				FOREACH,
				WHILE,
				UNTIL,
				ALIAS,
				SWITCH,
				CASE,
				DEFAULT,
				BREAK,
				CONTINUE,

				COMMENT,
				ERROR
			};

			enum class VariableType
			{
				NONE = 0,
			};

			enum class AccessModefier
			{
				NONE = 0,
			};

			typedef std::list<std::string_view> Paths;

			struct VariableInfo
			{
				Paths paths;
				VariableType type;
				AccessModefier mod;
				Id id;
			};

			class OperationData
			{
			private:
				OperationType _type; //operation type
				size_t _line;
				std::string_view _filename;

			public:
				OperationData(OperationType type, size_t line, std::string_view filename) :
					_type(type), _line(line), _filename(filename)
				{}

				//getter for type
				OperationType getType() const noexcept
				{
					return _type;
				}

				size_t getLine() const noexcept
				{
					return _line;
				}
			};


			struct Definition : public OperationData
			{
				Definition(OperationType type, size_t line, std::string_view filename) :
					OperationData(type, line, filename)
				{}

				static inline Definition* cast(OperationData* src)
				{
					return static_cast<Definition*>(src);
				}

				static inline const Definition* cast(const OperationData* src)
				{
					return static_cast<const Definition*>(src);
				}
			};

			struct Assignable : public OperationData
			{
				Assignable(OperationType type, size_t line, std::string_view filename) :
					OperationData(type, line, filename)
				{}

				static inline Assignable* cast(OperationData* src)
				{
					return static_cast<Assignable*>(src);
				}

				static inline const Assignable* cast(const OperationData* src)
				{
					return static_cast<const Assignable*>(src);
				}
			};

			struct Iteratable : Assignable
			{
				Iteratable(OperationType type, size_t line, std::string_view filename) :
					Assignable(type, line, filename)
				{}

				static inline Iteratable* cast(OperationData* src)
				{
					return static_cast<Iteratable*>(src);
				}

				static inline const Iteratable* cast(const OperationData* src)
				{
					return static_cast<const Iteratable*>(src);
				}
			};

			struct Variable : public Iteratable
			{
				VariableInfo info;

				Variable(size_t line, std::string_view filename, VariableInfo info) :
					Iteratable(OperationType::VARIABLE, line, filename), info(info)
				{}

				static inline Variable* cast(OperationData* src)
				{
					return static_cast<Variable*>(src);
				}

				static inline const Variable* cast(const OperationData* src)
				{
					return static_cast<const Variable*>(src);
				}
			};

			namespace values
			{
				enum class ValueType
				{
					STRING = 0,
					NUMBER,
					LIST,
					MAP,
					BOOL,
					EMPTY
				};

				struct BasicValue
				{
					ValueType type;

					BasicValue(ValueType type) :
						type(type)
					{}
				};

				struct Bool : BasicValue
				{
					bool cond;

					Bool(bool cond) :
						BasicValue(ValueType::BOOL), cond(cond)
					{
					}

					static inline Bool* cast(BasicValue* src)
					{
						return static_cast<Bool*>(src);
					}

					static inline const Bool* cast(const BasicValue* src)
					{
						return static_cast<const Bool*>(src);
					}
				};

				struct Number : BasicValue
				{
					long double num;

					Number(long double num) :
						BasicValue(ValueType::NUMBER), num(num)
					{
					}

					static inline Number* cast(BasicValue* src)
					{
						return static_cast<Number*>(src);
					}

					static inline const Number* cast(const BasicValue* src)
					{
						return static_cast<const Number*>(src);
					}
				};

				struct String : BasicValue
				{
					std::string str;

					String(std::string str) :
						BasicValue(ValueType::STRING), str(str)
					{
					}

					static inline String* cast(BasicValue* src)
					{
						return static_cast<String*>(src);
					}

					static inline const String* cast(const BasicValue* src)
					{
						return static_cast<const String*>(src);
					}
				};

				struct List : BasicValue
				{
					bool isMutable;
					std::vector<std::unique_ptr<Assignable>> elements;

					List(std::vector<std::unique_ptr<Assignable>> elements, bool isMutable) :
						BasicValue(ValueType::LIST), elements(std::move(elements)), isMutable(isMutable)
					{
					}

					static inline List* cast(BasicValue* src)
					{
						return static_cast<List*>(src);
					}

					static inline const List* cast(const BasicValue* src)
					{
						return static_cast<const List*>(src);
					}
				};

				struct Map : BasicValue
				{
					std::unordered_map<std::unique_ptr<BasicValue>, std::unique_ptr<Assignable>> map;

					Map(std::unordered_map<std::unique_ptr<BasicValue>, std::unique_ptr<Assignable>> map) :
						BasicValue(ValueType::MAP), map(std::move(map))
					{
					}

					static inline Map* cast(BasicValue* src)
					{
						return static_cast<Map*>(src);
					}

					static inline const Map* cast(const BasicValue* src)
					{
						return static_cast<const Map*>(src);
					}
				};

				struct Empty : BasicValue
				{
					Empty() :
						BasicValue(ValueType::EMPTY)
					{}

					static inline Empty* cast(BasicValue* src)
					{
						return static_cast<Empty*>(src);
					}

					static inline const Empty* cast(const BasicValue* src)
					{
						return static_cast<const Empty*>(src);
					}
				};
			}

			struct Value : public Iteratable
			{
				std::unique_ptr<values::BasicValue> value;

				Value(size_t line, std::string_view filename, std::unique_ptr<values::BasicValue> val) :
					Iteratable(OperationType::VALUE, line, filename), value(std::move(val))
				{}

				static inline Value* cast(OperationData* src)
				{
					return static_cast<Value*>(src);
				}

				static inline const Value* cast(const OperationData* src)
				{
					return static_cast<const Value*>(src);
				}
			};

			struct Operator : public OperationData
			{
				enum class Type
				{
					UNKNOWN = 0,
					//Binary operators
					ADD,
					SUB,
					DIV,
					MUL,
					MOD,
					SHL,
					SHR,
					XOR,
					BIN_AND,
					BIN_OR,
					BIN_NOT,
					INC,
					DEC,
					//Booleanic operators
					GREATER,
					LESS,
					EQUAL,
					GREATER_EQUAL,
					LESS_EQUAL,
					NOT_EQUAL,
					BOOL_AND,
					BOOL_OR,
					SAME_AS,
					BOOL_NOT
				};

				Type opType;
				std::list<std::unique_ptr<Assignable>> operands;

				Operator(size_t line, std::string_view filename, Type op_type) :
					OperationData(OperationType::OPERATOR, line, filename), opType(op_type), operands()
				{}

				Operator(size_t line, std::string_view filename, Type op_type, std::list<std::unique_ptr<Assignable>> operands) :
					OperationData(OperationType::OPERATOR, line, filename), opType(op_type), operands(std::move(operands))
				{}

				static inline Operator* cast(OperationData* src)
				{
					return static_cast<Operator*>(src);
				}

				static inline const Operator* cast(const OperationData* src)
				{
					return static_cast<const Operator*>(src);
				}
			};

			struct Memory : public Assignable
			{
				enum class Type
				{
					UNKNOWN = 0,
					ACCESS_VALUE,
					GET_PTR,
					GET_VALUE
				};

				Type opType;

				Memory(size_t line, std::string_view filename, Type op_type) :
					Assignable(OperationType::MEMORY, line, filename), opType(op_type)
				{}

				static inline Memory* cast(OperationData* src)
				{
					return static_cast<Memory*>(src);
				}

				static inline const Memory* cast(const OperationData* src)
				{
					return static_cast<const Memory*>(src);
				}
			};

			struct Expression : public Assignable
			{
				std::unique_ptr<Operator> head;
				bool isInclosed;

				Expression(size_t line, std::string_view filename, std::unique_ptr<Operator> head, bool isInclosed) :
					Assignable(OperationType::EXPRESSION, line, filename), head(std::move(head)), isInclosed(isInclosed)
				{}

				static inline Expression* cast(OperationData* src)
				{
					return static_cast<Expression*>(src);
				}

				static inline const Expression* cast(const OperationData* src)
				{
					return static_cast<const Expression*>(src);
				}
			};

			struct Call : public Iteratable
			{
				Paths paths;
				std::vector<std::unique_ptr<Assignable>> args;
				Id id;

				Call(size_t line, std::string_view filename, Paths paths, std::vector<std::unique_ptr<Assignable>> args) :
					Iteratable(OperationType::CALL, line, filename), paths(paths), args(std::move(args)), id(0)
				{}

				static inline Call* cast(OperationData* src)
				{
					return static_cast<Call*>(src);
				}

				static inline const Call* cast(const OperationData* src)
				{
					return static_cast<const Call*>(src);
				}
			};

			struct If : public OperationData
			{
				std::unique_ptr<Assignable> cond;

				If(size_t line, std::string_view filename, Assignable* cond) :
					OperationData(OperationType::IF, line, filename), cond(cond)
				{}

				static inline If* cast(OperationData* src)
				{
					return static_cast<If*>(src);
				}

				static inline const If* cast(const OperationData* src)
				{
					return static_cast<const If*>(src);
				}
			};

			struct Else : public OperationData
			{
				std::unique_ptr<Assignable> elifCond; //nullptr if it's just 'else'

				Else(size_t line, std::string_view filename, Assignable* _elif_cond) :
					OperationData(OperationType::ELSE, line, filename), elifCond(std::move(_elif_cond))
				{}

				static inline Else* cast(OperationData* src)
				{
					return static_cast<Else*>(src);
				}

				static inline const Else* cast(const OperationData* src)
				{
					return static_cast<const Else*>(src);
				}
			};


			struct Assign : public OperationData
			{
				enum class Type
				{
					UNKNOWN = 0,
					REGULAR,
					ADD,
					SUB,
					DIV,
					MUL,
					MOD,
					SHL,
					SHR,
					XOR,
					AND,
					OR,
					NOT,
				};

				Type assignType;
				std::list<std::pair<std::unique_ptr<Variable>, std::unique_ptr<Assignable>>> assigned;

				Assign(size_t line, std::string_view filename, std::list<std::pair<std::unique_ptr<Variable>, std::unique_ptr<Assignable>>> assigned, Type type) :
					OperationData(OperationType::ASSIGN, line, filename), assigned(std::move(assigned)), assignType(type)
				{}

				static inline Assign* cast(OperationData* src)
				{
					return static_cast<Assign*>(src);
				}

				static inline const Assign* cast(const OperationData* src)
				{
					return static_cast<const Assign*>(src);
				}
			};

			struct VariableDeclaration : public OperationData
			{
				std::unique_ptr<Variable> decl;

				VariableDeclaration(size_t line, std::string_view filename, std::unique_ptr<Variable> decl) :
					OperationData(OperationType::VARIABLE_DECLARATION, line, filename), decl(std::move(decl))
				{}

				static inline VariableDeclaration* cast(OperationData* src)
				{
					return static_cast<VariableDeclaration*>(src);
				}

				static inline const VariableDeclaration* cast(const OperationData* src)
				{
					return static_cast<const VariableDeclaration*>(src);
				}
			};

			struct FunctionDefinition : public Definition
			{
				VariableType rtype;
				std::string_view name;
				std::vector<std::unique_ptr<VariableDeclaration>> params;
				Id id;

				FunctionDefinition(size_t line, std::string_view filename, VariableType rtype, std::string_view name, std::vector<std::unique_ptr<VariableDeclaration>> params) :
					Definition(OperationType::FUNCTION_DEFINITION, line, filename), rtype(rtype), name(name), params(std::move(params)), id(0)
				{}

				static inline FunctionDefinition* cast(OperationData* src)
				{
					return static_cast<FunctionDefinition*>(src);
				}

				static inline const FunctionDefinition* cast(const OperationData* src)
				{
					return static_cast<const FunctionDefinition*>(src);
				}
			};

			struct ClassDefinition : public Definition
			{
				std::string_view name;
				std::vector<Paths> inherits;
				Id id;

				ClassDefinition(size_t line, std::string_view filename, std::string_view name, std::vector<Paths> inherits) :
					Definition(OperationType::CLASS_DEFINITION, line, filename), name(name), inherits(inherits), id(0)
				{}

				static inline ClassDefinition* cast(OperationData* src)
				{
					return static_cast<ClassDefinition*>(src);
				}

				static inline const ClassDefinition* cast(const OperationData* src)
				{
					return static_cast<const ClassDefinition*>(src);
				}
			};

			struct ModuleDefinition : public Definition
			{
				std::string_view name;
				Id id;

				ModuleDefinition(size_t line, std::string_view filename, std::string name) :
					Definition(OperationType::MODULE_DEFINITION, line, filename), name(name), id(0)
				{}

				static inline ModuleDefinition* cast(OperationData* src)
				{
					return static_cast<ModuleDefinition*>(src);
				}

				static inline const ModuleDefinition* cast(const OperationData* src)
				{
					return static_cast<const ModuleDefinition*>(src);
				}
			};

			struct Return : public OperationData
			{
				std::vector<std::unique_ptr<Assignable>> rets;

				Return(size_t line, std::string_view filename, std::vector<std::unique_ptr<Assignable>> ret) :
					OperationData(OperationType::RETURN, line, filename), rets(std::move(ret))
				{}

				static inline Return* cast(OperationData* src)
				{
					return static_cast<Return*>(src);
				}

				static inline const Return* cast(const OperationData* src)
				{
					return static_cast<const Return*>(src);
				}
			};

			struct Import : public OperationData
			{
				std::vector<Paths> modules;
				std::vector<Paths> specifiers;

				Import(size_t line, std::string_view filename, std::vector<Paths> modules, std::vector<Paths> specifiers) :
					OperationData(OperationType::IMPORT, line, filename), modules(modules), specifiers(specifiers)
				{}

				static inline Import* cast(OperationData* src)
				{
					return static_cast<Import*>(src);
				}

				static inline const Import* cast(const OperationData* src)
				{
					return static_cast<const Import*>(src);
				}
			};

			struct For : public OperationData
			{
				Variable indx;
				std::unique_ptr<Assignable> cond;
				std::unique_ptr<Assignable> step;

				For(size_t line, std::string_view filename, Variable indx, Assignable* cond, std::unique_ptr<Assignable> step) :
					OperationData(OperationType::FOR, line, filename), indx(indx), cond(std::move(cond)), step(std::move(step))
				{}

				static inline For* cast(OperationData* src)
				{
					return static_cast<For*>(src);
				}

				static inline const For* cast(const OperationData* src)
				{
					return static_cast<const For*>(src);
				}
			};

			struct Foreach : public OperationData
			{
				std::vector<std::unique_ptr<Variable>> iters; //iterators
				std::unique_ptr<Iteratable> obj;

				Foreach(std::string_view filename, size_t line, std::vector<std::unique_ptr<Variable>> iters, Iteratable* obj) :
					OperationData(OperationType::FOREACH, line, filename), iters(std::move(iters)), obj(obj)
				{}

				static inline Foreach* cast(OperationData* src)
				{
					return static_cast<Foreach*>(src);
				}

				static inline const Foreach* cast(const OperationData* src)
				{
					return static_cast<const Foreach*>(src);
				}
			};

			struct While : public OperationData
			{
				std::unique_ptr<Assignable> cond;

				While(size_t line, std::string_view filename, Assignable* cond) :
					OperationData(OperationType::WHILE, line, filename), cond(std::move(cond))
				{}

				static inline While* cast(OperationData* src)
				{
					return static_cast<While*>(src);
				}

				static inline const While* cast(const OperationData* src)
				{
					return static_cast<const While*>(src);
				}
			};

			struct Until : public OperationData
			{
				std::unique_ptr<Assignable> cond;

				Until(size_t line, std::string_view filename, Assignable* cond) :
					OperationData(OperationType::UNTIL, line, filename), cond(std::move(cond))
				{}

				static inline Until* cast(OperationData* src)
				{
					return static_cast<Until*>(src);
				}

				static inline const Until* cast(const OperationData* src)
				{
					return static_cast<const Until*>(src);
				}
			};

			struct Alias : public OperationData
			{
				VariableType prevType;
				VariableType currType;

				Alias(size_t line, std::string_view filename, VariableType prev_type, VariableType curr_type) :
					OperationData(OperationType::ALIAS, line, filename), currType(curr_type), prevType(prev_type)
				{}

				static inline Alias* cast(OperationData* src)
				{
					return static_cast<Alias*>(src);
				}

				static inline const Alias* cast(const OperationData* src)
				{
					return static_cast<const Alias*>(src);
				}
			};

			struct Switch : public OperationData
			{
				Variable arg;

				Switch(size_t line, std::string_view filename, Variable switch_arg) :
					OperationData(OperationType::SWITCH, line, filename), arg(switch_arg)
				{}

				static inline Switch* cast(OperationData* src)
				{
					return static_cast<Switch*>(src);
				}

				static inline const Switch* cast(const OperationData* src)
				{
					return static_cast<const Switch*>(src);
				}
			};

			struct Case : public OperationData
			{
				Value val;

				Case(size_t line, std::string_view filename, Value case_val) :
					OperationData(OperationType::CASE, line, filename), val(std::move(case_val))
				{}

				static inline Case* cast(OperationData* src)
				{
					return static_cast<Case*>(src);
				}

				static inline const Case* cast(const OperationData* src)
				{
					return static_cast<const Case*>(src);
				}
			};

			struct Break : public OperationData
			{
				Break(size_t line, std::string_view filename) :
					OperationData(OperationType::BREAK, line, filename)
				{}

				static inline Break* cast(OperationData* src)
				{
					return static_cast<Break*>(src);
				}

				static inline const Break* cast(const OperationData* src)
				{
					return static_cast<const Break*>(src);
				}
			};

			struct Continue : public OperationData
			{
				Continue(size_t line, std::string_view filename) :
					OperationData(OperationType::CONTINUE, line, filename)
				{}

				static inline Continue* cast(OperationData* src)
				{
					return static_cast<Continue*>(src);
				}

				static inline const Continue* cast(const OperationData* src)
				{
					return static_cast<const Continue*>(src);
				}
			};

			struct Error : public OperationData
			{
				issues::Issue issue;

				Error(size_t line, std::string_view filename, issues::Issue& issue) :
					OperationData(OperationType::ERROR, line, filename), issue(issues::Issue(issue))
				{}

				static inline Error* cast(OperationData* src)
				{
					return static_cast<Error*>(src);
				}

				static inline const Error* cast(const OperationData* src)
				{
					return static_cast<const Error*>(src);
				}
			};

			struct Comment : OperationData
			{
				std::string match;
				std::array<ptrdiff_t, SPAN_SIZE> span;

				Comment(size_t line, std::string_view filename, std::string match, std::array<ptrdiff_t, SPAN_SIZE> span) :
					OperationData(OperationType::COMMENT, line, filename), match(match), span(span)
				{}

				static inline Comment* cast(OperationData* src)
				{
					return static_cast<Comment*>(src);
				}

				static inline const Comment* cast(const OperationData* src)
				{
					return static_cast<const Comment*>(src);
				}
			};

			struct Scope : OperationData
			{
				enum class Type
				{
					UNKNOWN = 0,
					OPENING_SCOPE,
					CLOSING_SCOPE
				};

				Type scopeType;

				Scope(size_t line, std::string_view filename, Type scope_type) :
					OperationData(OperationType::SCOPE, line, filename), scopeType(scope_type)
				{}

				static inline Scope* cast(OperationData* src)
				{
					return static_cast<Scope*>(src);
				}

				static inline const Scope* cast(const OperationData* src)
				{
					return static_cast<const Scope*>(src);
				}
			};
		}
	}
}