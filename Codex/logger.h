#pragma once

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <string>
#include <mutex>

static std::mutex recordLock; //record logger event lock

namespace codex
{
    namespace engine
    {
        struct Logger
        {
            //aliases
            using Severity = boost::log::trivial::severity_level;

            /*
            Initiates the logger.
            input: none.
            output: none.
            */
            static void init()
            {
                boost::log::register_simple_formatter_factory<Severity, char>("Severity");

                boost::log::add_file_log
                (
                    boost::log::keywords::file_name = "codexEngine.log",
                    boost::log::keywords::format = "[time = %TimeStamp%] [thread = %ThreadID%] [type = %Severity%] %Message%",
                    boost::log::keywords::auto_flush = true
                );

                boost::log::core::get()->set_filter
                (
                    boost::log::trivial::severity >= boost::log::trivial::info
                );

                boost::log::add_common_attributes();
            }

            /*
            Create new event record.
            input: severity level, logger message.
            output: none.
            */
            static void record(Severity severity, std::string message)
            {
                switch (severity)
                {
                case Severity::info:
                    recordLock.lock();
                    BOOST_LOG_TRIVIAL(info) << message;
                    recordLock.unlock();
                    break;
                case Severity::debug:
                    recordLock.lock();
                    BOOST_LOG_TRIVIAL(debug) << message;
                    recordLock.unlock();
                    break;
                case Severity::error:
                    recordLock.lock();
                    BOOST_LOG_TRIVIAL(error) << message;
                    recordLock.unlock();
                    break;
                case Severity::fatal:
                    recordLock.lock();
                    BOOST_LOG_TRIVIAL(fatal) << message;
                    recordLock.unlock();
                    break;
                case Severity::trace:
                    recordLock.lock();
                    BOOST_LOG_TRIVIAL(trace) << message;
                    recordLock.unlock();
                    break;
                case Severity::warning:
                    recordLock.lock();
                    BOOST_LOG_TRIVIAL(warning) << message;
                    recordLock.unlock();
                    break;
                }
            }
        };
    }
}