#pragma once

#include "core.h"
#include "logger.h"
#include "engine_util.h"

#include <boost/bind/bind.hpp>
#include <boost/asio.hpp>
#include <nlohmann/json.hpp>
#include <chrono>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

constexpr auto END = '\0'; //null termination of buffer
constexpr auto DELIMITER = "\n"; //data delimiter
constexpr auto PORT = 8200;
constexpr auto THREAD_LIMIT = 32;
constexpr auto TIMEOUT_IN_MILISEC = 250;

static std::mutex taskLock; //task list lock
static std::mutex sendLock; //send lock
static std::mutex connLock; //connection list lock
static std::condition_variable taskNotify; //using taskLock

namespace codex
{
	class Engine
	{
	private:
		struct Task;

		//aliases
		using Json = nlohmann::json;
		using Tcp = boost::asio::ip::tcp;
		using Buffer = boost::asio::const_buffer;
		using Stream = boost::asio::streambuf;
		using Error = boost::system::error_code;
		using Service = boost::asio::io_service;
		using Severity = boost::log::trivial::severity_level;
		using Connection = std::list<Tcp::socket>::iterator;
		using TaskRef = std::list<Task>::iterator;
		using Logger = engine::Logger;
		using Clock = std::chrono::high_resolution_clock;
		using MilliSec = std::chrono::milliseconds;
		using TimePoint = std::chrono::time_point<Clock>;

		struct Request
		{
			std::string filename; //input file path
			std::string source;
			core::languages::LanguageCode target; //translation language target

			/*
			Decodes client request.
			input: socket buffer.
			output: request data.
			*/
			static bool decode(Buffer buffer, Request& request)
			{
				bool success = true;
				auto raw = std::string(boost::asio::buffer_cast<const char*>(buffer));
				auto json = Json::parse(raw.substr(0, raw.size() - 1), nullptr, false);

				if (json.is_discarded()) //if failed to parse json
				{
					success = false;
				}
				else if (exists(json, "filename") && exists(json, "source") && exists(json, "target"))
				{
					//build request
					request = { json["filename"].get<std::string>(), json["source"].get<std::string>(), core::languages::GetLanguageCode(json["target"].get<int>()) };
				}
				else
				{
					success = false;
				}

				return success;
			}

		private:

			/*
			Check if a field exists in json document.
			input: json, key.
			output: found flag.
			*/
			static bool exists(const Json& j, const std::string& key)
			{
				return j.find(key) != j.end();
			}
		};

		struct Response
		{
			enum class Code
			{
				ERR = 0,
				OK
			};

			Code code; //response code
			std::string translated; //translated code
			core::issues::IssueBoard issues; //issue list

			/*
			Encodes server response.
			input: response data.
			output: encoded string.
			*/
			static std::string encode(const Response& response)
			{
				return std::string(toJson(response).dump() + DELIMITER) + END;
			}

			/*
			Encodes invalid request message.
			input: response data.
			output: encoded string.
			*/
			static std::string invalid()
			{
				return std::string(toJson("invalid request.").dump() + DELIMITER) + END;
			}

			/*
			Encodes request timeout message.
			input: response data.
			output: encoded string.
			*/
			static std::string timeout()
			{
				return std::string(toJson("request timed out.").dump() + DELIMITER) + END;
			}

		private:
			/* json serializers */

			// serialize error
			static Json toJson(const std::string error)
			{
				Json json = {};

				json["code"] = Code::ERR;
				json["message"] = error;

				return json;
			}

			// serialize response
			static Json toJson(const Response& response)
			{
				Json json = {};

				json["code"] = (int)response.code;
				json["translated"] = response.translated;
				json["issues"] = toJson(response.issues);

				return json;
			}

			// serialize issues from issue board
			static Json toJson(const core::issues::IssueBoard& issues)
			{
				Json json = Json::array();

				for (auto& issue : issues.list())
				{
					json.push_back(toJson(issue));
				}

				return json;
			}

			// serialize issue
			static Json toJson(const core::issues::Issue& issue)
			{
				Json json = {};

				json["type"] = issue.type;
				json["message"] = issue.msg;
				json["span"] = { issue.span[0], issue.span[1] };
				json["line"] = issue.line;

				return json;
			}
		};

		struct Task
		{
			Connection socket; //socket connection
			Request request; //client request
			TimePoint time; //assignment time

			Task(Connection socket, const Request& request, TimePoint time) :
				socket(socket), request(request), time(time)
			{}

			Task(Task&& other) noexcept :
				socket(std::move(other.socket)), request(std::move(other.request)), time(std::move(other.time))
			{}
		};

		struct Attributes
		{
			Service service; //io service
			Tcp::acceptor acceptor; //acceptor
			std::queue<Task> pending; //pending tasks
			std::list<Task> processing; //processed tasks
			std::list<Tcp::socket> connections; //connections
			size_t working; //number of working threads

			Attributes() :
				service(), acceptor(service), pending(), processing(), working(0)
			{}

			Attributes(Attributes&) = delete;
			Attributes& operator=(Attributes&) = delete;
		};

		/*
		Send response to client.
		input: socket, response raw string.
		output: none.
		*/
		static void send(Tcp::socket& socket, std::string response)
		{
			std::lock_guard<std::mutex> lock(sendLock);
			if (socket.is_open()) //check if socket is still open
			{
				boost::asio::write(socket, boost::asio::buffer(response)); //send
			}
			else
			{
				Logger::record(Severity::error, "Client closed connection before he could receive response.");
			}
		}

		/*
		Send response to client.
		input: socket, response.
		output: none.
		*/
		static void send(Tcp::socket& socket, const Response& response)
		{
			send(socket, Response::encode(response));
		}

		/*
		Send invalid request message to client.
		input: attributes, connection.
		output: none.
		*/
		static void invalid(Attributes& attr, Connection socket)
		{
			send(std::ref(*socket), Response::invalid());

			closeConnection(attr, socket);

			Logger::record(Severity::error, "Closed connection with client. Client sent an invalid request.");
		}

		/*
		Send request timeout message to client.
		input: attributes, connection.
		output: none.
		*/
		static void timeout(Attributes& attr, TaskRef task)
		{
			send(std::ref(*task->socket), Response::timeout());

			closeTask(attr, task);

			Logger::record(Severity::error, "Closed connection with client. Request was timed out.");
		}

		/*
		Receive request from client.
		input: socket, request reference.
		output: success flag.
		*/
		static bool recv(Tcp::socket& socket, Request& request)
		{
			Stream stream = Stream();
			Error err = Error();
			bool success = true;

			if (socket.is_open())
			{
				boost::asio::read_until(socket, stream, DELIMITER, err); //read request

				if (err == boost::asio::error::eof) //if eof is reached
				{
					success = false;
				}
				else if (!Request::decode(stream.data(), request)) //try to decode request
				{
					success = false;
				}
			}
			else
			{
				success = false;
			}

			return success;
		}

		/*
		Listen for incoming connections.
		input: socket, request reference.
		output: success flag.
		*/
		static void listen(Attributes& attr)
		{
			//bind
			auto endpoint = Tcp::endpoint(boost::asio::ip::tcp::v4(), PORT);
			attr.acceptor.open(endpoint.protocol());
			attr.acceptor.set_option(Tcp::acceptor::reuse_address(true));
			attr.acceptor.bind(endpoint);
			attr.acceptor.listen();
			accept(attr);
		}

		/*
		Accept incoming connection.
		input: engine attributes.
		output: none.
		*/
		static void accept(Attributes& attr)
		{
			auto conn = attr.connections.emplace(attr.connections.begin(), attr.service); //save connection
			attr.acceptor.async_accept(*conn, boost::bind(&Engine::assignTask, std::ref(attr), conn, boost::asio::placeholders::error)); //accept
		}

		/*
		Assign new incoming task.
		input: engine attributes, connection, error code.
		output: none.
		*/
		static void assignTask(Attributes& attr, Connection connection, Error error)
		{
			bool success = true;
			Request request{};

			Logger::record(Severity::info, "New client has been accepted.");
			if (!error)
			{
				success = recv(std::ref(*connection), request); //get request

				if (success)
				{
					Logger::record(Severity::info, "New task was assigned.");
					attr.pending.emplace(connection, request, Clock::now()); //add to waiting list
					taskNotify.notify_one();
				}
				else
				{
					invalid(attr, connection);
				}
			}
			accept(attr); //accept next connection
		}

		/*
		Divides tasks between threads.
		input: engine attributes.
		output: none.
		*/
		static void taskManager(Attributes& attr)
		{
			using namespace std::chrono_literals;
			std::unique_lock<std::mutex> lock(taskLock);
			while (true)
			{
				taskNotify.wait(lock);
				while (attr.working >= THREAD_LIMIT) // wait if having more threads then allowed
				{
					std::this_thread::sleep_for(1ms);
				}

				//move task from waiting list
				auto task = attr.processing.emplace(attr.processing.begin(), std::move(attr.pending.front()));
				attr.pending.pop();

				if (task->socket->is_open()) //check if connection is open
				{
					if (checkTimeout(task))
					{
						timeout(attr, task);
					}
					else
					{
						//open new thread for task handling
						attr.working++;
						Logger::record(Severity::info, "Opened new thread for task handling. currently there are " + std::to_string(attr.working) + " working threads.");
						std::thread(&Engine::handleTask, std::ref(attr), task).detach();
					}
				}
				else
				{
					closeTask(attr, task);
				}
			}
		}

		/*
		Handles task.
		input: engine attributes, iterator to task.
		output: none.
		*/
		static void handleTask(Attributes& attr, TaskRef task)
		{
			Logger::record(Severity::info, "Handling new task.");
			Response response{};

			engine::util::unescape(task->request.source); //unescape source code

			//create project info
			core::parsing::ProjectInfo info{
				core::languages::GetLanguageCode(task->request.filename),
				task->request.filename,
				task->request.source
			};

			//parse and translate
			auto parsed = core::parsing::Parser::compile(info);
			auto translated = core::translators::MainTranslator::translate(parsed.code, task->request.target);

			//append results
			response.code = translated.second ? Response::Code::OK : Response::Code::ERR;
			response.translated = translated.first;
			response.issues = parsed.issues;

			send(std::ref(*task->socket), response); //send results

			closeTask(attr, task);

			attr.working--;
			Logger::record(Severity::info, "Closed connection with client. Task was Handled successfully.");
		}

		/*
		Closes task.
		input: engine attributes, iterator to task.
		output: none.
		*/
		static void closeTask(Attributes& attr, TaskRef task)
		{
			closeConnection(attr, task->socket);

			//remove task
			while (!taskLock.try_lock())
			{}
			attr.processing.erase(task);
			taskLock.unlock();
		}

		/*
		Closes connection with client.
		input: engine attributes, connection reference.
		output: none.
		*/
		static void closeConnection(Attributes& attr, Connection socket)
		{
			//close connection
			socket->shutdown(Tcp::socket::shutdown_both);
			socket->close();

			//remove socket from connections
			std::lock_guard<std::mutex> conlock(connLock);
			attr.connections.erase(socket);
		}

		/*
		Checks if request is timed out.
		input: engine attributes, task reference.
		output: timeout flag.
		*/
		static bool checkTimeout(TaskRef task)
		{
			TimePoint now = Clock::now(); //get current time
			MilliSec duration = std::chrono::duration_cast<MilliSec>(now - task->time); //calculate waiting time
			return duration.count() > TIMEOUT_IN_MILISEC; //check
		}

	public:
		/*
		Executes the engine.
		input: none.
		output: none.
		*/
		static void run()
		{
			auto attr = Attributes(); //initiate engine attributes
			Logger::init();

			std::thread(&Engine::taskManager, std::ref(attr)).detach();
			Logger::record(Severity::info, "Task manager initiated, start listening.");
			listen(attr); //start listening

			attr.service.run(); //run io service
		}
	};
}