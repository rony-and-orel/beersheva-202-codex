#pragma once

#include <iostream>
#include <functional>
#include "mapping.h"
#include "color.h"

#define TWO 2

namespace codex::core::debugging
{
	namespace ual
	{
		class Translator
		{
		public:
			/*
			Translates the abstract code into lua code.
			input: code map.
			output: translated text.
			*/
			static std::pair<std::string, bool> translate(mapping::CodeMap& map)
			{
				mapping::CodeMap::Cursor cursor = mapping::CodeMap::Cursor(map.getRoot());
				size_t indent_level = 0;
				return translateRecursive(cursor, indent_level, true);
			}

		private:
			struct Converters;

			/*
			Translates the code recursivly.
			input: scanning cursor, success flag.
			output: pair of translated text, success flag.
			*/
			static std::pair<std::string, bool> translateRecursive(mapping::CodeMap::Cursor& scanCursor, size_t& indent_level, bool success)
			{
				std::pair<std::string, bool> result = std::pair<std::string, bool>(); //convertion result
				std::list<mapping::Operation>& seq = scanCursor.getSeq(); //current sequance
				std::string translated = std::string(); //translated text
				operations::OperationType openOpType = operations::OperationType::NONE; //currently has no use
				operations::OperationType nextOpType = operations::OperationType::NONE;

				while (scanCursor.getIterator() != seq.end() && success)
				{
					result = convertOperation(scanCursor.getOp()); //convert
					success = result.second; //get success flag

					if (success)
					{
						translated += generateIndent(indent_level) + result.first + '\n'; //append translated text
					}

					if (!scanCursor.getOp().getScope().isEmpty() && success)
					{
						openOpType = scanCursor.getOp().getData()->getType();

						//enter scope
						scanCursor.down();
						translated += Converters::openScope(indent_level);

						result = translateRecursive(scanCursor, indent_level, success); //translate
						success = result.second; //get success flag

						if (success)
						{
							translated += result.first; //append translated text
						}

						//leave scope
						scanCursor.up();
						scanCursor++;
						if (scanCursor.isValidOp())
						{
							nextOpType = scanCursor.getOp().getData()->getType();
						}
						else
						{
							nextOpType = operations::OperationType::NONE;
						}
						scanCursor--;
						translated += Converters::closeScope(indent_level, openOpType, nextOpType);
					}
					scanCursor++; //advance
				}

				return { translated, success };
			}

			// Get indent level
			static std::string generateIndent(size_t indent)
			{
				std::string result;
				for (int i = 0; i < indent; i++)
					//result += "\t";
					result += "    ";
				return result;
			}
		public:
			/*
			Translates operation.
			input: operation.
			output: translated text, success flag.
			*/
			static std::pair<std::string, bool> convertOperation(const mapping::Operation& op)
			{
				std::pair<std::string, bool> result("", true);

				//add cases for operations that are supported in the language.
				const static std::unordered_map<operations::OperationType, std::function<std::string(operations::OperationData*)>> convertors = {
					{ operations::OperationType::IF,					[](operations::OperationData* data) -> std::string { return Converters::convertIf(operations::If::cast(data)); } },
					{ operations::OperationType::ELSE,					[](operations::OperationData* data) -> std::string { return Converters::convertElse(operations::Else::cast(data)); } },
					{ operations::OperationType::VARIABLE,				[](operations::OperationData* data) -> std::string { return Converters::convertVariable(operations::Variable::cast(data)); } },
					{ operations::OperationType::VALUE,					[](operations::OperationData* data) -> std::string { return Converters::convertValue(operations::Value::cast(data)); } },
					{ operations::OperationType::OPERATOR,				[](operations::OperationData* data) -> std::string { return Converters::convertOperator(operations::Operator::cast(data)); } },
					{ operations::OperationType::EXPRESSION,			[](operations::OperationData* data) -> std::string { return Converters::convertExpression(operations::Expression::cast(data)); } },
					{ operations::OperationType::ASSIGN,				[](operations::OperationData* data) -> std::string { return Converters::convertAssign(operations::Assign::cast(data)); } },
					{ operations::OperationType::VARIABLE_DECLARATION,	[](operations::OperationData* data) -> std::string { return Converters::convertVariableDeclaration(operations::VariableDeclaration::cast(data)); } },
					{ operations::OperationType::FUNCTION_DEFINITION,	[](operations::OperationData* data) -> std::string { return Converters::convertFunctionDefinition(operations::FunctionDefinition::cast(data)); } },
					{ operations::OperationType::CLASS_DEFINITION,		[](operations::OperationData* data) -> std::string { return Converters::convertClassDefinition(operations::ClassDefinition::cast(data)); } },
					{ operations::OperationType::CALL,					[](operations::OperationData* data) -> std::string { return Converters::convertCall(operations::Call::cast(data)); } },
					{ operations::OperationType::RETURN,				[](operations::OperationData* data) -> std::string { return Converters::convertReturn(operations::Return::cast(data)); } },
					{ operations::OperationType::IMPORT,				[](operations::OperationData* data) -> std::string { return Converters::convertImport(operations::Import::cast(data)); } },
					{ operations::OperationType::FOREACH,				[](operations::OperationData* data) -> std::string { return Converters::convertForeach(operations::Foreach::cast(data)); } },
					{ operations::OperationType::WHILE,					[](operations::OperationData* data) -> std::string { return Converters::convertWhile(operations::While::cast(data)); } },
					{ operations::OperationType::BREAK,					[](operations::OperationData* data) -> std::string { return Converters::convertBreak(operations::Break::cast(data)); } },
					{ operations::OperationType::CONTINUE,				[](operations::OperationData* data) -> std::string { return Converters::convertContinue(operations::Continue::cast(data)); } },
					{ operations::OperationType::COMMENT,				[](operations::OperationData* data) -> std::string { return Converters::convertComment(operations::Comment::cast(data)); } },
					{ operations::OperationType::ERROR,					[](operations::OperationData* data) -> std::string { return Converters::convertError(operations::Error::cast(data)); } },
				};

				auto it = convertors.find(op.getData()->getType());
				if (it == convertors.end())
				{
					result.second = false;
				}
				else
				{
					result.first = it->second(op.getData());
				}
				return result;
			}
		private:
			struct Converters
			{
				/*
				Scope closing converting method.
				input: none.
				output: converted text.
				*/
				static std::string closeScope(size_t& indent, operations::OperationType openOpType, operations::OperationType nextOpType)
				{
					if (indent == 0)
					{
						std::cout << "SCOPE_ERROR\n"; //Debugging
					}
					else
					{
						indent--;
					}
					return "";
				}

				/*
				Scope opening converting method.
				input: none.
				output: converted text.
				*/
				static std::string openScope(size_t& indent)
				{
					indent++;
					return "";
				}

				/*
				If converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertIf(const operations::If* data)
				{
					return color::paint("if ", color::ColorCode::FG_Cyan) + convertAssignable(data->cond.get());
				}

				/*
				VariableDeclaration converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertVariableDeclaration(const operations::VariableDeclaration* data)
				{
					return convertVariable(data->decl.get());
				}

				/*
				Else converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertElse(const operations::Else* data)
				{
					if (data->elifCond.get() == nullptr)
					{
						return color::paint("else", color::ColorCode::FG_Cyan);
					}
					else
					{
						return color::paint("elseif ", color::ColorCode::FG_Cyan) + convertAssignable(data->elifCond.get());
					}
				}

				/*
				Variable converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertVariable(const operations::Variable* data)
				{
					return color::paint("Variable( ", color::ColorCode::FG_Blue_Bright) + parsePath(data->info.paths) + color::paint(" )", color::ColorCode::FG_Blue_Bright);
				}

				/*
				Value converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertValue(const operations::Value* data)
				{
					return color::paint("Value( ", color::ColorCode::FG_Yellow_Bright) + convertValue(data->value.get()) + color::paint(" )", color::ColorCode::FG_Yellow_Bright);
				}
				static std::string convertValue(operations::values::BasicValue* data)
				{
					std::string result;
					switch (data->type)
					{
					case operations::values::ValueType::BOOL:
						return operations::values::Bool::cast(data)->cond ? "true" : "false";
					case operations::values::ValueType::NUMBER:
						result = std::to_string(operations::values::Number::cast(data)->num);
						while (result[result.size() - 1] == '0')
							result.pop_back();
						if (result[result.size() - 1] == '.')
							result.pop_back();
						return result;
					case operations::values::ValueType::STRING:
					{
						result = std::string(operations::values::String::cast(data)->str);
						size_t i = 0, backslashCount = 0;
						for (i = 0; i < result.length(); i++) //escape " ' " in middle of string
						{
							if (result[i] == '\'' && !(backslashCount & 1))
								result.insert(i, 1, '\\');

							if (result[i] == '\\')
								backslashCount++;
							else
								backslashCount = 0;
						}
						return "'" + result + "'";
					}
					case operations::values::ValueType::LIST:
						return "[ " + parseAssignables(operations::values::List::cast(data)->elements) + " ]";
					case operations::values::ValueType::MAP:
					{
						result += "{ ";
						operations::values::Map* map = operations::values::Map::cast(data);

						auto it = map->map.begin();
						for (it; it != map->map.end(); ++it)
						{
							if (it != map->map.begin())
							{
								result += ", ";
							}
							result += convertValue(it->first.get()) + ": " + convertAssignable(it->second.get());
						}
						result += " }";
						return result;
					}
					default:
						return "[UNKNOWN VALUE TYPE]"; //should never happen
					}
				}

				/*
				Arithmatic converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertOperator(const operations::Operator* data)
				{
					auto parse = [](const operations::Operator* data, const char* op) -> std::string
					{
						if (data->operands.size() == TWO)
						{
							return convertAssignable(data->operands.begin()->get()) + " " + op + " " + convertAssignable(std::next(data->operands.begin())->get());
						}
						else if (data->operands.size() == 1)
						{
							return std::string(op) + convertAssignable(data->operands.begin()->get());
						}
						return "[INVALID OPERANDS]";
					};

					static const std::unordered_map<operations::Operator::Type, std::function<std::string(const operations::Operator*)>> map = {
						{ operations::Operator::Type::XOR,				[&parse](const operations::Operator* data) -> std::string { return parse(data, "^"); } },
						{ operations::Operator::Type::SUB,				[&parse](const operations::Operator* data) -> std::string { return parse(data, "-"); } },
						{ operations::Operator::Type::SHR,				[&parse](const operations::Operator* data) -> std::string { return parse(data, ">>"); } },
						{ operations::Operator::Type::SHL,				[&parse](const operations::Operator* data) -> std::string { return parse(data, "<<"); } },
						{ operations::Operator::Type::BIN_OR,			[&parse](const operations::Operator* data) -> std::string { return parse(data, "|"); } },
						{ operations::Operator::Type::MUL,				[&parse](const operations::Operator* data) -> std::string { return parse(data, "*"); } },
						{ operations::Operator::Type::MOD,				[&parse](const operations::Operator* data) -> std::string { return parse(data, "%"); } },
						{ operations::Operator::Type::DIV,				[&parse](const operations::Operator* data) -> std::string { return parse(data, "/"); } },
						{ operations::Operator::Type::BIN_AND,			[&parse](const operations::Operator* data) -> std::string { return parse(data, "&"); } },
						{ operations::Operator::Type::ADD,				[&parse](const operations::Operator* data) -> std::string { return parse(data, "+"); } },
						{ operations::Operator::Type::BIN_NOT,			[&parse](const operations::Operator* data) -> std::string { return parse(data, "~"); } },
						{ operations::Operator::Type::GREATER,			[&parse](const operations::Operator* data) -> std::string { return parse(data, ">"); } },
						{ operations::Operator::Type::LESS,				[&parse](const operations::Operator* data) -> std::string { return parse(data, "<"); } },
						{ operations::Operator::Type::EQUAL,			[&parse](const operations::Operator* data) -> std::string { return parse(data, "=="); } },
						{ operations::Operator::Type::SAME_AS,			[&parse](const operations::Operator* data) -> std::string { return parse(data, "=="); } },
						{ operations::Operator::Type::GREATER_EQUAL,	[&parse](const operations::Operator* data) -> std::string { return parse(data, ">="); } },
						{ operations::Operator::Type::LESS_EQUAL,		[&parse](const operations::Operator* data) -> std::string { return parse(data, "<="); } },
						{ operations::Operator::Type::NOT_EQUAL,		[&parse](const operations::Operator* data) -> std::string { return parse(data, "!="); } },
						{ operations::Operator::Type::BOOL_AND,			[&parse](const operations::Operator* data) -> std::string { return parse(data, "and"); } },
						{ operations::Operator::Type::BOOL_OR,			[&parse](const operations::Operator* data) -> std::string { return parse(data, "or"); } },
						{ operations::Operator::Type::BOOL_NOT,			[&parse](const operations::Operator* data) -> std::string { return parse(data, "not "); } },
					};

					auto it = map.find(data->opType);
					return it == map.end() ? "[OPERATOR]" : it->second(data);
				}

				/*
				Expression converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertExpression(const operations::Expression* data)
				{
					std::string res;
					if (data->isInclosed)
					{
						res = "( " + convertOperator(data->head.get()) + " )";
					}
					else
					{
						res = convertOperator(data->head.get());
					}
					return res;
				}

				/*
				Assign converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertAssign(const operations::Assign* data)
				{
					static const std::unordered_map<operations::Assign::Type, const char*> assignTypes = {
						{ operations::Assign::Type::ADD, "+=" },
						{ operations::Assign::Type::AND, "&=" },
						{ operations::Assign::Type::DIV, "/=" },
						{ operations::Assign::Type::MOD, "%=" },
						{ operations::Assign::Type::MUL, "*=" },
						{ operations::Assign::Type::NOT, "~=" }, //TODO might not exist
						{ operations::Assign::Type::OR, "|=" },
						{ operations::Assign::Type::REGULAR, "=" },
						{ operations::Assign::Type::SHL, "<<=" },
						{ operations::Assign::Type::SHR, ">>=" },
						{ operations::Assign::Type::SUB, "-=" },
						{ operations::Assign::Type::UNKNOWN, "[ASSIGN_TYPE]" },
						{ operations::Assign::Type::XOR, "^=" },
					};

					std::string str;
					std::list<std::pair<std::unique_ptr<operations::Variable>, std::unique_ptr<operations::Assignable>>>::const_iterator it;
					for (it = data->assigned.begin(); it != data->assigned.end(); ++it)
					{
						if (it != data->assigned.begin())
						{
							str += ", ";
						}
						str += convertVariable(it->first.get());
					}
					str += ' ';
					str += assignTypes.find(data->assignType)->second;
					str += ' ';
					for (it = data->assigned.begin(); it != data->assigned.end(); ++it)
					{
						if (it != data->assigned.begin())
						{
							str += ", ";
						}
						str += convertAssignable(it->second.get());
					}
					return str;
				}

				/*
				Function definition converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertFunctionDefinition(const operations::FunctionDefinition* data)
				{
					return color::paint("FuncDef( ", color::ColorCode::FG_Red) + std::string(data->name) + "; args: { " + parseVarDeclarations(data->params) + " }" + color::paint(" )", color::ColorCode::FG_Red);
				}

				/*
				Class definition converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertClassDefinition(const operations::ClassDefinition* data)
				{
					return color::paint("ClassDef( ", color::ColorCode::FG_Magenta) + std::string(data->name) + "; inherits: { " + parseInherits(data->inherits) + " }" + color::paint(" ) ", color::ColorCode::FG_Magenta);
				}

				/*
				Call converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertCall(const operations::Call* data)
				{
					return color::paint("Call( ", color::ColorCode::FG_Red_Bright) + parsePath(data->paths) + "; args: { " + parseAssignables(data->args) + " }" + color::paint(" )", color::ColorCode::FG_Red_Bright);
				}

				/*
				Return converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertReturn(const operations::Return* data)
				{
					return color::paint("return ", color::ColorCode::FG_Cyan) + parseAssignables(data->rets);
				}

				/*
				Import converting method. Note: add Newlines \n!
				input: data object.
				output: converted text.
				*/
				static std::string convertImport(const operations::Import* data)
				{
					std::string result(color::paint("Import( ", color::ColorCode::FG_Magenta_Bright));

					if (data->specifiers.size() == 0)
					{
						auto it = data->modules.begin();
						for (it; it != data->modules.end(); ++it)
						{
							if (it != data->modules.begin())
							{
								result += ", ";
							}
							result += parsePath(*it);
						}
					}
					else
					{
						result += "{ ";
						for (auto it = data->specifiers.begin(); it != data->specifiers.end(); ++it)
						{
							if (it != data->specifiers.begin())
							{
								result += ", ";
							}
							result += parsePath(*it);
						}
						result += " } from " + parsePath(*data->modules.begin());
					}

					result += color::paint(" )", color::ColorCode::FG_Magenta_Bright);
					return result;
				}

				/*
				Foreach converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertForeach(const operations::Foreach* data)
				{
					return color::paint("foreach( ", color::ColorCode::FG_Cyan) + parseVariables(data->iters) + ", " + convertIteratable(data->obj.get()) + color::paint(" )", color::ColorCode::FG_Cyan);
				}

				/*
				While converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertWhile(const operations::While* data)
				{
					return color::paint("while ", color::ColorCode::FG_Cyan) + convertAssignable(data->cond.get());
				}

				/*
				Break converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertBreak(const operations::Break* data)
				{
					return color::paint("break", color::ColorCode::FG_Cyan);
				}

				/*
				Continue converting method.
				input: data object.
				output: converted text.
				*/
				static std::string convertContinue(const operations::Continue* data)
				{
					return color::paint("continue", color::ColorCode::FG_Cyan);
				}

				/*
				Builds coment value.
				input: data object.
				output: converted text.
				*/
				static std::string convertComment(const operations::Comment* data)
				{
					return color::paint("Comment( " + data->match + " )", color::ColorCode::FG_Green);
				}

				/*
				Builds error value.
				input: data object.
				output: converted text.
				*/
				static std::string convertError(const operations::Error* data)
				{
					return color::paint("Error( message: \"" + data->issue.msg + "\", span: [" + std::to_string(data->issue.span[0]) + ", " + std::to_string(data->issue.span[1]) + "], line: " + std::to_string(data->getLine()) + " )", color::ColorCode::BG_Red);
				}

			private:
				static std::string convertAssignable(operations::Assignable* op)
				{
					std::string output;

					//switch converters by operation's type
					switch (op->getType())
					{
						//add cases for operations that are supported in the language.
					case operations::OperationType::CALL:
						output = Converters::convertCall(operations::Call::cast(op));
						break;
					case operations::OperationType::VALUE:
						output = Converters::convertValue(operations::Value::cast(op));
						break;
					case operations::OperationType::EXPRESSION:
						output = Converters::convertExpression(operations::Expression::cast(op));
						break;
					case operations::OperationType::VARIABLE:
						output = Converters::convertVariable(operations::Variable::cast(op));
						break;
					}
					return output;
				}

				static std::string convertIteratable(operations::Iteratable* op)
				{
					std::string output;

					//switch converters by operation's type
					switch (op->getType())
					{
						//add cases for operations that are supported in the language.
					case operations::OperationType::CALL:
						output = Converters::convertCall(operations::Call::cast(op));
						break;
					case operations::OperationType::VALUE:
						output = Converters::convertValue(operations::Value::cast(op));
						break;
					case operations::OperationType::VARIABLE:
						output = Converters::convertVariable(operations::Variable::cast(op));
						break;
					}
					return output;
				}

				// parse the arguments in a function call
				static std::string parseAssignables(const std::vector<std::unique_ptr<operations::Assignable>>& args)
				{
					std::string str;
					for (auto& arg : args)
					{
						str += convertAssignable(arg.get()) + ", ";
					}
					if (args.size() > 0)
					{
						str.pop_back();
						str.pop_back();
					}
					return str;
				}

				// parse the arguments in a function call
				static std::string parseVariables(const std::vector<std::unique_ptr<operations::Variable>>& args)
				{
					std::string str;
					for (auto& arg : args)
					{
						str += convertVariable(arg.get()) + ", ";
					}
					if (args.size() > 0)
					{
						str.pop_back();
						str.pop_back();
					}
					return str;
				}

				// parse the variable declerations in a function definition
				static std::string parseVarDeclarations(const std::vector<std::unique_ptr<operations::VariableDeclaration>>& args)
				{
					std::string str;
					for (auto& arg : args)
					{
						str += convertVariableDeclaration(arg.get()) + ", ";
					}
					if (args.size() > 0)
					{
						str.pop_back();
						str.pop_back();
					}
					return str;
				}

				static std::string parseInherits(const std::vector<operations::Paths>& args)
				{
					std::string str;
					for (auto it = args.begin(); it < args.end(); ++it)
					{
						if (it != args.begin())
						{
							str += ", ";
						}
						str += parsePath(*it);
					}
					return str;
				}

				// parse together all the parts of a namespace
				static std::string parsePath(const operations::Paths& args)
				{
					std::string str;
					for (auto& arg : args)
					{
						str += arg;
						str += '.';
					}
					if (args.size() > 0)
					{
						str.pop_back();
					}
					return str;
				}
			};
		};
	}

	// Parse a CodeMap to a readable format
	std::pair<std::string, bool> view(mapping::CodeMap& map) noexcept
	{
		return ual::Translator::translate(map);
	}
}