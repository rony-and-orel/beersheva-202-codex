#pragma once
#include <string>
#include <unordered_map>

namespace color
{
	enum class ColorCode
	{
		FG_Black = 0,
		FG_Red,
		FG_Green,
		FG_Yellow,
		FG_Blue,
		FG_Magenta,
		FG_Cyan,
		FG_White,

		FG_Black_Bright,
		FG_Red_Bright,
		FG_Green_Bright,
		FG_Yellow_Bright,
		FG_Blue_Bright,
		FG_Magenta_Bright,
		FG_Cyan_Bright,
		FG_White_Bright,

		BG_Black,
		BG_Red,
		BG_Green,
		BG_Yellow,
		BG_Blue,
		BG_Magenta,
		BG_Cyan,
		BG_White,

		Default,
	};

	enum class TextType
	{
		Good = 0,
		Bad,
		Question,
		InstructionWithInput,
	};

	static const char* getColor(ColorCode color) noexcept
	{
		const static std::unordered_map<ColorCode, const char*> colorCodes = {
			{ ColorCode::FG_Black, "\u001b[30m" },
			{ ColorCode::FG_Red, "\u001b[31m" },
			{ ColorCode::FG_Green, "\u001b[32m" },
			{ ColorCode::FG_Yellow, "\u001b[33m" },
			{ ColorCode::FG_Blue, "\u001b[34m" },
			{ ColorCode::FG_Magenta, "\u001b[35m" },
			{ ColorCode::FG_Cyan, "\u001b[36m" },
			{ ColorCode::FG_White, "\u001b[37m" },

			{ ColorCode::FG_Black_Bright, "\u001b[30;1m" },
			{ ColorCode::FG_Red_Bright, "\u001b[31;1m" },
			{ ColorCode::FG_Green_Bright, "\u001b[32;1m" },
			{ ColorCode::FG_Yellow_Bright, "\u001b[33;1m" },
			{ ColorCode::FG_Blue_Bright, "\u001b[34;1m" },
			{ ColorCode::FG_Magenta_Bright, "\u001b[35;1m" },
			{ ColorCode::FG_Cyan_Bright, "\u001b[36;1m" },
			{ ColorCode::FG_White_Bright, "\u001b[37;1m" },

			{ ColorCode::BG_Black, "\u001b[40;1m" },
			{ ColorCode::BG_Red, "\u001b[41;1m" },
			{ ColorCode::BG_Green, "\u001b[42;1m" },
			{ ColorCode::BG_Yellow, "\u001b[43;1m" },
			{ ColorCode::BG_Blue, "\u001b[44;1m" },
			{ ColorCode::BG_Magenta, "\u001b[45;1m" },
			{ ColorCode::BG_Cyan, "\u001b[46;1m" },
			{ ColorCode::BG_White, "\u001b[47;1m" },

			{ ColorCode::Default, "\u001b[0m" },
		};
		return colorCodes.find(color)->second;
	}

	static std::string paint(const std::string_view text, ColorCode color) noexcept
	{
		return color::getColor(color) + std::string(text) + color::getColor(color::ColorCode::Default);
	}

	static const char* getColor(TextType type) noexcept
	{
		const static std::unordered_map<TextType, ColorCode> typeCodes = {
			{ TextType::Good, ColorCode::FG_Green_Bright },
			{ TextType::Bad, ColorCode::FG_Red },
			{ TextType::Question, ColorCode::FG_Yellow_Bright },
			{ TextType::InstructionWithInput, ColorCode::FG_Green_Bright },
		};
		return getColor(typeCodes.find(type)->second);
	}

	static std::string paint(const std::string_view text, TextType color) noexcept
	{
		return color::getColor(color) + std::string(text) + color::getColor(color::ColorCode::Default);
	}
};