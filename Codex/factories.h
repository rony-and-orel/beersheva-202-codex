#pragma once
#include "languages.h"
#include "factory_io.h"

namespace codex::core::factories
{
	struct MainFactory
	{
		/*
		Produces a list of operations by given lexemes of a specified language.
		input: language code, lexemes, filename.
		output: operation list, list of comment references, and a success flag.
		*/
		static io::FactoryOutput produce(languages::LanguageCode lang, const std::vector<lexical::Lexeme>& lexemes, std::string_view filename, const std::string_view fileContent)
		{
			auto result = io::FactoryOutput();

			//pick the matched factory and produce
			switch (lang)
			{
			case languages::LanguageCode::PY:
				result = python::Factory::produce(lexemes, filename, fileContent);
				break;
			case languages::LanguageCode::LUA:
				result = lua::Factory::produce(lexemes, filename, fileContent);
				break;
			default:
				result.success = false;
				break;
			}

			return result;
		}
	};
}