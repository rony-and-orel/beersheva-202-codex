#pragma once
#include <array>
#include <vector>
#include <string>
#include <unordered_map>
#include <boost/regex.hpp>

#define SPAN_SIZE 2

namespace codex::core::lexical
{
	enum class LexemeType
	{
		NONE = 1,
		NOP,
		IF,
		ELSE,
		ELIF,
		VARIABLE,
		VALUE,
		ARITHMATIC_OP,
		BOOLEANIC_OP,
		EXPRESSION,
		CONDITION,
		ASSIGN,
		VARIABLE_DECLARATION,
		FUNCTION_DEFINITION,
		CLASS_DEFINITION,
		MODULE_DEFINITION,
		CALL,
		SCOPE,
		MEMORY,
		RETURN,
		IMPORT,
		FOR,
		FOREACH,
		WHILE,
		UNTIL,
		ALIAS,
		SWITCH,
		CASE,
		DEFAULT,
		BREAK,
		CONTINUE,
		ENDLINE,
		SINGLE_LINE_COMMENT,
		IDENTIFIER,

		/* For Lua */
		INT,
		FLOAT,

		STR_SINGLE_LINE_SINGLE_QUOTE,
		STR_SINGLE_LINE_DOUBLE_QUOTE,
		STR_MULTI_LINE_START,
		STR_MULTI_LINE_END,

		BOOL_TRUE,
		BOOL_FALSE,

		NIL,
		DO,
		LUA_END,
		GOTO,
		LOCAL,
		DO_WHILE,
		THEN,
		FIELD_SEP,

		// brackets
		OPEN_REGULAR,
		CLOSE_REGULAR,
		OPEN_CURLY,
		CLOSE_CURLY,
		OPEN_SQUARE,
		CLOSE_SQUARE,

		// comment
		COMMENT_MULTI_START,
		COMMENT_MULTI_END,


		/* For Python */
		STR,
		DICT_START,
		DICT_END,
		LIST_START,
		LIST_END,
		COLON,
		OPEN_BRACKET,
		CLOSE_BRACKET,
		IDENTATION,
		IN,
		COMA,
		PERIOD,
		FROM,
		WITH,
		APOS,
		QOUT,
		IS,
		BOOL_NOT,
		BOOL_AND,
		BOOL_OR,
		BOOL_EQUAL,
		BOOL_LESS,
		BOOL_GREATER,
		BOOL_LESS_EQUAL,
		BOOL_GREATER_EQUAL,
		BOOL_NOT_EQUAL,
		MATH_PLUS,
		MATH_MINUS,
		MATH_MOD,
		MATH_MULT,
		MATH_DIV,
		MATH_SHR,
		MATH_SHL,
		MATH_XOR,
		MATH_AND,
		MATH_OR,
		MATH_NOT,
		ADD_EQUALIZER,
		SUB_EQUALIZER,
		MULT_EQUALIZER,
		MOD_EQUALIZER,
		DIV_EQUALIZER,
		SHR_EQUALIZER,
		SHL_EQUALIZER,
		XOR_EQUALIZER,
		OR_EQUALIZER,
		AND_EQUALIZER,
		REGULAR_EQUALIZER,
		SPACE,
		EMPTY
	};

	static std::string to_string(lexical::LexemeType type) noexcept
	{
		const static std::unordered_map<lexical::LexemeType, const char*> types = {
			{ lexical::LexemeType::NONE, "NONE"},
			{ lexical::LexemeType::NOP, "NOP"},
			{ lexical::LexemeType::IF, "IF"},
			{ lexical::LexemeType::ELSE, "ELSE"},
			{ lexical::LexemeType::VARIABLE, "VARIABLE"},
			{ lexical::LexemeType::VALUE, "VALUE"},
			{ lexical::LexemeType::ARITHMATIC_OP, "ARITHMATIC_OP"},
			{ lexical::LexemeType::BOOLEANIC_OP, "BOOLEANIC_OP"},
			{ lexical::LexemeType::EXPRESSION, "EXPRESSION"},
			{ lexical::LexemeType::ASSIGN, "ASSIGN"},
			{ lexical::LexemeType::VARIABLE_DECLARATION, "VARIABLE_DECLARATION"},
			{ lexical::LexemeType::FUNCTION_DEFINITION, "FUNCTION_DEFINITION"},
			{ lexical::LexemeType::CLASS_DEFINITION, "CLASS_DEFINITION"},
			{ lexical::LexemeType::MODULE_DEFINITION, "MODULE_DEFINITION"},
			{ lexical::LexemeType::CALL, "CALL"},
			{ lexical::LexemeType::SCOPE, "SCOPE"},
			{ lexical::LexemeType::MEMORY, "MEMORY"},
			{ lexical::LexemeType::RETURN, "RETURN"},
			{ lexical::LexemeType::IMPORT, "IMPORT"},
			{ lexical::LexemeType::FOR, "FOR"},
			{ lexical::LexemeType::FOREACH, "FOREACH"},
			{ lexical::LexemeType::WHILE, "WHILE"},
			{ lexical::LexemeType::UNTIL, "UNTIL"},
			{ lexical::LexemeType::ALIAS, "ALIAS"},
			{ lexical::LexemeType::SWITCH, "SWITCH"},
			{ lexical::LexemeType::CASE, "CASE"},
			{ lexical::LexemeType::DEFAULT, "DEFAULT"},
			{ lexical::LexemeType::BREAK, "BREAK"},
			{ lexical::LexemeType::CONTINUE, "CONTINUE"},
			{ lexical::LexemeType::ENDLINE, "ENDLINE"},
			{ lexical::LexemeType::SINGLE_LINE_COMMENT, "COMMENT"},
			{ lexical::LexemeType::IDENTIFIER, "IDENTIFIER"},
			{ lexical::LexemeType::INT, "INT"},
			{ lexical::LexemeType::FLOAT, "FLOAT"},
			{ lexical::LexemeType::STR, "STR"},
			{ lexical::LexemeType::BOOL_TRUE, "BOOL_TRUE"},
			{ lexical::LexemeType::BOOL_FALSE, "BOOL_FALSE"},
			{ lexical::LexemeType::DICT_START, "DICT_START"},
			{ lexical::LexemeType::DICT_END, "DICT_END"},
			{ lexical::LexemeType::LIST_START, "LIST_START"},
			{ lexical::LexemeType::LIST_END, "LIST_END"},
			{ lexical::LexemeType::COLON, "COLON"},
			{ lexical::LexemeType::OPEN_BRACKET, "OPEN_BRACKET"},
			{ lexical::LexemeType::CLOSE_BRACKET, "CLOSE_BRACKET"},
			{ lexical::LexemeType::IDENTATION, "IDENTATION"},
			{ lexical::LexemeType::IN, "IN"},
			{ lexical::LexemeType::COMA, "COMA"},
			{ lexical::LexemeType::PERIOD, "PERIOD"},
			{ lexical::LexemeType::FROM, "FROM"},
			{ lexical::LexemeType::WITH, "WITH"},
			{ lexical::LexemeType::QOUT, "QOUT"},
			{ lexical::LexemeType::APOS, "APOS"},
			{ lexical::LexemeType::IS, "IS"},
			{ lexical::LexemeType::BOOL_NOT, "BOOL_NOT"},
			{ lexical::LexemeType::BOOL_AND, "BOOL_AND"},
			{ lexical::LexemeType::BOOL_OR, "BOOL_OR"},
			{ lexical::LexemeType::BOOL_EQUAL, "BOOL_EQUAL"},
			{ lexical::LexemeType::BOOL_LESS, "BOOL_LESS"},
			{ lexical::LexemeType::BOOL_GREATER, "BOOL_GREATER"},
			{ lexical::LexemeType::BOOL_LESS_EQUAL, "BOOL_LESS_EQUAL"},
			{ lexical::LexemeType::BOOL_GREATER_EQUAL, "BOOL_GREATER_EQUAL"},
			{ lexical::LexemeType::BOOL_NOT_EQUAL, "BOOL_NOT_EQUAL"},
			{ lexical::LexemeType::MATH_PLUS, "MATH_PLUS"},
			{ lexical::LexemeType::MATH_MINUS, "MATH_MINUS"},
			{ lexical::LexemeType::MATH_MOD, "MATH_MOD"},
			{ lexical::LexemeType::MATH_MULT, "MATH_MULT"},
			{ lexical::LexemeType::MATH_DIV, "MATH_DIV"},
			{ lexical::LexemeType::MATH_SHR, "MATH_SHR"},
			{ lexical::LexemeType::MATH_SHL, "MATH_SHL"},
			{ lexical::LexemeType::MATH_XOR, "MATH_XOR"},
			{ lexical::LexemeType::MATH_AND, "MATH_AND"},
			{ lexical::LexemeType::MATH_OR, "MATH_OR"},
			{ lexical::LexemeType::MATH_NOT, "MATH_NOT"},
			{ lexical::LexemeType::ADD_EQUALIZER, "ADD_EQUALIZER"},
			{ lexical::LexemeType::SUB_EQUALIZER, "SUB_EQUALIZER"},
			{ lexical::LexemeType::MULT_EQUALIZER, "MULT_EQUALIZER"},
			{ lexical::LexemeType::MOD_EQUALIZER, "MOD_EQUALIZER"},
			{ lexical::LexemeType::DIV_EQUALIZER, "DIV_EQUALIZER"},
			{ lexical::LexemeType::SHR_EQUALIZER, "SHR_EQUALIZER"},
			{ lexical::LexemeType::SHL_EQUALIZER, "SHL_EQUALIZER"},
			{ lexical::LexemeType::XOR_EQUALIZER, "XOR_EQUALIZER"},
			{ lexical::LexemeType::OR_EQUALIZER, "OR_EQUALIZER"},
			{ lexical::LexemeType::AND_EQUALIZER, "AND_EQUALIZER"},
			{ lexical::LexemeType::REGULAR_EQUALIZER, "REGULAR_EQUALIZER"},
			{ lexical::LexemeType::SPACE, "SPACE"},

			// lua types
			{ lexical::LexemeType::STR_SINGLE_LINE_SINGLE_QUOTE, "STR_SINGLE_LINE_SINGLE_QUOTE"},
			{ lexical::LexemeType::STR_SINGLE_LINE_DOUBLE_QUOTE, "STR_SINGLE_LINE_DOUBLE_QUOTE"},
			{ lexical::LexemeType::STR_MULTI_LINE_START, "STR_MULTI_LINE_START"},
			{ lexical::LexemeType::STR_MULTI_LINE_END, "STR_MULTI_LINE_END"},
			{ lexical::LexemeType::DO, "DO"},
			{ lexical::LexemeType::ELIF, "ELIF"},
			{ lexical::LexemeType::LUA_END, "END"},
			{ lexical::LexemeType::GOTO, "GOTO"},
			{ lexical::LexemeType::LOCAL, "LOCAL"},
			{ lexical::LexemeType::NIL, "NIL"},
			{ lexical::LexemeType::DO_WHILE, "DO_WHILE"},
			{ lexical::LexemeType::THEN, "THEN"},
			{ lexical::LexemeType::FIELD_SEP, "FIELD_SEP"},

			{ lexical::LexemeType::OPEN_REGULAR, "OPEN_REGULAR"},
			{ lexical::LexemeType::CLOSE_REGULAR, "CLOSE_REGULAR"},
			{ lexical::LexemeType::OPEN_CURLY, "OPEN_CURLY"},
			{ lexical::LexemeType::CLOSE_CURLY, "CLOSE_CURLY"},
			{ lexical::LexemeType::OPEN_SQUARE, "OPEN_SQUARE"},
			{ lexical::LexemeType::CLOSE_SQUARE, "CLOSE_SQUARE"},
			{ lexical::LexemeType::COMMENT_MULTI_START, "COMMENT_MULTI_START"},
			{ lexical::LexemeType::COMMENT_MULTI_END, "COMMENT_MULTI_END"},
		};

		auto it = types.find(type);
		return it == types.end() ? "UNKNOWN" : it->second;
	}

	struct Regex
	{
		Regex(const std::string& pattern, LexemeType type) noexcept :
			pattern(pattern), type(type)
		{}

		std::string to_string() const noexcept
		{
			return "{ type: " + lexical::to_string(type) + ", pattern: " + pattern.str() + " }";
		}

		boost::regex pattern;
		LexemeType type;
	};

	class Lexeme
	{
	public:
		//c'tor
		constexpr explicit Lexeme(std::string_view match, const std::array<ptrdiff_t, SPAN_SIZE>& span, LexemeType type) noexcept :
			_match(match), _type(type), _span(span)
		{}

		//deleted copy c'tor
		Lexeme(const Lexeme&) = delete;

		//added c'tor implementation for std::move
		Lexeme(Lexeme&& other) noexcept :
			_match(std::move(other._match)), _type(other._type), _span(std::move(other._span))
		{
			other._type = LexemeType::NONE;
		}

		//deleted copy c'tor
		Lexeme& operator=(const Lexeme&) = delete;

		//added c'tor implementation for std::move
		Lexeme& operator=(Lexeme&& other) noexcept
		{
			_match = std::move(other._match);
			_span = std::move(other._span);
			_type = other._type;
			other._type = LexemeType::NONE;
			return *this;
		}

		bool isInside(const Lexeme& other) const noexcept
		{
			return _span[0] >= other._span[0] && _span[1] < other._span[1] || _span[0] > other._span[0] && _span[1] <= other._span[1];
		}

		//type getter
		LexemeType getType() const noexcept
		{
			return _type;
		}

		//span getter
		std::array<ptrdiff_t, SPAN_SIZE> getSpan() const noexcept
		{
			return _span;
		}

		//match getter
		std::string_view getMatch() const noexcept
		{
			return _match;
		}

		std::string to_string() const noexcept
		{
			return "{ match: \"" + static_cast<std::string>(getMatch()) + "\", span: (" + std::to_string(getSpan()[0]) + ", " + std::to_string(getSpan()[1]) + "), type: " + lexical::to_string(getType()) + " }";
		}
	private:
		std::string_view _match; //matched string
		LexemeType _type; //operation type
		std::array<ptrdiff_t, SPAN_SIZE> _span; //match span
	};
}