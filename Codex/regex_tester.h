#pragma once
#include <iostream>
#include <filesystem>
#include <fstream>
#include <string>
#include "languages.h"
#include "lexeme.h"
#include "lexer.h"
#include "debugging_util.h"

namespace codex
{
	namespace core
	{
		namespace debugging
		{
			class RegexTester
			{
			private:
				struct regexTesterInfo
				{
					std::string code;
					std::vector<lexical::Regex> regexes;
					std::vector<lexical::Regex const*> badRegexes;
					std::vector<bool> matches;
				};

				// clear matches vector
				static void clearMatches(regexTesterInfo& info)
				{
					for (size_t i = 0; i < info.matches.size(); i++)
					{
						info.matches[i] = false;
					}
				}

				// print given code with regex match markings
				static void markPrint(const regexTesterInfo& info)
				{
					std::string output;
					bool isMatch = false;

					for (size_t i = 0; i < info.code.size(); i++)
					{
						if (isMatch != info.matches[i])
						{
							isMatch = info.matches[i];
							output += isMatch ? color::getColor(color::ColorCode::BG_Cyan) : color::getColor(color::ColorCode::Default);
						}
						output += info.code[i];
					}
					std::cout << output << color::getColor(color::ColorCode::Default) << "\n\n";
				}

				// print all the problomatic regexes
				static void printIncorrectRegexes(const regexTesterInfo& info)
				{
					std::string output("\nIncorrect regexes:\n");
					for (auto& regex : info.badRegexes)
					{
						output += color::paint(lexical::to_string(regex->type), color::TextType::Bad) + " - " + regex->pattern.str() + "\n";
					}

					if (info.badRegexes.size() > 0)
					{
						std::cout << output << "\n";
					}
				}

				// test a given regex
				static void testRegex(regexTesterInfo& info, const lexical::Regex& regex)
				{
					std::vector<lexical::Lexeme> lexemes = lexical::Lexer::matchPattern(info.code, regex);

					// Compile matches into array
					for (auto& lexeme : lexemes)
					{
						for (ptrdiff_t i = lexeme.getSpan()[0]; i < lexeme.getSpan()[1]; i++)
						{
							info.matches[i] = true;
						}
					}

					// print match
					RegexTester::markPrint(info);
				}

				// ask user if a given regex is valid
				static void askIsValidRegex(std::vector<lexical::Regex const*>& badRegexes, lexical::Regex const* regex)
				{
					char ch = 0;
					bool valid = false;

					std::cout << color::paint("Is the regex correct? y/n ", color::TextType::Question);

					if (!util::getYesNoInput()) // keep incorrect regex
					{
						badRegexes.push_back(regex);
					}
				}

				static void printRegexByType(const regexTesterInfo& info)
				{
					int option = 0;
					bool valid = false;
					std::vector<lexical::Regex>::const_iterator it;

					do
					{
						std::cout << color::paint("Choose regex type (type 0 for exit): ", color::TextType::InstructionWithInput);

						do // while invalid input
						{
							valid = false;
							std::cin >> option;
							if (std::cin.fail())
							{
								std::cin.clear();
								std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
							}
							else if (option == 0) // exit option
							{
								valid = true;
							}
							else
							{
								it = std::find_if(info.regexes.begin(), info.regexes.end(), [&option](const lexical::Regex& regex) {return (int)regex.type == option; });
								if (it != info.regexes.end())
								{
									valid = true;
								}
							}
							if (!valid)
							{
								std::cout << util::std_invalid_msg;
							}
						} while (!valid);
						if (option != 0)
						{
							std::cout << color::paint(lexical::to_string(it->type), color::ColorCode::FG_Green) + " - " + it->pattern.str() + "\n";
						}
					} while (option != 0);

				}

				// ask for regex types and test them
				static void testRegexByType(regexTesterInfo& info)
				{
					int option = 0;
					bool valid = false;
					std::vector<lexical::Regex>::const_iterator it;

					do
					{
						std::cout << color::paint("Choose regex type (type 0 for exit): ", color::TextType::InstructionWithInput);

						do // while invalid input
						{
							valid = false;
							std::cin >> option;
							if (std::cin.fail())
							{
								std::cin.clear();
								std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
							}
							else if (option == 0) // exit option
							{
								valid = true;
							}
							else
							{
								it = std::find_if(info.regexes.begin(), info.regexes.end(), [&option](const lexical::Regex& regex) {return (int)regex.type == option; });
								if (it != info.regexes.end())
								{
									valid = true;
								}
							}
							if (!valid)
							{
								std::cout << util::std_invalid_msg;
							}
						} while (!valid);

						if (option != 0) //test regex
						{
							util::cls();
							RegexTester::testRegex(info, *it);
							RegexTester::askIsValidRegex(info.badRegexes, &*it);
							RegexTester::clearMatches(info);
						}
					} while (option != 0);

					RegexTester::printIncorrectRegexes(info);
				}

				// Test all the regexes of a language one by one
				static void printAllRegexes(const regexTesterInfo& info)
				{
					util::cls();
					for (auto& regex : info.regexes)
					{
						std::cout << (int)regex.type << "\t- " << color::paint(lexical::to_string(regex.type), color::ColorCode::FG_Green) + " - " + regex.pattern.str() + "\n";
					}
				}

				static void testAllRegexes(regexTesterInfo& info)
				{
					for (auto& regex : info.regexes)
					{
						util::cls();
						std::cout << "Viewing regex:\t" + color::paint(lexical::to_string(regex.type), color::ColorCode::FG_Cyan_Bright) + "\n\n";
						RegexTester::testRegex(info, regex);
						std::cout << "Viewing regex:\t" + color::paint(lexical::to_string(regex.type), color::ColorCode::FG_Cyan_Bright) + "\n";
						RegexTester::askIsValidRegex(info.badRegexes, &regex);
						RegexTester::clearMatches(info);
					}
					RegexTester::printIncorrectRegexes(info);
				}

				// Get and test a regex from the user
				static void testCustomRegex(regexTesterInfo& info)
				{
					std::string customPattern;
					std::cout << color::paint("Please enter a custom regex: ", color::TextType::InstructionWithInput);
					std::cin >> customPattern;
					util::cls();
					try
					{
						RegexTester::testRegex(info, lexical::Regex(customPattern, lexical::LexemeType::NONE));
					}
					catch (std::exception&)
					{
						std::cout << color::paint("Invalid regex pattern!\n\n", color::TextType::Bad);
					}
				}

				static void testFile(const std::filesystem::path& filePath, languages::LanguageCode lang)
				{
					regexTesterInfo info;
					info.regexes = lexical::getDefaultRegexSet(lang);
					info.code = util::readFile(filePath);
					info.matches.resize(info.code.size());

					int option = 0;
					do
					{
						std::cout << color::paint("Welcome to Regex testing wizard!\n", color::ColorCode::FG_Cyan_Bright)
							+ "What would you like to do ?\n"
							""
							"1) Print regex by type\n"
							"2) Test regex by type\n"
							"3) Print all regexes\n"
							"4) Test all regexes\n"
							"5) Enter custom regex\n"
							"6) Exit\n";
						option = util::getValidInput(1, 6);
						switch (option)
						{
						case 1:
							RegexTester::printRegexByType(info);
							break;
						case 2:
							RegexTester::testRegexByType(info);
							break;
						case 3:
							RegexTester::printAllRegexes(info);
							break;
						case 4:
							RegexTester::testAllRegexes(info);
							break;
						case 5:
							RegexTester::testCustomRegex(info);
							break;
						case 6:
							break;
						}

						RegexTester::clearMatches(info);

						// clear bad regex list
						info.badRegexes.resize(0);
					} while (option != 6);
				}
			public:
				/*
				Run the Regex Tester.
				Can run on a directory, and then choose a file, or be run on a file directly.
				input: relative path to file/directory, language to test.
				output: none.
				*/
				static void run(const std::string path, languages::LanguageCode lang)
				{
					std::filesystem::path workPath = std::filesystem::current_path().append(path);

					if (std::filesystem::is_directory(workPath))
					{
						// ask for file from directory
						do
						{
							std::filesystem::path filePath = util::getFileInput(workPath);
							testFile(filePath, lang);
							std::cout << color::paint("Would you like to choose another file? y/n ", color::TextType::Question);
						} while (util::getYesNoInput());
					}
					else if (std::filesystem::is_regular_file(workPath))
					{
						testFile(workPath, lang);
					}
					else
					{
						std::cout << color::paint("Not a valid path!\n", color::TextType::Bad);
					}
				}
			};
		}
	}
}