function foo1 (a)
	x = 4
end

function foo2 ()
	x = 4
end

function foo3 (  a  )
end

function foo4 ()
end

function foo5 (   a   )
	return x ~= 4
end

function foo6 ()
	return x == 4
end

function foo7(a, b, c)
	return a, c, b
end


function foo1(a)
	x=4
end

function foo2()
	x=4
end

function foo3(a)
end

function foo4()
end

function foo5(a)
	return x==4
end

function foo6()
	return x==4
end

function foo7(a,b,c)
	return a,c,b
end