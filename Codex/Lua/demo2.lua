function printCentered( y,s )
	local x = math.floor((w - string.len(s)) / 2)
	term.setCursorPos(x,y)
	term.clearLine()
end
 
local op = 1
 
local function drawMenu()
	term.clear()
	term.setCursorPos(1,1)
	if op == 1 then
		term.write("Yes")
	else
		term.write("No")
	end
end

local function drawFrontend()
   printCentered( math.floor(h/2), "test 1")
end

while true do
	local e = os.pullEvent()
	if e == "key" then
		local key = p
		if key == 73 or key == 176 then
			if op > 1 then
				op = op - 1
				drawFrontend()
			end
		elseif key == 72 or key == 175 then
			if op < 2 then
				op = op + 1
				drawFrontend()
			end
		elseif key == 28 then
			break
		end
	end
end

if op  == 1 then
	file = fs.open("shell/req", "r")
	content = file.readAll()
	file.close()

if string.find(content, "mode") then
	shell.run("shell/prog1")
else
	shell.run("shell/prog2")
end

if op  == 2 then
	shell.run("shell/prog3")
end
