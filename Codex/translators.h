#pragma once
#include "languages.h"
#include "mapping.h"

namespace codex::core::translators
{
	struct MainTranslator
	{
		/*
		Translates the abstract code into a specified language.
		input: code map, target language code.
		output: translated text.
		*/
		static std::pair<std::string, bool> translate(mapping::CodeMap& map, languages::LanguageCode lang)
		{
			std::pair<std::string, bool> result = std::pair<std::string, bool>();
			bool success = true;

			switch (lang)
			{
			case languages::LanguageCode::LUA:
				result = lua::Translator::translate(map);
				break;
			case languages::LanguageCode::PY:
				result = python::Translator::translate(map);
				break;
			case languages::LanguageCode::JS:
				result = javascript::Translator::translate(map);
				break;
			case languages::LanguageCode::RUBY:
				result = ruby::Translator::translate(map);
				break;
			case languages::LanguageCode::PSUDO:
				result = psudo::Translator::translate(map);
				break;
			default:
				result.second = false;
				break;
			}

			return result;
		}
	};
}