#pragma once
#include <list>
#include <array>
#include <string>
#include "issue.h"

#define SPAN_SIZE 2

namespace codex
{
	namespace core
	{
		namespace issues
		{
			class IssueBoard
			{
			public:
				IssueBoard() :
					_issues()
				{}

				~IssueBoard()
				{}

				void submit(Issue issue)
				{
					_issues.push_front(issue);
				}

				void submit(std::array<ptrdiff_t, SPAN_SIZE> span, size_t line)
				{
					_issues.emplace_front(span, line);
				}

				void submit(const Issue::Type type, const std::string& msg, std::array<ptrdiff_t, SPAN_SIZE> span, size_t line)
				{
					_issues.emplace_front(type, msg, span, line);
				}

				bool empty() const
				{
					return _issues.empty();
				}

				std::pair<std::list<Issue>::iterator, bool> last()
				{
					return { _issues.begin(), !empty() };
				}

				std::pair<std::list<Issue>::const_iterator, bool> last() const
				{
					return { _issues.begin(), !empty() };
				}

				const std::list<Issue>& list() const
				{
					return _issues;
				}

			private:
				std::list<Issue> _issues;
			};
		}
	}
}