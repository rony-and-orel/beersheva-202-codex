#pragma once

#include <vector>
#include <string>
#include <boost/regex.hpp>

#include "lua.h"
#include "python.h"
#include "javascript.h"
#include "ruby.h"
#include "psudo.h"

namespace codex
{
	namespace core
	{
		namespace languages
		{
			enum class LanguageCode
			{
				NONE = 0,
				LUA,
				PY,
				JS,
				RUBY,
				PSUDO, //English
			};

			/*
			Get the programming language from a file's name.
			input: filename to check.
			output: language code.
			*/
			static LanguageCode GetLanguageCode(const std::string& filename)
			{
				LanguageCode code = LanguageCode::NONE;
				size_t fileTypeIndex = filename.find_last_of('.');

				if (filename.size() != 0 && fileTypeIndex != -1)
				{
					std::string_view filetype(filename.c_str() + fileTypeIndex + 1);

					if (filetype == "py")
					{
						code = LanguageCode::PY;
					}
					else if (filetype == "lua")
					{
						code = LanguageCode::LUA;
					}
					else if (filetype == "js")
					{
						code = LanguageCode::JS;
					}
					else if (filetype == "rb")
					{
						code = LanguageCode::RUBY;
					}
				}
				return code;
			}
			
			/*
			Get the programming language from an integer value.
			input: integer value.
			output: language code.
			*/
			static LanguageCode GetLanguageCode(const int code)
			{
				const static std::unordered_map<int, LanguageCode> codes{
					{ (int)LanguageCode::LUA , LanguageCode::LUA },
					{ (int)LanguageCode::PY, LanguageCode::PY },
					{ (int)LanguageCode::RUBY, LanguageCode::RUBY },
					{ (int)LanguageCode::JS, LanguageCode::JS },
					{ (int)LanguageCode::PSUDO, LanguageCode::PSUDO },
				};

				auto it = codes.find(code);
				return it == codes.end() ? LanguageCode::NONE : it->second;
			}
		}
	}
}