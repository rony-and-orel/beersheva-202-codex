#pragma once
#include <unordered_map>

namespace util
{
	// LookupTable allows for indexing a value by two diffrent key types
	template <class value, class keyA, class keyB>
	class LookupTable
	{
	public:
		typedef typename std::unordered_map<keyA, value>::iterator valueRef;

		explicit LookupTable() = default;

		// disable copying
		LookupTable(const LookupTable<value, keyA, keyB>&) = delete;
		LookupTable& operator=(const LookupTable<value, keyA, keyB>&) = delete;

		LookupTable(LookupTable<value, keyA, keyB>&& other) noexcept :
			_data(std::move(other._data)), _dataRef(std::move(other._dataRef))
		{}

		LookupTable& operator=(LookupTable<value, keyA, keyB>&& other) noexcept
		{
			_data = std::move(other._data);
			_dataRef = std::move(other._dataRef);
			return *this;
		}

		//getters
		std::unordered_map<keyA, value>& getData() noexcept
		{
			return _data;
		}

		// insert a new value into database
		std::pair<valueRef, bool> insert(value&& val, const keyA& keya, const keyB& keyb) noexcept
		{
			auto it = _data.insert(std::make_pair(keya, std::move(val)));
			if (it.second)
			{
				_dataRef.insert(std::make_pair(keyb, keya));
			}
			return it;
		}

		bool empty() const noexcept
		{
			return _data.empty() && _dataRef.empty();
		}

		// emplaces a new value into map
		template<typename... Args>
		std::pair<valueRef, bool> emplace(const keyA& keya, const keyB& keyb, Args&&... args) noexcept
		{
			auto it = _data.emplace(keya, std::move(value(std::forward<Args>(args)...)));
			if (it.second)
			{
				_dataRef.emplace(keyb, keya);
			}
			return it;
		}

		valueRef find(const keyA& keya) noexcept
		{
			return _data.find(keya);
		}

		valueRef find(const keyB& keyb) noexcept
		{
			auto itRef = _dataRef.find(keyb);

			if (itRef == _dataRef.end())
			{
				return _data.end();
			}
			else
			{
				return _data.find(itRef.second);
			}
		}

		valueRef begin()
		{
			return _data.begin();
		}

		valueRef end()
		{
			return _data.end();
		}
	private:
		std::unordered_map<keyA, value> _data; // main table
		std::unordered_map<keyB, keyA> _dataRef; // secondary table
	};
}